-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 08-07-2020 a las 14:20:38
-- Versión del servidor: 5.6.47-cll-lve
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `paginaautodenar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agendac`
--

CREATE TABLE `agendac` (
  `ida` int(11) NOT NULL,
  `cedula` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vehiculo` int(11) DEFAULT NULL,
  `placa` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revision` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `idmeca` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `agendac`
--

INSERT INTO `agendac` (`ida`, `cedula`, `nombre`, `celular`, `email`, `vehiculo`, `placa`, `revision`, `idmeca`) VALUES
(7, '1085291627', 'Jonnathan', '3127922374', 'jj.rosero@hotmail.com', 9, 'vhg-513', '', 3),
(11, '12121', 'asa', '2212', 'sas@gmail.com', 15, 'xxx', '75000', 3),
(12, '10982342', 'dario fernando lopez', '3128973472', 'darrio@hotmail.com', 15, 'xxxxxxx', '65000', 1),
(13, '1085249756', 'wilmer diaz', '3174199456', 'wilmerneyq', 1, 'DLV075', '50000', 2),
(14, '1085249756', 'wilmer diaz', '3174199456', 'wilmerney@gmail.com', 1, 'DLV075', '50000', 2),
(15, '1086360377', 'Javier', '3127289794', 'javiererasorosero@gmail.com', 6, 'Dtk319', 'otr', 8),
(16, '18128868', 'CARLOS CAMACHO QUINTERO', '3112317445', 'camachocarlos1981@gmail.com', 31, 'iim242', '5000', 1),
(17, '123', 'dd', '222', 'sss', 0, 'xx', '85000', 0),
(18, '30732955', 'ANA SILVIA MARTINEZ', '3182881992', 'anasmart@esap.edu.co', 0, 'MJT372', 'otr', 0),
(19, '1085248195', 'Alejandro NarvÃ¡ez 321', '3218732626', 'alejandronarvaez86@hotmail.com', 0, 'Azw', 'otr', 0),
(20, '1089482009', 'nilber oliver cÃ¡rdenas', '3188371623', 'nilbercardenas@hotmail.com', 0, 'ENX489', '10000', 0),
(23, '94393525', 'JosÃ© Guillermo Viedma', '3012544957', 'joguiviro123@hotmail.com', 0, 'IYZ011', '25000', 0),
(24, '94393525', 'JosÃ© Guillermo Viedma', '3012544957', 'joguiviro123@hotmail.com', 0, 'IYZ011', '25000', 0),
(30, '15814849', 'ROBINSON GOMEZ', '3167368653', 'robinsongomeza@gmail.com', 0, 'QGA755', 'otr', 0),
(36, '87069370', 'Daniel Benavides', '3143364434', 'btorresdaniel@hotmail.com', 0, 'Hmr439', 'otr', 0),
(40, '5268463', 'jhon wilmar mora', '3155792698', 'wilmersiemens@hotmail.com', 2, 'ENX441', 'ayf', 16),
(45, '1085257772', 'AndrÃ©s Montealegre', '3183439177', 'montehappy@outlook.com', 12, 'Faz175', 'otr', 18),
(46, '847154236', 'orlando', '3127737349', '11616', 0, 'luf335', '75000', 0),
(47, '', '', '', '', 0, '', '', 0),
(48, '', '', '', '', 0, '', '', 0),
(49, '12751828', 'Oscar MesÃ­as', '3022644892', 'omesas@hotmail.co.uk', 0, 'AVA212', '75000', 0),
(50, '1757664659', 'Carlos guevara', '0996153095', 'Cargue312@gmail.com', 0, 'Pdi2760', 'ayf', 0),
(51, '', '', '', '', 0, '', '', 0),
(52, '', '', '', '', 0, '', '', 0),
(53, '1002462925', 'GLADYS GUAÃ‘A', '0997905513', 'gladyscgh@hotmail.com', 0, 'PID588', '5000', 0),
(54, '', '', '', '', 0, '', '', 0),
(55, '', '', '', '', 0, '', '', 0),
(56, '', '', '', '', 0, '', '', 0),
(57, '', '', '', '', 0, '', '', 0),
(58, '', '', '', '', 0, '', '', 0),
(59, '', '', '', '', 0, '', '', 0),
(60, '', '', '', '', 0, '', '', 0),
(61, '', '', '', '', 0, '', '', 0),
(62, '', '', '', '', 0, '', '', 0),
(63, '13071606', 'fredy cardenas', '3103395623', 'camcholo@gmail.com', 0, 'mwv273', '45000', 0),
(64, '12974972', 'Homero tobat', '3102252154', 'Homtobate@gmail.com ', 0, 'Auv217', '', 0),
(65, '', '', '', '', 0, '', '', 0);

--
-- Disparadores `agendac`
--
DELIMITER $$
CREATE TRIGGER `addplaca` AFTER INSERT ON `agendac` FOR EACH ROW insert into colision(idc) 
values(new.ida)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `fecha_de_carga` datetime NOT NULL,
  `ruta` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `banner`
--

INSERT INTO `banner` (`id`, `fecha_de_carga`, `ruta`) VALUES
(1, '2019-01-18 12:00:00', '1.jpg'),
(2, '2019-01-18 12:01:00', '2.jpg'),
(3, '2019-01-18 12:02:00', '3.jpg'),
(4, '2019-01-18 12:04:00', '4.jpg'),
(5, '2019-01-18 12:05:00', '5.jpg'),
(6, '2019-03-23 00:00:00', '5.png'),
(7, '2019-03-23 00:00:00', '4.png'),
(8, '2019-03-23 00:00:00', '3.png'),
(9, '2019-03-23 00:00:00', '2.png'),
(10, '2019-03-23 00:00:00', '1.png'),
(11, '2019-11-05 00:00:00', '1.jpg'),
(12, '2020-03-20 00:00:00', 'cuidado redes adaptado-05.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracteristicas_bc`
--

CREATE TABLE `caracteristicas_bc` (
  `idbc` int(11) NOT NULL,
  `idv` int(11) NOT NULL,
  `intro` mediumtext COLLATE utf8mb4_unicode_ci,
  `inimg` mediumtext COLLATE utf8mb4_unicode_ci,
  `diseño` mediumtext COLLATE utf8mb4_unicode_ci,
  `dimg` mediumtext COLLATE utf8mb4_unicode_ci,
  `motor` mediumtext COLLATE utf8mb4_unicode_ci,
  `mimg` mediumtext COLLATE utf8mb4_unicode_ci,
  `dc` mediumtext COLLATE utf8mb4_unicode_ci,
  `dcimg` mediumtext COLLATE utf8mb4_unicode_ci,
  `seguridad` mediumtext COLLATE utf8mb4_unicode_ci,
  `simg` mediumtext COLLATE utf8mb4_unicode_ci,
  `img` mediumtext COLLATE utf8mb4_unicode_ci,
  `bcimgn` mediumtext COLLATE utf8mb4_unicode_ci,
  `bctxtn` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `caracteristicas_bc`
--

INSERT INTO `caracteristicas_bc` (`idbc`, `idv`, `intro`, `inimg`, `diseño`, `dimg`, `motor`, `mimg`, `dc`, `dcimg`, `seguridad`, `simg`, `img`, `bcimgn`, `bctxtn`) VALUES
(19, 19, 'Seguridad y diseño ajustados a su negocio.\r\nEl NHR Reward Euro IV, es un camión con un peso bruto vehicular de 4.100 kg, que se ajusta a las necesidades de una gran variedad de negocios.\r\nLa dirección es asistida hidráulicamente y abatible en altura y posición garantizando mayor ergonomía y comodidad al conductor. Ahora, el NHR Reward Euro IV está equipado con una silla central para dos pasajeros, 3 cinturones de seguridad (2 de 3 puntos y central de tres puntos), 2 manijas en la cabina y 2 manijas en la puerta que facilitan el acceso al camión. Adicionalmente, el nuevo NHR Reward Euro IV es entregado con llanta de repuesto y con herramientas de cambio (palanca y gato).', 'img/camiones/nhrin.jpg', 'La cabina del nuevo NHR REWARD EURO IV, es cuadrada con el nuevo diseño aerodinamico,con columnas frontales y laterales rectas que proporcionan  una belleza funcional y un excelente aprovechamiento del espacio. El parabrisas y las ventanas laterales estan diseñadas para una maxima versatilidad, creando una cabina en forma de cubiculo que optimiza el nuevo diseño aerodinamico y el espacio interior en comparacion con los modelos\r\nanteriores.', 'img/camiones/nhrd.jpg', 'Ideal para el combustible Colombiano.\r\nEl Chevrolet NHR Reward Euro IV está equipado con un motor Isuzu 4JH1-TC Turbo, cargado con Intercooler, con inyección directa, potencia: 104@2.800 hp@rpm, torque: 23,4@1.400 kg-m@rpm y emisiones Euro IV, que garantizan una operación ideal para el combustible colombiano y menores costos de mantenimiento', 'img/camiones/nhrm.jpg', '', 'img/camiones/nhrdc.jpg', 'Nuevo diseño que ofrece mayor visibilidad.\r\nLa cabina del nuevo NHR Reward Euro IV, altamente rígida y el bastidor reforzado controlan las fuerzas de impacto para minimizar la distorsión en caso de colisión, protegiendo a los conductores al crear un espacio de supervivencia.\r\nAdemás, con el nuevo diseño de cabina, los conductores tienen mayor visibilidad de las áreas que rodean el vehículo.\r\nEl NHR Reward Euro IV cuenta además con 2 espejos laterales, 1 en cabina, y tres cinturones de seguridad (2 de tres puntos y central de 2 puntos) proporcionando mayor seguridad a los ocupantes.', 'img/camiones/nhrs.jpg', 'img/camiones/nhr.jpg', 'segmentos/camiones/img/nhr1.JPG', 'Seguridad y diseño ajustados a su negocio. El NHR Reward Euro IV, es un camión con un peso bruto vehicular de 4.100 kg, que se ajusta a las necesidades de una gran variedad de negocios.'),
(20, 20, 'Seguridad y diseño ajustados a su negocio.\r\nEl nuevo NHR Doble Cabina Reward Euro IV puede cargar hasta 2 toneladas y el primero de Chevrolet que cuenta con un espacio confortable para 6 pasajeros, es el camión que necesitan las empresas que requieren movimiento de carga más personal, es el vehículo ideal para trabajo multifuncional con opción de transporte familiar.\r\nCuenta con un motor Isuzu 4JH1-TC de inyección directa Common Rail que garantiza mayor economía en el consumo de combustible y genera menos contaminación ambiental.', 'img/camiones/nhdin.jpg', 'La cabina del nuevo NHR Doble Cabina Reward Euro IV es cuadrada con un diseño aerodinámico, con columnas frontales y laterales rectas que además de un buen diseño, da un excelente aprovechamiento del espacio logrando que 6 pasajeros puedan viajar cómodamente.\r\nCuenta con 2 espejos laterales, 1 en cabina. Las ventanas laterales están especialmente diseñadas para optimizar el desempeño aerodinámico y el espacio interior, y lo mejor, el panel de instrumentos utiliza el motivo “onda dura” que expande el sentido del espacio interior, proporcionando mayor comodidad al conductor.', 'img/camiones/nhdd.jpg', 'Ideal para el combustible colombiano.\r\nEl Chevrolet NHR Doble Cabina Reward Euro IV, está equipado con un motor Isuzu 4JH1-TC de inyección directa Common Rail, es un motor que garantiza un mayor ahorro de combustible y menor contaminación, lo logra gracias a su sistema de control que inyecta con precisión el combustible necesario.\r\nSu motor es de 2.999 cc. Con una potencia de 104 HP. Y 23.4 Kg-m. de torque, viene con el sistema de emisiones Euro IV que es administrado bajo tecnología EGR que no requiere el uso de Urea y optimiza la operación del motor.', 'img/camiones/nhdm.jpg', NULL, 'img/camiones/nhddc.jpg', 'Nuevo diseño que ofrece mayor visibilidad.\r\nLa cabina del NHR Doble Cabina Reward Euro IV es altamente rígida y el bastidor viene reforzado para controlar las fuerzas de impacto, minimizando la distorsión en caso de una colisión, todo esto para proteger al conductor y los ocupantes al crear un espacio de supervivencia, y para más seguridad, viene con seis cinturones (4 de tres puntos y 2 centrales de 2 puntos).', 'img/camiones/nhds.jpg', 'img/camiones/nhd.jpg', 'segmentos/camiones/img/nhrd.jpg', 'El nuevo NHR Doble Cabina REWARD puede cargar hasta 2 toneladas, es el camión que necesitan las empresas, con opción de transporte familiar.'),
(21, 21, 'NKR MEDIO REWARD EURO IV.\r\nSeguridad y diseño ajustados a su negocio\r\nEl camión NKR Reward, en sus dos versiones NKR Medio y NKR Largo.\r\nEs un camión ultraliviano con motor Isuzu 4JJ1-TC de inyección directa Common Rail, que garantiza mayor economía en el consumo de combustible y menor contaminación, gracias a su control electrónico que inyecta con precisión el combustible a una presión extremadamente alta, reduciendo en NO2 y la materia en partículas.', 'img/camiones/nkrin.jpg', 'La cabina del NKR Reward, es cuadrada con un diseño aerodinámico con columnas frontales y laterales rectas que proporcionan una belleza funcional y un excelente aprovechamiento del espacio.\r\nEl parabrisas y las ventanas laterales fueron diseñados para una máxima versatilidad, creando una cabina en forma de cubículo que optimiza el desempeño aerodinámico y el espacio interior en comparación con los modelos anteriores.\r\nEl panel de instrumentos utiliza un motivo de “onda dura” que expande el sentido del espacio interior, proporcionando mayor comodidad y ergonomía al conductor.', 'img/camiones/nkrd.jpg', 'Ideal para el combustible colombiano.\r\nEl Chevrolet NKR Medio Reward Euro IV está equipado con un motor Isuzu 4JJ1-TC Turbo cargado Intercooler, con inyección directa Common Rail, que reduce el consumo de combustible y por lo tanto los costos de operación al mismo tiempo que emite menor cantidad de contaminantes.\r\nPotencia: 122@2.600 hp@rpm, torque: 36@1.500 kg-m@rpm y Emisiones Euro IV, que garantizan una operación ideal para el combustible colombiano y menor costo de mantenimiento.', 'img/camiones/nkrm.jpg', NULL, 'img/camiones/nkrdc.jpg', 'Nuevo diseño que ofrece mayor visibilidad.\r\nLa cabina del NKR Medio Reward Euro IV es cuadrada, más amplia y ancha que la competencia brindando mayor comodidad y visibilidad al conductor. Además, la nueva cabina es altamente rígida de tal forma que minimiza la deformación en caso de colisión.\r\nEl NKR Medio Reward Euro IV, cuenta además con 2 espejos laterales y 1 en cabina, y 3 cinturones de seguridad (2 de tres puntos y central de 2 puntos) proporcionando mayor seguridad a los ocupantes.', 'img/camiones/nkrs.jpg', 'img/camiones/nkr.jpg', 'segmentos/camiones/img/nkr1.jpg', 'Seguridad y diseño.La cabina del NKR Reward, es cuadrada con un diseño aerodinámico con columnas que proporcionan una belleza funcional y un excelente aprovechamiento del espacio.'),
(22, 22, 'Seguridad y diseño ajustados a su negocio.\r\nEl NNR Reward, es un camión con un peso bruto vehicular de 6.300 Kg, que se ajusta a la necesidad de una gran variedad de negocios.\r\nLa dirección es asistida hidráulicamente y abatible en altura y posición garantizando mayor ergonomía y comodidad al conductor. Ahora, el NNR Reward está equipado con una silla central para dos pasajeros, 3 cinturones de seguridad (2 de 3 puntos y central de tres puntos), 2 manijas en la cabina y 2 manijas en puerta para que faciliten el acceso al camión.\r\nAdicionalmente, el NNR Reward es entregado con llantas de marca y herramienta (palanca y gato), antena, dos parlantes y una cabina más amplia y ergonómica que facilita el acceso al camión.', 'img/camiones/nnrin.jpg', 'La cabina del NNR Reward, es cuadrada con un diseño aerodinámico con columnas frontales y laterales rectas que proporcionan una belleza funcional y un excelente aprovechamiento del espacio.\r\nEl parabrisas y las ventanas laterales están diseñados para una máxima versatilidad, creando una cabina en forma de cubículo que optimiza el desempeño aerodinámico y el espacio interior en comparación con los modelos anteriores.\r\nEl panel de instrumentos utiliza un motivo de “onda dura” que expande el sentido del espacio interior, proporcionando mayor comodidad y ergonomía al conductor.\r\n ', 'img/camiones/nnrd.jpg', 'Ideal para el combustible Colombiano.\r\nEl Chevrolet NNR Reward Euro IV está equipado con un motor Isuzu 4JJ1 TC Turbo cargado Intercooler, con inyección directa Common Rail, que reduce el consumo de combustible y por lo tanto los costos de operación al mismo tiempo que emite menor cantidad de contaminantes. Potencia: 122@2.600 hp@rpm, torque: 36@1.500 kg-m@rpm y Emisiones Euro IV, que garantizan una operación ideal para el combustible Colombiano y menores costo de mantenimiento.', 'img/camiones/nnrm.jpg', NULL, 'img/camiones/nnrdc.jpg', 'Nuevo diseño que ofrece mayor visibilidad.\r\nLa cabina del NNR Reward Euro IV, altamente rígida y el bastidor reforzado controlan las fuerzas de impacto para minimizar la distorsión en caso de colisión, protegiendo a los conductores al crear una espacio de supervivencia.\r\nAdemás, con el nuevo diseño de cabina, los conductores tienen mayor visibilidad de las áreas que rodean el vehículo.\r\nPara ofrecer mayor seguridad, el chasis del NNR Reward Euro IV es perforado con el fin de reducir los impactos en casos de choque.\r\nEl NNR Reward Euro IV, cuenta además con 2 espejos laterales y 1 en cabina, y tres cinturones de seguridad (2 de tres puntos y central de 2 puntos) proporcionando mayor seguridad a los ocupantes.', 'img/camiones/nnrs.jpg', 'img/camiones/nnr.jpg', 'segmentos/camiones/img/nnr.jpg', 'El NNR Reward, es un camión con un peso bruto vehicular de 6.300 Kg, que se ajusta a la necesidad de una gran variedad de negocios.'),
(23, 23, 'Seguridad y diseño ajustados a su negocio.\r\nEl NPR Reward Euro IV, es un camión liviano con un nuevo motor Isuzu 4HK1-TCN de inyección directa Common Rail, que garantiza mayor economía en el consumo de combustible y menor contaminación, gracias a su control electrónico que inyecta con precisión el combustible a una presión extremadamente alta, reduciendo la emisión de NO2 y la contaminación ambiental.\r\nEl camión NPR Reward Euro IV, ahora viene equipado con dirección asistida hidráulicamente, telescópica y ajustable en altura y posición, 2 cinturones de seguridad de 3 puntos, cinturón central de 2 puntos, antena, dos parlantes y una cabina más amplia y ergonómica que facilita el acceso al camión.', 'img/camiones/nprin.jpg', 'La cabina del NPR Reward Euro IV, es cuadrada con un diseño aerodinámico, con columnas frontales y laterales rectas que proporcionan una belleza funcional y un excelente aprovechamiento del espacio.\r\nEl parabrisas y las ventanas laterales están diseñados para una máxima versatilidad, creando una cabina en forma de cubículo que optimiza el desempeño aerodinámico y el espacio interior en comparación con los modelos anteriores.\r\nEl panel de instrumentos utiliza un motivo de “onda dura” que expande el sentido del espacio interior, proporcionando mayor comodidad y ergonomía al conductor.', 'img/camiones/nprd.jpg', 'Ideal para el combustible colombiano.\r\nEl Chevrolet NPR Reward Euro IV está equipado con un motor Isuzu 4HK1-TCN Turbo, cargado con Intercooler, con inyección directa Common Rail, que reduce el consumo de combustible y por lo tanto los costos de operación al mismo tiempo que emite menor cantidad de contaminantes. Potencia: 153@2.600 hp@rpm, torque: 42,7@1.600 kg-m@rpm y emisiones Euro IV, que garantizan una operación ideal para el combustible colombiano y menores costos de mantenimiento.', 'img/camiones/nprm.jpg', NULL, 'img/camiones/nprdc.jpg', 'Nuevo diseño que ofrece mayor visibilidad.\r\nLa cabina del NPR Reward Euro IV, altamente rígida y el bastidor reforzado, controlan las fuerzas de impacto para minimizar la distorsión en caso de colisión, protegiendo a los conductores al crear un espacio de supervivencia.\r\nEl Chasis del NPR Reward Euro IV tiene agujeros a lo largo para mejorar la absorción de impactos en caso de colisión.\r\nAdemás, viene equipado con llantas de marca reconocida, de tamaño comercial 215/75 R17.5, que garantizan mayor estabilidad, durabilidad y seguridad', 'img/camiones/nprs.jpg', 'img/camiones/npr.jpg', 'segmentos/camiones/img/npr.jpg', 'Seguridad y diseño ajustados a su negocio El NPR Reward Euro IV garantiza mayor economía en el consumo de combustible y menor contaminación.'),
(24, 24, 'MICROBUS NKR REWARD EURO IV.\r\nEl Microbus NKR Reward Euro IV de Chevrolet está pensado como una nueva alternativa para el transporte de pasajeros.', 'img/busess/nkrin.jpg', 'Pensado para ser un vehículo de dimensiones reducidas, ideal para el transporte de pasajeros. Con una capacidad de carga de 3.672 kg, el Microbus NKR Reward Euro IV tiene un largo total de 6.030 mm. un ancho de 1.860 mm. y una distancia entre ejes de 3.345 mm.', 'img/busess/nkrd.jpg', 'Un bus bien equipado.\r\nEl Microbus NKR Reward Euro IV está equipado con un motor 4JJ1-TC Isuzu Turbo Diesel con Intercooler de cuatro cilindros en línea e inyección directa de 3 litros.\r\nAlcanza una potencia máxima: 122@2.600 hp@rpm y su torque neto: 36@1.500 kg-m@rpm.', 'img/busess/nkrm.jpg', NULL, 'img/busess/nkrdc.jpg', 'Óptimo rendimiento.\r\nPara garantizarle el mejor rendimiento, el Microbus NKR Reward Euro IV de Chevrolet cuenta con dirección asistida hidráulicamente y sistema de freno hidráulico.', 'img/busess/nkrs.jpg', 'img/busess/nkr.jpg', 'segmentos/buses/img/nkr.jpg', 'El Microbus NKR Reward Euro IV de Chevrolet está pensado como una nueva alternativa para el transporte de pasajeros.'),
(25, 25, 'MINIBUSETA NPR REWARD EURO IV.\r\nLa minibuseta NPR Reward Euro IV hace parte del segmento liviano de buses.\r\nTiene una capacidad entre 21 y 24 pasajeros y es ideal para aplicación urbana.', 'img/busess/nprin.jpg', 'Cuenta con un motor Isuzu 4HK1-TCN de inyección directa Common Rail y emisiones Euro IV, que garantiza mayor economía en el consumo de combustible y menor contaminación, gracias a su control electrónico que inyecta con precisión el combustible a una presión extremadamente alta, reduciendo la emisión de NO2 y la contaminación ambiental.', 'img/busess/nprd.jpg', 'Ideal para el combustible colombiano.\r\nLa Minibuseta NPR Reward Euro IV de Chevrolet está equipada con un motor Isuzu 4HK1-TCN Turbo, cargado con Intercooler, con inyección directa Common Rail, que reduce el consumo de combustible y por lo tanto los costos de operación, al mismo tiempo que emite menor cantidad de contaminantes. Con un motor de 5.193 cc. Potencia: 153@2.600 hp@rpm y su torque: 42@1.600 kg-m@rpm garantiza un mejor desempeño en las carreteras de la topografía colombiana.', 'img/busess/nprm.jpg', NULL, 'img/busess/nprdc.jpg', 'La Minibuseta NPR Reward Euro IV, tiene barra estabilizadora delantera, para asegurar mayor estabilidad en carretera. Además cuenta con una suspensión compuesta por 4 amortiguadores (2 delanteros y 2 traseros) que brindan mayor comodidad a los pasajeros.\r\nEl Chasis de la Minibuseta NPR Reward Euro IV tiene agujeros a lo largo para mejorar la absorción de impactos en caso de colisión. Además, viene equipada con llantas de marca reconocida, de tamaño comercial 215/75 R17.5, que garantizan mayor estabilidad, durabilidad y seguridad.', 'img/busess/nprs.jpg', 'img/busess/npr.jpg', 'segmentos/buses/img/busnpr.jpg', 'El NPR REWARD EURO IV Minibuseta, hace parte del segmento liviano de buses. Es ideal el transporte de pasajeros en ciudades y transporte intermuniciapal debido a su tamaño y versatilidad en el tráfico.'),
(26, 26, 'Amigable con el medio ambiente.\r\nEl nuevo NQR Buseton Euro IV es una excelente opción que Buses y Camiones Chevrolet ofrece para los sistemas masivos a nivel nacional,\r\ndonde requiere nivel de emisiones EURO IV.\r\n', 'img/busess/nqrin.jpg', 'El chasis del nuevo Busetón NQR Euro IV fue diseñado bajo los lineamientos de la normativa NTC 5206 próxima a implementarse en el país.\r\nEl panel de instrumentos utiliza un motivo de “onda dura” que expande el sentido del espacio interior, proporcionando mayor comodidad y ergonomía al\r\nconductor y además facilitando la operatividad del bus. Además cuenta con un alternador de 80 amperios, que permite mayor versatilidad a la hora de la conexión de dispositivos sobre el bus.', 'img/busess/nqrd.jpg', 'Pensado para andar por el territorio colombiano con un Motor ISUZU 4HK1-TCN de inyección Common Rail y emisiones catalogadas Euro IV cumpliendo los\r\nrequerimientos de los sistemas masivos de transporte, 5.193 centímetros cúbicos con 153 hp que garantizan un gran desempeño a un excelente consumo de combustible. Torque de 419 N.m', 'img/busess/nqrm.jpg', NULL, 'img/busess/nqrdc.jpg', 'Sistema que garantiza un óptimo rendimiento en cualquier circunstancia', 'img/busess/nqrs.jpg', 'img/busess/nqr.jpg', 'segmentos/buses/img/nqrbuseton.jpg', 'Amigable con el medio ambiente. El nuevo Busetón NQR Reward Euro IV es una excelente opción donde se requiere nivel de emisiones EURO IV.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracteristicas_v`
--

CREATE TABLE `caracteristicas_v` (
  `id_c` int(11) NOT NULL,
  `id_v` int(11) NOT NULL,
  `intro` mediumtext COLLATE utf8mb4_unicode_ci,
  `introim` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exterior` mediumtext COLLATE utf8mb4_unicode_ci,
  `exteriorimg` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `interior` mediumtext COLLATE utf8mb4_unicode_ci,
  `interiorimg` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `desempe` mediumtext COLLATE utf8mb4_unicode_ci,
  `desempeimg` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tec` mediumtext COLLATE utf8mb4_unicode_ci,
  `tecimg` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `segury` mediumtext COLLATE utf8mb4_unicode_ci,
  `seguryimg` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` mediumtext COLLATE utf8mb4_unicode_ci,
  `imgn` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `txtn` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `caracteristicas_v`
--

INSERT INTO `caracteristicas_v` (`id_c`, `id_v`, `intro`, `introim`, `exterior`, `exteriorimg`, `interior`, `interiorimg`, `desempe`, `desempeimg`, `tec`, `tecimg`, `segury`, `seguryimg`, `img`, `imgn`, `txtn`) VALUES
(0, 33, 'Creada para lograr todo lo que te propones Cada cierto tiempo aparece alguien capaz de superar cualquier reto, alguien que lo cambia todo, que avanza con fuerza, determinaciÃ³n e inteligencia. Te presentamos la totalmente nueva Blazer RS, la camioneta que lo cambiarÃ¡ todo.', '', 'El disruptivo diseÃ±o exterior de la Blazer RS entrega en cada lÃ­nea la fusiÃ³n  perfecta entre lo robusto y lo deportivo.', 'img/carros/exteriorblazer.png', 'LA BLAZER RS ES LA MEJOR COMBINACIÃ“N DE COMODIDAD Y DEPORTIVIDAD Asiento de conductor y pasajero con ajuste  elÃ©ctrico multi-direccional de 8 vÃ­as y soporte elÃ©ctrico lumbar de 2 vÃ­as. Volante en cuero con calefacciÃ³n y ajuste elÃ©ctrico de altura y profundidad. Aire acondicionado de doble zona. Sillas delanteras con calefacciÃ³n y ventilaciÃ³n. Memoria asiento, retrovisores y direcciÃ³n. Espejo retrovisor electrocrÃ³mico con proyector de una segunda cÃ¡mara de visiÃ³n trasera. Sistema de encendido y apertura de puertas sin llave. Sistema de Entretenimiento MyLink con pantalla tÃ¡ctil de 8â€ reconfigurable. CÃ¡mara 360.', 'img/carros/interiorblazewr.png', 'CON UN MOTOR V6 DE 3.6L, LA BLAZER RS ES LA SUV DEPORTIVA IDEAL PARA TODOS TUS PLANES. Motor V6 3.6 L   Potencia (hp/rpm) 308 @ 6600   Torque (nm @ rpm) 365 @ 5000   TransmisiÃ³n automÃ¡tica de 9 velocidades. Sistema Stop/Start   SuspensiÃ³n delantera McPherson y trasera independiente de 5 brazos   TracciÃ³n AWD', 'img/carros/desempeÃ±oblazer.png', 'WiFi powered by OnStarÂ® Descubre el mÃ¡ximo nivel de tecnologÃ­a, porque ahora tu Chevrolet tiene Wi-Fi y te proveerÃ¡ con conectividad de hasta 7 dispositivos al mismo tiempo en un rango de 15 metros y mejor intensidad de seÃ±al que en un telÃ©fono mÃ³vil o smartphone.', 'img/carros/tecnologiablazer.png', 'Seguridad activa', 'img/carros/seguridadblazer.jpg', 'img/carros/bannerblazer.png', '', ''),
(2, 2, 'DiseÃ±ado especialmente para la ciudad, Spark GT es compacto y se desliza sutilmente por los espacios urbanos. Al desempeÃ±o de su motor 1.2L de 4 cilindros se suman la seguridad del airbag frontal para el conductor, reconocimiento de voz para llamadas y sistema ChevyStar Connect. MantÃ©n tu nivel de conectividad dentro de tu Spark GT : conexiÃ³n para iPod, bluetooth y USB. AdemÃ¡s, descubre el amplio espacio lleno de detalles que esconde su exterior compacto. Conoce una nueva definiciÃ³n de diversiÃ³n. AtrÃ©vete a conducir un Spark GT ', 'img/sparkgt/b.png', '\r\nAtrevido, urbano y expresivo\r\nEl Chevrolet Spark GT es un vehÃ­culo compacto con estructura dinÃ¡mica y caracterÃ­sticas de diseÃ±o que le dan al conductor una mayor sensaciÃ³n de estabilidad, solidez y confianza. Cuenta con un sinfÃ­n de detalles. Resaltan sus lÃ­neas deportivas y juveniles con parrilla frontal de doble divisiÃ³n y parachoques delantero rediseÃ±ado. Los faros delanteros elÃ­pticos envolventes y las luces exploradoras cromadas (versiÃ³n LTZ) de alta penetraciÃ³n, le otorgan un aspecto mÃ¡s audaz. En la parte posterior, el spoiler tiene incorporado el stop LED central que mejora la visibilidad trasera del conductor y las versiones LTZ tienen espejo con desempaÃ±ador y luz direccional.\r\n\r\nLuce bien desde cualquier Ã¡ngulo\r\nSegÃºn sus versiones, Spark GT cuenta con elementos que lo hacen resaltar: guardabarros delantero-posterior mÃ¡s ajustados a las llantas que generan sensaciÃ³n de robustez y mayor desempeÃ±o aerodinÃ¡mico; manijas traseras ocultas que le dan apariencia de un vehÃ­culo 3 puertas con los beneficios de un 5 puertas; y spoiler trasero integrado a la carrocerÃ­a de doble funcionalidad.', 'img/sparkgt/sparkgtex.jpg', '\r\nNo vas a querer bajarte\r\nEl Spark GT cuenta con un diseÃ±o innovador inspirado en los felinos africanos con curvas aerodinÃ¡micas e impactantes. El panel de instrumentos y la consola central cuentan con iluminaciÃ³n Ice blue, que da la sensaciÃ³n de estar en un carro deportivo a sus ocupantes, ademÃ¡s estÃ¡n diseÃ±ados bajo el tamaÃ±o perfecto para que puedas utilizarlos sin perder de vista el camino. MantÃ©n tu viaje controlado: climatiza con el sistema de aire acondicionado disponible, desempaÃ±a los espejos elÃ©ctricos cuando sea necesario (LTZ), almacena lo que quieras en sus mÃºltiples espacios y opera con libertad los seguros de puertas y los vidrios elÃ©ctricos. El diseÃ±o inteligente del Spark GT saca mÃ¡ximo provecho del espacio interior disponible, por eso cada pasajero viaja siempre cÃ³modo y seguro en sus asientos ergonÃ³micos.\r\n\r\nMarca tu estilo en el pavimento\r\nMarca aÃºn mÃ¡s ese estilo deportivo que buscas en tu Spark GT : su versiÃ³n Full Equipo trae rines de aluminio de 14\'\'. Que se roban todas las miradas gracias a su look deportivo.', 'img/sparkgt/sparkgtit.jpg', '\r\nAlgunos carros se adaptan a tu vida mejor que otros\r\nEl Spark GT es un carro diseÃ±ado para hacer tu vida mÃ¡s fÃ¡cil, para moverte en la ciudad con seguridad, estilo y comodidad, condÃºcelo en pendientes y estaciÃ³nalo en los espacios mÃ¡s estrechos sin dificultad debido a su direcciÃ³n asistida hidrÃ¡ulicamente. DiviÃ©rtete con su motor 4 cilindros de 1,2L que otorga hasta 80,5 HP con un torque de 108 NM. Su transmisiÃ³n con embrague mejorado de 5 velocidades tiene relaciones de marchas desarrolladas y ajustadas espec&iacute;ficamente para la topolog&iacute;a del terreno Colombiano ventaja que tiene al ser ensamblado en nuestro pa&iacute;s que se denomina como de las mejores plantas de GM en SudamÃ©rica bajo los estÃ¡ndares globales de General Motors.', 'img/sparkgt/sparkgtd.png', '\r\nTECNOLOGIAS EXCLUSIVAS:<br>\r\n-Sistema de alarma preventiva de avisos de pico y placa.\r\n<br>-Apertura de puertas desde tu Smartphone v&iacute;a mensaje de texto o desde la APP Chevystar (que se te queden las llaves adentro ya no serÃ¡ un problema).\r\n<br>-Sistema de localizaci&oacute;n satelital de fÃ¡brica ( puedes saber dÃ³nde estÃ¡ tu carro desde tu Smartphone).\r\n<br>-Ubicaci&oacute;n y recuperaci&oacute;n con efectividad del 97% en caso de Robo.\r\n<br>-Sistema de consejer&iacute;a para reservar restaurantes desde tu Spark GT con solo oprimir el botÃ³n del Corbat&iacute;n Chevrolet.\r\n<br>-Servicio de AcompaÃ±amiento en ruta con monitoreo satelital (cuenta siempre con el equipo de Chevystar cuidÃ¡ndote en el camino).\r\n<br>-Encendido de Veh&iacute;culo con cÃ³digo de seguridad de 5 d&iacute;gitos.\r\n<br>-Comandos de voz para llamadas v&iacute;a bluetooth (sin tocar tu celular solo con tu voz indica a quien quieres llamar y tu Spark GT se encarga).\r\n<br>-Asistencia mÃ©dica y mecÃ¡nica.', 'img/sparkgt/sparkgtt.jpg', '\r\nPreviene, protege y responde\r\nMuÃ©vete con confianza por la ciudad gracias a los elementos de seguridad de Spark GT, que hacen de este compacto uno de los mÃ¡s seguros de su tipo.\r\nSpark GT estÃ¡ equipado con una 3ra luz de stop, frenos delanteros a disco y traseros a tambor y tiene apoyacabezas individual en todos los asientos (delanteros + traseros). AdemÃ¡s, trae airbag para conductor y seguros de niÃ±os en las puertas traseras. El sistema ISOfix para sujeciÃ³n de sillas infantiles estÃ¡ disponible segÃºn la versiÃ³n.', 'img/sparkgt/sparkgts.jpg', 'img/sparkgt/sparkgt.jpg', 'segmentos/spark/img/sg.jpg', 'El Chevrolet Spark GT, un carro deportivo y juvenil que te acompaÃ±arÃ¡ a donde quieras llegar. Cuenta con Chevystar integrado ademÃ¡s de, con conexiÃ³n para iPod®, Bluetooth y USB.'),
(3, 3, 'Sail llega para revolucionar el concepto de vehÃculo sedÃ¡n compacto gracias a su amplio espacio interior y su potente motor que te ofrece una gran eficiencia en el uso del combustible.\r\n\r\nLA MANERA MÃ¡S LINDA DE COMPARTIR EN FAMILIA. CHEVROLET SAIL', 'img/sail/sailin.jpg', '\r\nUn sedÃ¡n moderno.\r\n\r\nChevrolet Sail es un sedÃ¡n con lÃneas aerodinÃ¡micas y un frontal moderno y elegante. Consiste en una propuesta compacta que sorprende por la amplitud de sus espacios interiores y sus 420 litros de volumen en el Ã¡rea de carga', 'img/sail/sailex.jpg', '\r\nInterior fuera de serie\r\n\r\nEl Chevrolet Sail SedÃ¡n tiene un completo equipamiento en materia de confort que incluye: Chevystar rastreo y localizaciÃ³n, cojinerÃa en ecocuero, bloqueo central, vÃdrios elÃ©ctricos delanteros, espejos retrovisores exteriores elÃ©ctricos.\r\n\r\nEn su versiÃ³n LTZ cuenta adicionalmente con vÃdrios elÃ©ctricos en sus 4 puertas, alarma y apertura a distancia, radio con pantalla tÃ¡ctil, navegaciÃ³n GPS, y el exclusivo sistema de Chevrolet Chevystar que aplica a (Versiones LT y LTZ), ahora con servicios cÃ³mo Alerta de Pico y Placa, conserjerÃa a bordo, entre otros que te sorprenderÃ¡n.', 'img/sail/sailit.jpg', 'Un motor de alma agresiva para un sedÃ¡n de alma agresiva.\r\n\r\nTodas las versiones del Chevrolet Sail vienen equipadas con un confiable motor de 1.4 litros a gasolina de 4 cilindros y 16 vÃ¡lvulas DOCH, que le permiten alcanzar una potencia mÃ¡xima de 102 HP @6.000 RPM. Su sistema de VGIS (mÃºltiple de admisiÃ³n de geometrÃa variable), le proporciona un control especÃfico sobre el aire de entrada al motor, dÃ¡ndole mayor potencia y mejor desempeÃ±o a bajas y altas revoluciones.\r\n\r\nSe complementa con una transmisiÃ³n manual de 5 velocidades. Por su sistema de inyecciÃ³n de geometrÃa variable, este impulsor asegura una excelente economÃa del combustible resultando en un promedio de hasta 60 km/galÃ³n (sujeto a las condiciones y estilo de manejo).', 'img/sail/sailid.jpg', 'Un carro inteligente con tecnologÃa inteligente.\r\n\r\nEn su equipo se encuentra bloqueo o cierre centralizado, radio CD + MP3/ AUX/USB, sincronizaciÃ³n de dispositivos de audio vÃa Bluethoot (versiones LT y LTZ), entrada auxiliar que ofrece la posibilidad de conectar dispositivos de audio externos, elevavÃdrios elÃ©ctricos delanteros, espejos retrovisores externos elÃ©ctricos, consola central ergonÃ³mica, tacÃ³metro y medidor de combustible digital. \r\n\r\nLas versiones LTZ se encuentran equipadas con vÃdrios elÃ©ctricos en las 4 puertas, radio pantalla tÃ¡ctil y el exclusivo sistema Chevrolet “ Chevystar” permitiendo a los ocupantes realizar llamadas desde su telÃ©fono celular usando comandos de voz, y los nuevos servicios de consejerÃa y alerta de Pico y Placa (Versiones LT y LTZ).', 'img/sail/sailt.jpg', 'La seguridad nunca estÃ¡ de mÃ¡s\r\n\r\nChevrolet Sail SedÃ¡n cuenta con sistema de airbag para el conductor, apoyacabezas delanteros regulables en altura, sistema para sujeciÃ³n de sillas de niÃ±os (ISOFIX), cinturones de seguridad de tres puntos (delanteros y posteriores laterales) y de dos puntos (central posterior). \r\n\r\nPara la versiÃ³n LTZ cuenta con airbag para conductor y pasajero, frenos ABS + EBD y luces exploradoras delanteras, ademÃ¡s de todas las ventajas de Chevystar como: monitoreo en ruta, ubicaciÃ³n y recuperaciÃ³n de vehÃculo en caso de robo y muchos otros beneficios mÃ¡s que solo Chevrolet ofrece.', 'img/sail/sails.jpg', 'img/sail/sail.jpg', 'segmentos/medianos/img/sail1.jpg', 'Sail llega para revolucionar el concepto de vehÃculo sedÃ¡n compacto, gracias a su amplio espacio interior y su potente motor que te ofrece una gran eficiencia en el uso del combustible.'),
(4, 4, 'NUEVO CHEVROLET ONIX 2017\r\nEl nuevo Chevrolet Onix 2017 cuenta con la nueva generaciÃ³n de ChevyStar, una tecnolog&iacute;a exclusiva de Chevrolet que te permitirÃ¡ estar siempre conectado desde tu Chevrolet con lo que mÃ¡s te gusta. Chevrolet Onix, el carro diseÃ±ado para hoy.\r\nPara los d&iacute;as de selfies, para los d&iacute;as de music streaming, para los d&iacute;as en que necesitas estar conectado, para todos los d&iacute;as: Chevrolet Onix. Su diseÃ±o exterior es innovador, atractivo y juvenil, en su interior cuenta con toda la tecnolog&iacute;a y la conectividad de ChevyStar MyLink con una pantalla touch de 7 exclusiva de Chevrolet y Ãºnica en su segmento. Videos, fotos. DisfrÃºtalo en su versiÃ³n automÃ¡tica y mecÃ¡nica,', 'img/onix/b.png', 'El nuevo Chevrolet Onix, el carro diseÃ±ado para hoy.\r\nLa personalidad del nuevo Chevrolet Onix 2017 se refleja en cada detalle de su exterior e interior. Un diseÃ±o deportivo, con una parrilla frontal renovada, rines de 15\'\', exploradoras, faros de gran alcance y la mayor tecnolog&iacute;a que facilita tu camino.', 'img/onix/onixex.jpg', 'La experiencia de conducir el nuevo Chevrolet Onix 2017 es inolvidable. Sus dos versiones AutomÃ¡tica y MecÃ¡nica hacen que conducir sea una experiencia Ãºnica y llena de confort. Con mando de Control para Radio y TelÃ©fono, Control de Crucero en su versiÃ³n automÃ¡tica, Ãºnico en su segmento. Vidrios elÃ©ctricos con One Touch, Radio Touch y Computador Abordo, en la versiÃ³n AutomÃ¡tica y posteriores, Aire Acondicionado, entre otros. La ciudad se disfruta de una manera diferente.\r\nAdemÃ¡s, conexiÃ³n BLUETOOTH, ChevyStar, Android Auto y CarPlay para tener todo el entretenimiento de tu telÃ©fono celular en la pantalla tÃ¡ctil de 7\'\' de tu veh&iacute;culo.\r\nLa seguridad, la comodidad y el entretenimiento conjugados para brindarte la mejor experiencia.', 'img/onix/onixit.jpg', 'DesempeÃ±o para el d&iacute;a de hoy.\r\nCon un motor de 1,4 litros y 97 HP, este modelo estÃ¡ equipado con direcciÃ³n hidrÃ¡ulica, proporcionando un mayor confort en tu d&iacute;a a d&iacute;a. AdemÃ¡s de contar con un consumo Ã³ptimo de combustible, tambiÃ©n puedes elegir el tipo de transmisiÃ³n que mÃ¡s te convenga.', 'img/onix/onixd.jpg', 'Tecnolog&iacute;a y conectividad:\r\nToda la tecnolog&iacute;a y conectividad la tienes en el nuevo Chevrolet Onix, podrÃ¡s disfrutar la Ãºltima versiÃ³n de ChevyStar con sistemas de entretenimiento MyLink.\r\nEl Chevrolet Onix hace que todo sea mÃ¡s fÃ¡cil. Disfruta de vidrios automÃ¡ticos con One Touch, bloqueo desde asiento delantero y espejos laterales que puedes controlar electrÃ³nicamente. Aprovecha ademÃ¡s todos los beneficios de ChevyStar, el servicio exclusivo de Chevrolet creado para facilitar tu vida.', 'img/onix/onixt.jpg', 'Siempre en primer lugar:\r\nAirbag para conductor y pasajero, ABS, Chevystar, sensor de reversa, alarma, apertura a distancia y bloqueo central, luces frontales led y exploradoras.\r\nAtributos estÃ¡ndar como barras de puertas, cinturones de seguridad en todos los asientos de 3 puntos en extremos, de 2 puntos en asiento central.\r\nEl Chevrolet Onix cuenta con sensor de reversa, luces frontales LED y exploradoras, apertura a distancia y bloqueo central, alarma, Airbag para conductor y pasajero, Frenos ABS, barras de puertas y cinturones de seguridad en todos los asientos, de 3 puntos en extremos y 2 puntos en asiento central. Todo para que tÃº puedas conducir con total tranquilidad.', 'img/onix/onixs.jpg', 'img/onix/onix.jpg', 'segmentos/medianos/img/o.jpg', 'La tecnologÃa y la conectividad que tu vida te exige, la encuentras en el nuevo Chevrolet Onix; el carro diseÃ±ado para hoy ya que cuentas con opciones de personalizaciÃ³n y las apps Apple CarPlay® y Android Auto®.'),
(5, 5, 'NUEVO DISEÃ±O CHEVROLET SONIC SEDÃ¡N 2017.\r\n\r\nDisfruta la ciudad en un carro joven, con un diseÃ±o completamente renovado y lo Ãºltimo en conectividad. Descubre la nueva generaciÃ³n de ChevyStar y los diferentes avances en seguridad para que manejes siempre tranquilo. DisfrÃºtalo en versiÃ³n mecÃ¡nica y automÃ¡tica secuencial.', 'img/sonic/b.png', 'El nuevo diseÃ±o exterior del Chevrolet Sonic SedÃ¡n es fresco y sus formas aerodinÃ¡micas y lÃneas agresivas juegan perfectamente con el ambiente de la ciudad. DisfrÃºtalo con sunroof en todas sus versiones para que no te pierdas ningÃºn momento de tu viaje.\r\n\r\nCon un exterior completamente renovado, el Chevrolet Sonic representa el nuevo ADN de la marca Chevrolet: TecnologÃa, diseÃ±o y desempeÃ±o. No nos perdimos de ningÃºn detalle: Nuevo capÃ³, nueva parrilla frontal, nuevo panel de instrumentos, limpiabrisas trasero, un tercer stop, luces LED posteriores y de circulaciÃ³n diurna.', 'img/sonic/sonicex.jpg', 'El diseÃ±o interior del Chevrolet Sonic respira tecnologÃa. Su tablero de instrumentos fue renovado y combina con el computador a bordo. Su amplio diseÃ±o interior cuenta con una cojinerÃa en tela deportiva que hace juego con el panel frontal y sus acabados en color aluminio. Disfruta de Apple CarPlay y Android auto para que sincronizar tu celular y la pantalla Chevystar MyLink con pantalla de 7¨.', 'img/sonic/sonicit.jpg', 'El Chevrolet Sonic SedÃ¡n viene con un motor 1.6 L y 16 vÃ¡lvulas para que disfrutes al mÃ¡ximo la ciudad. El vehÃculo cuenta en la versiÃ³n automÃ¡tica con seis marchas adelante y opciÃ³n secuencial, y cinco marchas adelante en su versiÃ³n mecÃ¡nica. Sus cambios estÃ¡n perfectamente relacionados para el trÃ¡fico de la ciudad y carretera, lo que junto a su eficiente motor logran un consumo Ã³ptimo de combustible. Cuenta con un sistema de frenado ABS y su relaciÃ³n de Peso/Potencia es excelente.', 'img/sonic/sonicd.jpg', 'El Chevrolet Sonic facilita tu vida, con tecnologÃas como Passive Entry donde no necesitas usar la llave para entrar a tu Chevrolet; con solo tenerla y acercarte al vehÃculo se desbloquearÃ¡n los seguros de las puertas. Y Passive Start en la que podrÃ¡s encender el motor con tan solo oprimir un botÃ³n.\r\nDisfruta tambiÃ©n de vidrios automÃ¡ticos con One Touch y bloqueo desde asiento delantero, los espejos laterales los puedes controlar electrÃ³nicamente. Aprovecha ademÃ¡s todos los beneficios de ChevyStar, el servicio exclusivo de Chevrolet creado para facilitar tu vida', 'img/sonic/sonict.jpg', 'El nuevo Chevrolet Sonic SedÃ¡n cuentas con frenos ABS, 4 airbags en la versiÃ³n automÃ¡tica, 2 en la mecÃ¡nica, sensor de parqueo y de lluvia, alama y bloqueo a distancia. Tu Chevrolet Sonic cuenta con barras de seguridad en cada asiento y columna de direcciÃ³n con absorciÃ³n de impacto. AdemÃ¡s con los controles de llamada y radio en el timÃ³n podrÃ¡s mantener tu atenciÃ³n en el camino para que disfrutes cada minuto de tu viaje.', 'img/sonic/sonics.jpg', 'img/sonic/sonic.jpg', 'segmentos/medianos/img/so.jpg', 'LlegÃ³ la diversiÃ³n que estabas esperando con un nuevo diseÃ±o, motor de 1,6 litros y 16 vÃ¡lvulas.'),
(6, 6, 'HECHA PARA LA CIUDAD: La nueva Chevrolet Tracker te permite crear tu propia versiÃ³n de tu ciudad con un diseÃ±o completamente renovado', 'img/tracker/trackerin.jpg', 'DISEÃ±O TOTALMENTE NUEVO', 'img/tracker/trackerex.png', 'Nuevo diseÃ±o interior en: \r\n<br>-ClÃºster de 3.\'\', panel y tablero de instrumentos\r\n<br>-Asiento trasero abatible 60/40 \r\n<br>-Llave inteligente de presencia con botÃ³n encendido y acceso sin llave \r\n<br>Seguros, ventanas y espejos elÃ©ctricos\r\n<br>-Asiento de conductor con ajuste elÃ©ctrico multidireccional de 6 posiciones', 'img/tracker/trackerit.jpg', 'DEFINE TU CAMINO:\r\nLa ciudad tiene muchas caras y de una calle a otra puedes encontrarte con un mundo diferente, por eso Chevrolet Tracker tiene la tecnolog&iacute;a adecuada para adaptarse a cada una de ellas con un motor de 1,8L y 4 cilindros.\r\n ', 'img/tracker/trackerd.jpg', 'Disfruta de toda la tecnolog&iacute;a de Chevrolet Tracker:\r\n<br>-La integraciÃ³n con tu smartphone te permite disfrutar de las aplicaciones desde la pantalla del carro. \r\n<br>-Pantalla tÃ¡ctil de 7\'\'\r\n<br>-Sistema de infoentretenimiento Chevystar MyLink, Apple Carplay & Android Auto. \r\n<br>-Entrada USB, Bluetooth, dispositivo auxiliar o MP3\r\n<br>-Sistema de audio con 6 bocinas', 'img/tracker/trackert.jpg', 'Chevrolet Tracker cuenta con tecnolog&iacute;a para mantenerte seguro en cada viaje como:\r\n<br>-Sistema de control de tracciÃ³n\r\n<br>-Frenos ABS\r\n<br>-Sistema de control de estabilidad Stabilitrak\r\n<br>-6 bolsas de aire\r\n<br>-Control de frenado en curva\r\n<br>-DistribuciÃ³n electrÃ³nica de frenado\r\n<br>-Asistente de arranque en pendiente\r\n<br>-Sensores de reversa y disponible cÃ¡mara de reversa', 'img/tracker/trackers.jpg', 'img/tracker/tracker.jpg', 'segmentos/camionetas/img/tr.jpg', 'Perfecta para la ciudad en la que quieres vivir. Descubre su diseÃ±o renovado y los avances en seguridad y tecnologÃa de esta camioneta urbana y moderna.'),
(7, 7, 'En la Captiva Sport 3.0 el lujo y la potencia conviven en un solo vehÃculo. Tu vida estÃ¡ a punto de cambiar gracias al sensor de reversa, que junto con sus sensores y alarma acÃºstica facilita las maniobras de estacionamiento al detectar objetos. TambiÃ©n despliega la informaciÃ³n de la cÃ¡mara de visiÃ³n trasera en el espejo retrovisor. La experiencia en la Captiva Sport te sorprenderÃ¡ mucho mÃ¡s por sus asientos con calefacciÃ³n, sunroof elÃ©ctrico y sistema de audio premium con 10 parlantes.', 'img/captiva/captivaintro.jpg', 'DiseÃ±o imponente\r\nCaptiva Sport tiene espacio para cinco pasajeros y todo su equipaje, pero su belleza no acaba en el interior. Con su diseÃ±o estilizado y deportivo, Captiva Sport no sÃ³lo es divertida de manejar, sino tambiÃ©n agradable a la vista. Alta e imponente, su estilo se completa con barras laterales del color de la carrocerÃa y su spoiler trasero invita a manejar. No importa si tu estilo es urbano o deportivo: esta todoterreno estÃ¡ lista para que lleves tu mundo contigo.', 'img/captiva/captivaex.jpg', 'DiseÃ±ada para adaptarse a tu vida\r\nYa sea que tengas una familia de cinco personas, o mÃ¡s bien, necesites espacio adicional para llevar mÃ¡s cosas, Captiva Sport es tu mejor opciÃ³n. Captiva Sport cuenta con capacidad para llevar 5 pasajeros y su equipaje cÃ³modamente, sin embargo si necesitas mÃ¡s espacio, te da la posibilidad de abatir sus sillas traseras en 60/40 y asÃ tener mÃ¡s capacidad de carga cuando la requieras.', 'img/captiva/captivait.jpg', 'Todo el poder para las aventuras\r\nLa Chevrolet Captiva Sport 3.0 estÃ¡ equipada con un motor de 6 cilindros en “V” de 3.0 litros, 24v DOHC, con inyecciÃ³n electrÃ³nica multipunto de combustible. Alcanza una potencia mÃ¡xima de 264 hp a 6950 rpm y un torque de 300 Nm a 5100 rpm. El mismo estÃ¡ acoplado a una transmisiÃ³n automÃ¡tica de seis velocidades y tiene tracciÃ³n en las cuatro ruedas (AWD).\r\nInteligencia y eficiencia\r\nCaptiva Sport ofrece una transmisiÃ³n automÃ¡tica de 6 velocidades, que tambiÃ©n brinda la posibilidad de manejo manual buscando una opciÃ³n mas deportiva. Cuenta con un sistema inteligente que utiliza lo mejor del motor, con toda la comodidad para cambiar marchas sin esfuerzo y con la mÃ¡xima eficiencia. Adicionalmente cuenta con TecnologÃa ECO MODE que permite optimizar el consumo de combustible.', 'img/captiva/captivad.jpg', 'El sistema de audio premium con 10 parlantes y subwoofer es sÃ³lo el comienzo de una larga lista de elementos que componen la tecnologÃa de Captiva Sport. El espejo retrovisor electrocrÃ³mico se atenÃºa automÃ¡ticamente y elimina el deslumbramiento nocturno, mientras que el control electrÃ³nico de velocidad crucero simplifica la conducciÃ³n en carretera. Para mayor seguridad, una cÃ¡mara de visiÃ³n trasera asiste visualmente al conductor en el momento de estacionarse en reversa. Adicionalmente cuenta con el sistema de entretenimiento Mylink de Chevystar que integra una pantalla tÃ¡ctil, es compatible con smarthphones y te permite interactuar con el sistema a travÃ©s de BT y comando de voz.', 'img/captiva/captivat.jpg', 'Captiva Sport fue diseÃ±ada pensando en la seguridad de tu familia. Para ayudar a evitar colisiones, todos los modelos vienen equipados con frenos a disco y sistema antibloqueo en las 4 ruedas, sistema de control electrÃ³nico de estabilidad StabiliTrak®, control de tracciÃ³n y  DistribuciÃ³n ElectrÃ³nica de Frenado.\r\nEl sistema StabiliTrak® mejora la estabilidad del vehÃculo en la mayorÃa de superficies de carretera, especialmente durante las maniobras de emergencia y en condiciones de conducciÃ³n difÃciles, tales como  pavimento mojado o pedregoso. StabiliTrak® compara tu modo de manejar con la respuesta del vehÃculo y, si es necesario, hace aplicaciones pequeÃ±as de freno y ajusta el torque del motor para mantenerte sobre el camino.', 'img/captiva/captivas.jpg', 'img/captiva/captiva.jpg', 'segmentos/camionetas/img/captivasport.jpg', 'Que tu familia necesita, ademÃ¡s de toda la seguridad y comodidad que el mejor amigo de tu familia merece. 		 		Trae todo el espacio y elegancia '),
(8, 8, 'Chevrolet DMAX una 4X2 durable y con gran desempeÃ±o para aquellos quienes hacen su propio camino. Seguridad y confort a tu paso.', 'img/dmax/dmaxin.png', 'LlegÃ³ para quedarse\r\nDiseÃ±o moderno con luces frontales halÃ³genas, parachoques delantero color carrocer&iacute;a y posterior color negro, con rin 15 pulgadas.\r\nSus l&iacute;neas exteriores inspiradas en la espada japonesa \"Katana\" sumado a su bajo coeficiente aerodinÃ¡mico (Cd=0.47), le permite mejor desplazamiento y menor resistencia al viento.', 'img/dmax/dmaxex.jpg', 'Con amplio espacio y confort interior, cada detalle fue desarrollado para ergonom&iacute;a del conductor y los pasajeros. Chevrolet DMAX con direcciÃ³n hidrÃ¡ulica, vidrios elÃ©ctricos, aire acondicionado, apoyabrazos traseros centrales, volante de direcciÃ³n ajustable en altura y cabina insonorizada.\r\n', 'img/dmax/dmaxit.jpg', 'Un motor capaz de superar cualquier tarea.\r\nSu motor te lleva donde tu imaginaciÃ³n alcance. 2.5 L, 130 HP y torque plano 320Nm (1800-2800rpm) para moverte al ritmo que desees. Su chasis estÃ¡ concebido para ser mÃ¡s resistente al trabajo pesado. Su suspensiÃ³n delantera tipo MacPherson y doble horquilla la hacen ideal para la comodidad de sus ocupantes; y la suspensiÃ³n trasera tipo ballesta de doble etapa y amortiguador la hacen mÃ¡s que ideal para todo tipo de carga y trabajo pesado. Gracias a este diseÃ±o Chevrolet DMAX posee muy buenos Ã¡ngulos de entrada (21\') y salida (17,2\').\r\nMotor diesel electrÃ³nico common-rail con sistema de turbo de geometr&iacute;a variable e intercooler de 2,5L con toda la fuerza para hacer tu propio camino.\r\nChevrolet DMAX Cabina Doble - Torque\r\n Chevrolet DMAX Cabina Doble - Motor', 'img/dmax/dmaxd.jpg', 'Tecnolog&iacute;a para acompaÃ±ar el trabajo.\r\nMejora tu d&iacute;a con la Ãºltima tecnolog&iacute;a: radio pantalla tÃ¡ctil con GPS, CD, MP3, USB, entrada AUX y dos parlantes.\r\nAdemÃ¡s lleva a la mano todos los beneficios que Chevystar pone a tu alcance como lo son: apertura de puertas v&iacute;a Smartphone, asistente personal a bordo, comando de voz para llamadas v&iacute;a Bluetooth, entre otros.', 'img/dmax/dmaxt.jpg', 'SEGURIDAD\r\nCuando estÃ¡s a bordo de una Chevrolet DMAX estÃ¡s rodeado de medidas de protecciÃ³n como lo son: sistema de frenos ABS y EBD, sistema ISOFIX, seguros de niÃ±os en puertas traseras, airbags frontales para conductor y pasajero, barras de seguridad en puertas laterales, cinturones de seguridad de tres puntos, columna de direcciÃ³n colapsable, gancho delantero para remolque y protector de cÃ¡rter. AdemÃ¡s su nuevo chasis de acero ultra resistente y tope para llantas delanteras que protege el habitÃ¡culo ante cualquier eventualidad.\r\nCon Chevrolet DMAX cuentas con todo el respaldo de Chevystar, que te permitirÃ¡ tener acceso a: bloqueo central, localizaciÃ³n satelital, servicio de acompaÃ±amiento en ruta, alerta de parqueo, alerta de velocidad, asistencia mÃ©dica y mecÃ¡nica, encendido de motor con cÃ³digo de seguridad; ademÃ¡s de la promesa de valor de la marca: si te lo roban lo recuperamos; si no te entregamos uno nuevo (aplican condiciones y restricciones, no incluye modificaciones de carrocer&iacute;a ni accesorios).', 'img/dmax/dmaxs.jpg', 'img/dmax/dmax.jpg', 'segmentos/camionetas/img/dmax2.jpg', 'Creada para llegar a donde ningÃºn auto puede. Una 4x4 ideal para cualquier tipo de trabajo, tecnologÃa Chevystar MyLink y radio touch con GPS.'),
(9, 9, 'MÃ¡S QUE UNA VAN, UNA OPORTUNIDAD.\r\n\r\nQuien administra una empresa sabe que unir productividad a econom&iacute;a es la mejor ecuaciÃ³n. Por eso, puedes contar con la Van N300 Cargo. Con un generoso volumen de carga de 3.6m3, y hasta 550kg de capacidad de carga Ãºtil, ella ofrece un motor de 1.2L de 81 hp que enfrenta el trabajo duro con toda la facilidad y, sobre todo, con econom&iacute;a. Son tantas las ventajas, que ya no estamos hablando de una Van, sino de una oportunidad de negocio.', 'img/sparkgt/b.png', 'El diseÃ±o de la capacidad\r\nToda la capacidad de carga de la Van N300 Cargo se refleja en sus l&iacute;neas objetivas. Las formas rectas y harmÃ³nicas privilegian la creaciÃ³n de un espacio interno sin obstÃ¡culos para el almacenamiento de mercanc&iacute;as. Y todo esto sin dejar de lado la preocupaciÃ³n estÃ©tica, que aparece en &iacute;tems como los parachoques del color de la carrocer&iacute;a, la grilla delantera con el emblema dorado y las puertas laterales deslizantes, que facilitan el acceso a la carga.\r\nPequeÃ±a van. Grandes posibilidades.\r\nLa Van N300 Cargo parece compacta, pero es muy grande. Son 4 metros de largo, 1,62 m de ancho, 1,90 m de altura y una distancia entre ejes de 2,70 m. Todo esto resulta en una capacidad de carga volumÃ©trica de 3,6 metros cÃºbicos.', 'img/van/vancex.jpg', 'Todo el confort para quien trabaja\r\nPara quien se gana la vida al volante, el carro es un verdadero local de trabajo. Nada mÃ¡s justo que garantizar el confort y el bienestar en este ambiente. Por esto, la Van N300 Cargo ofrece &iacute;tems como asientos reclinables y deslizables con apoyacabezas, calefacciÃ³n, cenicero y encendedor de cigarrillos, ademÃ¡s de la opciÃ³n de aire acondicionado en la versiÃ³n Plus.\r\nPoder y espacio.\r\nVan N300 Cargo es una herramienta importante que puede hacer mucho por tu empresa o por tu negocio. Y lo que prueba esto son los nÃºmeros: 550 Kg de capacidad de carga Ãºtil. Pero no se trata solamente del poder de llevar grandes pesos. El volumen del Ã¡rea de carga es de 3.6 metros cubicos, libres de obstÃ¡culos gracias a la gran distancia entre ejes de 2,7 m y un diseÃ±o limpio.', 'img/van/vancit.jpg', 'Tu fuerza de trabajo\r\nCon un motor 1.2L, la Van N300 Cargo tiene fuerza en la medida adecuada para enfrentar cualquier trabajo o tarea. EstÃ¡ dotada de cuatro cilindros, 16 vÃ¡lvulas e inyecciÃ³n electrÃ³nica multipunto, desarrollando la potencia de 81 hp y torque de 108 Nm. Y todo esto respetando la norma Euro IV de emisiones. AdemÃ¡s de confiable, estÃ¡ al d&iacute;a con el medio ambiente.', 'img/van/vancd.jpg', 'Porque tambiÃ©n es importante divertirse\r\nCon toda la capacidad, espacio y fuerza que ofrece, la Van N300 Cargo es perfecta para el trabajo. Pero quien trabaja sabe que relajarse tambiÃ©n es muy importante. Por esto ella viene equipada con radio AM/FM, CD y MP3, dos parlantes y antena, para garantizar momentos de relajaciÃ³n durante el d&iacute;a.', 'img/van/vanct.jpg', 'PrevenciÃ³n:\r\nLa Van N300 Cargo fue pensada para ofrecer la mÃ¡xima seguridad, tanto al trabajar como al manejar en el trÃ¡fico. Por eso, viene equipada con una rejilla de protecciÃ³n separando el espacio de carga del habitÃ¡culo del conductor y del pasajero, cuenta con trabas de seguridad para niÃ±os en las puertas traseras, tercera luz de stop, frenos ABS y apertura remota de la tapa de combustible', 'img/van/vancs.jpg', 'img/van/vanc.jpg\r\n\r\n', 'segmentos/van/img/car.jpg', 'Van N300 Cargo es una verdadera oportunidad de negocio. Con capacidad de 550 Kg de carga Ãºtil y motor de 81 HP, gran capacidad y economÃa.'),
(10, 10, 'PARA EL TRABAJO, PARA LA FAMILIA.\r\nPara tus negocios y para la familia ya cuentas con la Van N300 Pasajeros. Con un excelente espacio interno, capacidad de hasta 8 pasajeros y excelente acabado, tiene un motor 1.2 L de 81 hp que provee potencia a la medida y con mucha econom&iacute;a. Si haces cuentas, vas a ver que esta Van es una soluciÃ³n que se multiplica por dos, atendiendo las necesidades de tu familia y de tu trabajo.', 'img/van/vanpin.jpg', 'Estilo versÃ¡til:\r\nSea cual sea su uso, la Van N300 Pasajeros saca la cara cuando se trata de estilo. Sus l&iacute;neas rectas crean un estilo sutil y equilibrado que incluso ayuda a promover un espacio interno sin obstÃ¡culos. Grandes faros de lente transparente se destacan en la delantera, los parachoques pueden ser blancos o plateados, ademÃ¡s de ofrecer manijas en negro o del color de la carrocer&iacute;a en la versiÃ³n full, que tambiÃ©n cuenta con neblineros empotrados en el parachoques delantero.\r\nCompletando su estilo, la versiÃ³n Semi Full cuenta con llantas de 14\'\' en acero mientras que la versiÃ³n Full las trae en aluminio.\r\nCada vez mÃ¡s espacio.\r\nLa Van N300 Pasajeros parece compacta, pero es muy grande. Son 4 metros de largo, 1,62 de ancho, 1,90 m de altura y una distancia entre ejes de 2,70 m. As&iacute; ella consigue abrigar segunda y tercera fila de asientos sumando espacio para 8 pasajeros en la versiÃ³n semi full, y 7 pasajeros en la versiÃ³n full.', 'img/van/vanpex.jpg', 'Comodidad para todos:\r\nCalefacciÃ³n, asientos reclinables, ventanas con apertura manual en las puertas posteriores, desempaÃ±ador trasero, portavasos, radio con CD/MP3 y dos parlantes forman parte del completo equipamiento de la l&iacute;nea Van N300 de Chevrolet. La versiÃ³n full ofrece aire acondicionado, direcciÃ³n asistida hidrÃ¡ulicamente, vidrios delanteros elÃ©ctricos y bloqueo central.\r\nPoder de multiplicar el espacio\r\nNo importa la situaciÃ³n o el uso que elijas. En la Van N300 Pasajeros, espacio es lo que no falta. Con la segunda y tercera fila de asientos tipo banca, las 2 versiones de esta camioneta tienen capacidad para 7 pasajeros. En la versiÃ³n Full, la segunda fila cuenta con asientos tipo Captain Seat para brindarte toda esta capacidad siempre con mucho confort.', 'img/van/vanpit.jpg', 'Poder siempre a disposiciÃ³n\r\nCon un motor 1.2L, la Van N300 Pasajeros tiene fuerza en la medida correcta para enfrentar cualquier trabajo o viaje de familia. EstÃ¡ dotada con cuatro cilindros, 16 vÃ¡lvulas e inyecciÃ³n electrÃ³nica multipunto, desarrollando una potencia de 81 hp y torque de 108 Nm. Y todo eso respetando la norma Euro IV de emisiones. De manera que, siempre que la exijas, va a responder a la altura, respetando el medio ambiente.', 'img/van/vanpd.jpg', 'Porque diversiÃ³n tambiÃ©n es esencial\r\nCuando se lleva hasta 8 pasajeros en un carro, la diversiÃ³n es algo muy importante. Por eso la Van N300 Pasajeros mantiene el ambiente siempre ligero, y viene equipada con radio AM/FM, CD y MP3, dos parlantes (cuatro en la versiÃ³n full) y antena, para garantizar momentos de diversiÃ³n durante el d&iacute;a.', 'img/van/vanpt.jpg', 'PrevenciÃ³n\r\nLa Van N300 Pasajeros fue pensada para ofrecer la mÃ¡xima seguridad, tanto al trabajar como al manejar en el trÃ¡fico. Por eso, viene equipada con seguro de niÃ±os en las puertas traseras y tercera luz de stop, ademÃ¡s de apertura remota de la tapa de combustible. La versiÃ³n Full viene equipada con cierre central con accionamiento remoto en la llave, ademÃ¡s de frenos ABS.', 'img/van/vanps.jpg', 'img/van/vanp.jpg', 'segmentos/van/img/van.jpg', 'Van N300 Pasajeros es una soluciÃ³n para la familia y para los negocios. Con capacidad total de hasta 8 pasajeros, y motor de 81 hp, comodidad, estilo y economÃa.'),
(11, 11, 'DESAFÃO CHEVROLET CRUZE.\r\nLa tecnologÃa y el desempeÃ±o del nuevo Chevrolet Cruze fue puesto a prueba para desafiar todo lo que creÃas. DescÃºbrelo aquÃ.', 'img/premium/cruzein.jpg', 'DISFRUTA EL NUEVO CHEVROLET CRUZE EN 360º.\r\nPorque cada detalle cuenta, conoce la nueva generaciÃ³n del Chevrolet Cruze desde todos sus Ã¡ngulos', 'img/premium/cruzeex.jpg', 'DISFRUTA EL NUEVO CHEVROLET CRUZE EN 360º.\r\nPorque cada detalle cuenta, conoce la nueva generaciÃ³n del Chevrolet Cruze desde todos sus Ã¡ngulos', 'img/premium/cruzet.jpg', NULL, 'img/premium/cruzed.jpg', 'Chevystar es una tecnologÃa exclusiva de Chevrolet.\r\nOprimiendo el botÃ³n del retrovisor del Chevrolet Cruze, un asistente a bordo puede ayudar al conductor y sus pasajeros en tiempo real buscando puntos de interÃ©s, informaciÃ³n, asistencia de emergencias, recuperaciÃ³n del vehÃculo y comandos de envÃo remotos', 'img/premium/cruzet.jpg', '', 'img/premium/cruzes.png', 'img/premium/cruze.jpg', 'segmentos/premium/img/cruzenn.jpg', 'La Nueva GeneraciÃ³n del Chevrolet Cruze ha llegado para desafiar el sentido comÃºn. Con la mÃ¡s alta tecnologÃa Chevrolet, para una experiencia de manejo inigualable.'),
(12, 12, 'NUEVA CHEVROLET TRAILBLAZER.\r\nLa nueva Chevrolet Trailblazer, una SUV completa con la que puedes descubrir nuevas posibilidades y nuevos\r\nterrenos. Es la combinaciÃ³n perfecta entre tecnolog&iacute;a, rendimiento, diseÃ±o y seguridad que te permite llegar\r\na donde quieras.', 'img/premium/train.jpg', 'El nuevo diseÃ±o exterior de la Trailblazer representa el actual ADN de la marca Chevrolet, con un aspecto mÃ¡s robusto gracias a sus l&iacute;neas, su nueva parrilla doble y la combinaciÃ³n con las luces tipo LED. Sus llantas de 18\" y sus estribos laterales ademÃ¡s de darle un aspecto mÃ¡s dinÃ¡mico, son caracter&iacute;sticas exteriores propias de una todoterreno', 'img/premium/traiex.jpg', 'Dentro de la Trailblazer vas a encontrar un conjunto de los mejores materiales en cada detalle, tanto en sus siete confortables asientos en cuero, como en su computadora y su panel tÃ¡ctil de a bordo.', 'img/premium/trait.jpg', 'La nueva Chevrolet Trailblazer es una poderosa SUV con un motor Duramax 2.8 Turbodiesel 197 Hp y 500 Nm de Torque. Esta 4X4 con chasis independiente y Shift on the fly, cuenta con 10 asistencias off-road; como el Control de Descenso en Pendiente (HCD), Asistente de Arranque en Pendiente (HSA), Control ElectrÃ³nico de Estabilidad (ESC), que te permiten retar los l&iacute;mites y explorar hasta donde puedes llegar.', 'img/premium/traid.jpg', 'TECNOLOGIA CHEVYSTAR', 'img/premium/traitt.jpg', 'Todo en esta SUV fue pensado en nombre de la seguridad, para garantizar que de lo Ãºnico que tengas que preocuparte es del prÃ³ximo camino que quieres explorar. Cuenta con avanzados asistentes de seguridad como: el Asistente de Permanencia de Carril, Alerta de ColisiÃ³n Frontal, Alerta de Punto Ciego, Indicador de Distancia del Veh&iacute;culo, y cÃ¡maras de parqueo delanteras y traseras. AdemÃ¡s de 6 airbags y luz LED diurna que te dan la confianza y potencia que necesitas para retar los l&iacute;mites.', 'img/premium/trais.jpg', 'img/premium/trai.jpg', 'segmentos/premium/img/trail.jpg', 'La nueva Chevrolet Trailblazer es la verdadera todoterreno. Con un gran espacio interior,posee la versatilidad y potencia necesaria para superar el asfalto y cualquier terreno que se te ocurra.'),
(13, 13, 'CHEVROLET TRAVERSE.\r\nUna camioneta de diseÃ±o vanguardista y a la vez deportivo, con tres filas de asientos para transportar cÃ³modamente hasta 8 pasajeros, un motor V6 de 3.6 litros con una potencia mÃ¡xima de 281 HP, y un excelente nivel de confort.\r\nLa nueva Chevrolet Traverse LS ofrece cinco kits de accesorios para personalizarla de acuerdo al estilo de vida de cada familia.', 'img/premium/travin.png', 'Con un exterior que cuenta con KITS de personalizaciÃ³n.', 'img/premium/travex.jpg', 'Comodidad para los pasajeros.\r\nChevrolet Traverse piensa en la comodidad y en el bienestar de todos los pasajeros. Por eso, cuenta con tres filas de asientos y sistema Smart Slide para fÃ¡cil acceso.', 'img/premium/travt.jpg', 'El rendimiento no sufre en nombre de la eficiencia.\r\nChevrolet Traverse, se encuentra equipada con un potente motor 3.6L V6 de inyecciÃ³n directa (SIDI) y (VVT)*, diseÃ±ado para entregar una sorprendente potencia mÃ¡xima de 281HP@6.300 RPM, lo cual le permite tener un excelente desempeÃ±o aÃºn en condiciones pesadas de carga o arrastre. Su tren motriz se complementa con una transmisiÃ³n automÃ¡tica de 6 velocidades con cambios secuenciales que ofrecen una opciÃ³n de conducciÃ³n mÃ¡s deportiva, a lo que se suma a su tracciÃ³n All Wheel Drive (AWD) o tracciÃ³n en las 4 llantas para los diferentes caminos por recorrer.', 'img/premium/travd.jpg', 'TECNOLOG&iacute;A DE CHEVYSTAR.\r\nCuenta con el sistema Chevystar Connect que te ofrece 21 servicios para que estÃ©s acompaÃ±ado vayas donde vayas. Puedes estar tranquilo en la v&iacute;a ya que tienes a tu disposiciÃ³n servicios de acompaÃ±amiento y seguridad como monitoreo en ruta, rastreo, ubicaciÃ³n, inmovilizaciÃ³n, localizaciÃ³n, apertura remota de puertas y recuperaciÃ³n de tu Chevrol', 'img/premium/travt.jpg', 'Y nada mÃ¡s importante para la familia que la seguridad, por eso la Nueva Traverse LS cuenta con 6 Airbags, Anclajes isofix para las sillas de los niÃ±os y bebÃ©s, frenos de disco en las 4 llantas, Control de estabilidad Stabilitrack, Control de tracciÃ³n, ABS y EBD para evitar accidentes y garantizar la seguridad de sus ocupantes', 'img/premium/travs.jpg', 'img/premium/trav.jpg', 'segmentos/premium/img/traverse2.jpg', 'Chevrolet Traverse, una camioneta diseÃ±ada para que toda tu familia viaje con seguridad, comodidad y lo mejor del entretenimiento a bordo.'),
(14, 14, 'UNA SUV TODO TERRENO CON TODO EL CONFORT PREMIUM.\r\nSu diseÃ±o exterior impresiona a primera vista. Chevrolet Tahoe es la SUV grande capaz de llegar a donde pocos pueden. Cuenta con la tecnolog&iacute;a, el diseÃ±o y el desempeÃ±o para brindarte una experiencia de manejo incre&iacute;ble. Visita nuestros concesionarios y conoce la nueva Chevrolet Tahoe.', 'img/premium/tahin.jpg', 'Imponente diseÃ±o exterior.\r\nLas nuevas l&iacute;neas exteriores de su carrocer&iacute;a serÃ¡n lo primero que notarÃ¡s en la rediseÃ±ada Chevrolet Tahoe. AdemÃ¡s tiene una nueva parrilla frontal doble, faros envolventes con luces de mayor penetraciÃ³n e intensidad, luces de posiciÃ³n de LED, spoiler trasero donde se oculta el parabrisas y estribos laterales.\r\nCada uno de los elementos de Chevrolet Tahoe contribuye al estilo y funcionalidad de esta refinada SUV grande que ademÃ¡s de un diseÃ±o elegante, es totalmente aerodinÃ¡mica.', 'img/premium/tahex.jpg', '', 'img/premium/tahit.jpg', 'UNA EXPERIENCIA DE DISEÃ±O GRATIFICANTE.\r\nPotencia y eficiencia para alto desempeÃ±o:\r\nEl motor Ecotec de 5,3 L direct injection con Active Fuel Management (AFM) de la Tahoe, fue recientemente desarrollado por General Motors y es poderosamente eficiente entregando 355 caballos de potencia y un torque de 519 Nm, que en conjunto con su diseÃ±o aerodinÃ¡mico y su novedosa calibraciÃ³n de la transmisiÃ³n automÃ¡tica permiten un menor consumo de combustible aproximadamente 10% menos que su versiÃ³n anterior.', 'img/premium/tahd.jpg', 'INNOVACIÃ³N EN TECNOLOGÃA.\r\nLa tecnolog&iacute;a viaja contigo. Pensando en tu comodidad y entretenimiento, la nueva Chevrolet Tahoe viene con: Acceso remoto keyless entry, botÃ³n start/stop para encendido, apertura de baÃºl handsfree, Chevystar MyLink + Reproductor DVD Blu-Ray, Sistema de audio Bose, Charging pad inalÃ¡mbrico*, sillas abatibles elÃ©ctricamente en segunda y tercera fila, memorias de ajuste de posiciÃ³n del conductor y mucho mÃ¡s.\r\n*No todos los celulares son compatibles, pueden requerir de accesorios adicionales.', 'img/premium/taht.jpg', 'Seguridad de punta a punta.\r\nAparte de los 7 airbags y los diferentes sistemas activos de seguridad, la Chevrolet Tahoe incluye tecnolog&iacute;as innovadoras para la asistencia al conductor, fundamentÃ¡ndose en el uso de cÃ¡maras ubicadas estratÃ©gicamente y sensores distribuidos a lo largo del veh&iacute;culo para alertar a los conductores sobre riesgos potenciales en los 360 grados. Por ejemplo, la Tahoe utiliza vibraciones direccionales en el asiento del conductor y notificaciones sonoras, para alertarlo de una posible colisiÃ³n. Entre estas asistencias estÃ¡n: Alerta de ColisiÃ³n Frontal, Alerta de Cambio de Carril, Alerta de TrÃ¡fico en Parte Posterior y Sensor de Punto Ciego.', 'img/premium/tahs.jpg', 'img/premium/tah.jpg', 'segmentos/premium/img/taho.jpg', 'Su diseÃ±o exterior impresiona a primera vista. Chevrolet Tahoe® es la SUV grande capaz de llegar a donde pocos pueden. Cuenta con la tecnologÃa para brindarte una experiencia de manejo increÃble.'),
(15, 15, 'LA REALIDAD NUNCA FUE TAN PODEROSA.\r\nEl nuevo Chevrolet Camaro SIX SS materializa una leyenda contemporÃ¡nea que hÃ© evolucionado a travÃ©s de 5 emblemÃ¡ticas generaciones. Es el protagonista de los cuartos de milla que serÃ¡ nuevo referente de desempeÃ±o.', 'img/premium/camaroin.jpg', 'DISEÃ±O DE ALTO DESEMPEÃ±O.\r\nLos resultados de extensas horas de pruebas en el tÃºnel de viento se pueden encontrar en todas las lÃneas meticulosamente afinadas del rediseÃ±ado Chevolet Camaro Six SS. Desde la apertura mÃ¡s amplia y el preciso Ã¡ngulo de inclinaciÃ³n lateral de la parrilla, hasta el sorprendente canal de “mohawk invertido” en el capÃ³, pasando por el puntal del alerÃ³n que ayuda a reducir la elevaciÃ³n, se analizÃ³ el diseÃ±o de cada superficie y borde hasta comprobar que mejoraba el flujo de aire y el rendimiento del vehÃculo.\r\n ', 'img/premium/camaroex.jpg', 'DISEÃ±O INTERIOR CON PERSONALIDAD\r\nLos materiales de primera calidad, y costuras hechas a mano le dan un nÃvel de sofisticaciÃ³n superior al interior del Chevrolet Camaro Six SS. SiÃ©ntate al voltante y descubrirÃ¡s que todo el diseÃ±o estÃ¡ optimizado para un mejor rendimento en la conducciÃ³n. Con un estilo mucho mÃ¡s acentuado que incluye:\r\n\r\n*Aire Acondicionando bizona con controles de temperatura en el diÃ¡metro exterior de los ductos de aire de la consola central que reemplazÃ¡n a los botones para una intervensiÃ³n mÃ¡s rÃ¡pida e intuitiva.\r\n\r\n*Materiales Premium, texturas enriquecidas y costuras personalizables.', 'img/premium/camarot.jpg', 'DESEMPEÃ±O INTELIGENTE.\r\nUna experiencia de alto desempeÃ±o que pone a prueba los sentidos. Gracias entre otras cosas a su diseÃ±o mÃ¡s liviano, que en conjunto con su motor completamente en aluminio entrega 455 HP caballos de potencia y un torque de 617 Nm', 'img/premium/camarod.jpg', 'TECNOLOGÃA A PRUEBA DE VELOCIDAD.\r\nEl Chevrolet Camaro Six SS esta quipado con tecnologÃa de alto desempaÃ±o centrada en el conductor, cuenta con una pantalla Color Head-Up Display y un cargador inalÃ¡mbrico para smartphones. Todo disponible y listo para llevar tu experiencia de conducciÃ³n al siguiente nÃvel.', 'img/premium/camarot.jpg', '360° DE SEGURIDAD.\r\nTodo auto de alto desempeÃ±o necesita una buena malla de seguridad. Con toda la tecnologÃa de alto desempeÃ±o que despliega esta maquina de gran precisiÃ³n, puedes tener la tranquilidad de que su tecnologÃa de seguridad es igual de precisa e impresionante.', 'img/premium/camaros.jpg', 'img/premium/camaro.jpg', 'segmentos/premium/img/ss.jpg', 'Descubre el nuevo Chevrolet Camaro Six SS, la leyenda estÃ¡ mÃ¡s en forma que nunca.'),
(16, 16, 'CHEVYTAXI.\r\nLos verdaderos hÃ©roes de la calle siempre estarÃ¡n listos para ayudar con el nuevo Chevytaxi. VersÃ¡til, robusto y confortable, estÃ¡ diseÃ±ado para hacer mÃ¡s fÃ¡cil tu trabajo. Un taxi seguro y Ã¡gil, su bajo consumo de combustible te permitirÃ¡ llevar a tus pasajeros a donde quieras.', 'img/taxis/chtin2.jpg', 'En la medida correcta.\r\nSiempre estÃ¡s en donde te necesitan, por eso debes moverte por la ciudad sin problema. El tamaÃ±o del Chevytaxi es perfecto para eso con 3.495 mm de largo y 1.495 mm de ancho.\r\n\r\nConfiable bajo el sol o la lluvia.\r\nTu Chevytaxi es seguro y Ã¡gil, con todo lo que buscabas en tu herramienta de trabajo ideal. EstÃ¡ listo para todo gracias a una combinaciÃ³n perfecta entre un exterior versÃ¡til, un interior espacioso, gran eficiencia, con acabados y materiales de alta calidad.', 'img/taxis/chtex.jpg', 'MÃ¡s comodidad para tus pasajeros.\r\nAyuda a cualquier pasajero en un taxi sencillo y econÃ³mico. El Chevytaxi es un City Car diseÃ±ado para que te muevas por la ciudad cÃ³modo, sin importar la congestiÃ³n con la que te enfrentas dÃa a dÃa.\r\n\r\nEspacio para todo\r\nSu amplio espacio interior te permitirÃ¡ llevar hasta 5 ocupantes con comodidad y seguridad. EstÃ¡ listo para cualquier reto porque cuenta con un generoso compartimiento de carga. Tiene todas las comodidades de un auto de mayor gama, pero mÃ¡s econÃ³mico.', 'img/taxis/chtit.jpg', 'Simple, fuerte y econÃ³mico.\r\nCon tu Chevytaxi tendrÃ¡s una conducciÃ³n mÃ¡s dinÃ¡mica, cÃ³moda y con menos consumo de combustible gracias a su motor (S-Tec) SMART-TEC de 1,0 Litros SOHC, de 4 cilindros, 8 vÃ¡lvulas y 65 caballos de potencia. AdemÃ¡s cuenta con sistemas SOHC (Un solo eje de levas) y MPFI (InyecciÃ³n electrÃ³nica multipunto), junto a un sensor Ã³ptico avanzado que le dan al motor un desempeÃ±o excepcional, con menores costos de mantenimiento.', 'img/taxis/chtt.jpg', NULL, 'img/taxis/chtd.jpg', 'PrevenciÃ³n y protecciÃ³n.\r\nLas barras laterales de la carrocerÃa del Chevytaxi han sido rediseÃ±adas y especÃficamente soldadas para minimizar la energÃa del impacto en caso de un choque lateral. Su estructura estÃ¡ principalmente constituida por acero de alta resistencia que le da mÃ¡s rigidez, estabilidad y un menor peso final. AdemÃ¡s, el pilar B estÃ¡ hecho de 3 placas de acero de diferentes espesores, con una soldadura a la medida por supresiÃ³n (Tailor-Welded Blanks) utilizado en vehÃculos de gama mayor.\r\n\r\nSiempre te mantendrÃ¡s en la ruta, sin importar la condiciÃ³n, gracias a su sistema de frenos de disco delanteros y frenos de tambor en la parte posterior; junto con una suspensiÃ³n independiente McPherson adelante y de torsiÃ³n atrÃ¡s.', 'img/taxis/chts.jpg', 'img/taxis/cht.jpg', 'segmentos/taxis/img/chevytaxi1.jpg', 'Listo para llevarte por las calles de Colombia, el nuevo Chevytaxi tiene la versatilidad que necesitabas. Uno de los taxis mÃ¡s confiables, serÃ¡ tu herramienta de trabajo ideal gracias a su practicidad y rentabilidad.');
INSERT INTO `caracteristicas_v` (`id_c`, `id_v`, `intro`, `introim`, `exterior`, `exteriorimg`, `interior`, `interiorimg`, `desempe`, `desempeimg`, `tec`, `tecimg`, `segury`, `seguryimg`, `img`, `imgn`, `txtn`) VALUES
(17, 18, 'CHEVYTAXI PLUS.\r\nConoce el nuevo Chevytaxi Plus, con todas las caracter&iacute;sticas que estabas buscando para hacer mÃ¡s sencillas tus largas jornadas de trabajo. Un taxi versÃ¡til y espacioso, con un potente motor de 102HP y un eficiente consumo de combustible. ', 'img/taxis/chpd.jpg', 'Estilo para trabajar.\r\nEl Chevytaxi Plus tiene un diseÃ±o frontal moderno y aerodinÃ¡mico que le da un aspecto deportivo. Cuenta con una carrocer&iacute;a estilizada que le da mayor eficiencia, junto a un amplio espacio interior y gran capacidad de carga se convierte en la herramienta de trabajo ideal para ayudar a cualquier pasajero. ', 'img/taxis/chpex.jpg', 'Confort a toda prueba.\r\nUn verdadero hÃ©roe de la calle necesita mÃ¡s confort para no dejar pasar ninguna carrera. El Chevytaxi Plus cuenta con todas las comodidades que estabas buscando: aire acondicionado, elevavidrios elÃ©ctricos, espejos retrovisores externos elÃ©ctricos, consola central ergonÃ³mica, tacÃ³metro y medidor de combustible digital.\r\nInteriores con Ecocuero.\r\nTus largos d&iacute;as de trabajo serÃ¡n mÃ¡s cÃ³modos, tu Chevytaxi Plus cuenta con una silla para el conductor especialmente diseÃ±ada con Ecocuero y tela perforada con tecnolog&iacute;a spacer. AdemÃ¡s, el resto de las sillas de taxi estÃ¡n diseÃ±adas con Ecocuero y cubretapetes en vinilo para facilitarte la limpieza de tu veh&iacute;culo.', 'img/taxis/chpit.jpg', 'Simple, fuerte y econÃ³mico.\r\nEl Chevytaxi Plus viene equipado con un motor 16 vÃ¡lvulas \r\nDOHC de 1.4 litros que desarrolla hasta 102 caballos de \r\npotencia. Tiene el novedoso sistema VGIS (mÃºltiple de \r\nadmisiÃ³n variable) que controla el aire que ingresa al motor \r\npara hacerlo mÃ¡s confiable y eficiente, al requerir una mayor\r\npotencia y desempeÃ±o en altas o bajas revoluciones. Con \r\nestas innovaciones el Chevytaxi Plus tiene una excelente \r\neconom&iacute;a de combustible.\r\n*Sujeto a las condiciones y estilo de manejo.', 'img/taxis/chpd2.jpg', 'Comodidad y entretenimiento para tus pasajeros.\r\nEl Chevytaxi Plus no solo tiene un incre&iacute;ble diseÃ±o exterior, cuenta con todo el equipo necesario para hacer cada viaje mÃ¡s cÃ³modo y funcional. Cuenta con sistema de bloqueo centralizado y direcciÃ³n hidrÃ¡ulica. Que en tus viajes siempre haya buena mÃºsica con un radio con CD, MP3, USB y entrada AUX. La silla trasera es abatible 60/40, Ãºnica en el segmento, que te permitirÃ¡ tener aÃºn mÃ¡s espacio de almacenamiento para ayudar a cualquier pasajero. ', 'img/taxis/chpt.jpg', 'PrevenciÃ³n y protecciÃ³n.\r\nUn verdadero hÃ©roe de la calle siempre va seguro. El nuevo Chevytaxi Plus cuenta con un airbag para el conductor, apoyacabezas delanteros regulables, sistema ISOFIX para sillas de niÃ±os, cinturones de seguridad de tres puntos para asientos delanteros y posteriores laterales y cinturones de seguridad de dos puntos para el asiento central posterior. Su carrocer&iacute;a cuenta con la tecnolog&iacute;a Body Frame Integrated, reforzada con puntos de deformaciÃ³n programados para cuidar a todos los ocupantes. AdemÃ¡s, la silla del conductor cuenta con un diseÃ±o ergonÃ³mico en Ecocuero, pensada para tus largas jornadas de trabajo y cubretapetes en vinilo para facilitar la limpieza de tu Chevytaxi Plus. ', 'img/taxis/chps.jpg', 'img/taxis/chp.jpg', 'segmentos/taxis/img/chevytaxiplus.jpg', 'El nuevo miembro de la familia Chevytaxi llegÃ³ para ayudar a los verdaderos hÃ©roes de la calle.SerÃ¡ tu herramienta de trabajo ideal gracias a su gran eficiencia de combustible.'),
(18, 28, 'La Nueva Chevrolet Equinox les brinda el homenaje que se merecen con su potente motor Turbo 15.L de 170HP que proporciona un excelente optimizaciÃ³n de combustible; ademÃ¡s de todos los aditamentos tecnolÃ³gicos de punta.', 'img/Equinox/eqintro.png', 'Su tecnolog&iacute;a de vanguardia te asegura ir a donde quieres, con mayor comodidad. Dale clic a cada uno de los videos y descubre la superioridad de la Nueva Chevrolet Equinox.', 'img/Equinox/eqexterior.jpg', 'Su diseÃ±o interior les brinda a ti y a tus pasajeros una experiencia Ãºnica dentro de la camioneta, gracias a su cojiner&iacute;a en cuero con terminados de lujo y toda la comodidad que te brinda todo su amplio espacio con sus aditamentos exclusivos.*', 'img/Equinox/eqinterior.jpg', 'Los verdaderos hÃ©roes llegan hasta donde tienen que llegar:\r\n\r\n-Motor1.5L que brinda un mejor rendimiento de combustible.\r\n-TransmisiÃ³n automÃ¡tica de 6 velocidades con modo manual.\r\n-Ventilas activas que regulan electrÃ³nicamente la entrada de aire de la parrilla.', 'img/Equinox/eqdesmpeÃ±o.jpg', 'Centro de Entretenimiento MyLink\r\n\r\nLa informaciÃ³n es poder.Todo hÃ©roe lo sabe y por ello, mantenerse siempre conectado es vital. Por eso, la Nueva\r\nChevrolet Equinox te ofrece las mejores herramientas en la conectividad gracias a su Smartphone Integration, para que conectes tu\r\ndispositivo a nuestra pantalla MyLink de 7\" y puedas manejar tu mundo como siempre quisiste.', 'img/Equinox/eqtecnologia.jpg', 'Los hÃ©roes no solo deben ser fuertes y poderosos. Deben asegurar que quienes lo rodean se encuentren siempre protegidos. Esto en la Nueva Chevrolet Equinox, se traduce en tecnolog&iacute;a de seguridad', 'img/Equinox/eqseguridad.jpg', 'img/Equinox/eqinicio.jpg', 'segmentos/camionetas/img/equin.png', 'Para Chevrolet los hÃ©roes no nacen, SE HACEN. Se hacen porque descubren su necesidad innata de brindar a los demÃ¡s un entorno en el que todos se sientan seguros y protegidos.'),
(19, 27, 'Una nueva raza ha nacido. Que no se deja de nadie ni de nada, que sabe lo que quiere y va por ello siempre. Para ellos hemos tra&iacute;do la Nueva Chevrolet Colorado, que combina la fortaleza y autonom&iacute;a de una Pick up con la seguridad y tecnolog&iacute;a de un SUV para conquistar lo que quieras. ', 'img/colorado.jpg', 'TU IMAGEN REFLEJA TU ESTILO DE VIDA. ES POR ESTO QUE CADA DETALLE DE LA NUEVA CHEVROLET COLORADO FUE PENSADA PARA DARLE A TU VIDA LA MEZCLA PERFECTA ENTRE ESTILO, SOFISTICACIÃ³N Y DESEMPEÃ±O: RINES DEPORTIVOS DE ALUMINIO DE 18\'\'; BARRAS DE TECHO Y LATERALES; FAROS DELANTEROS Y TRASEROS CON TECNOLOG&iacute;A LED; PARRILLA Y TERMINADOS EN CROMO.', 'img/exteriorcoloraado.webp', 'El Ã©xito llega a quienes estÃ¡n en constante movimiento. Para que te encuentre, debes tener balance perfecto entre conectividad, tecnolog&iacute;a y desempeÃ±o.', 'img/interiorcolorado.webp', 'La nueva Chevrolet Colorado es lo que tu vida necesita ya que incorpora el motor 2.8L Turbo diesel Duramax con atributos de alto desempeÃ±o, como su alto torque de 440 Nm en la versiÃ³n manual y de 500 NM en su versiÃ³n automÃ¡tica, complementado por su excelente consumo de 40 km por galÃ³n (dependiendo de las condiciones de manejo).', 'img/desempenocol.webp', 'Su pantalla tÃ¡ctil de \'\' cuenta con el sistema MyLink de Ãºltima generaciÃ³n que permite controlar las funciones de audio y telefon&iacute;a con solo oprimir un botÃ³n desde el volante, gracias a sus comandos de voz.', 'img/tecnocol.webp', 'La nueva Chevrolet Colorado incorpora atributos de seguridad activa y pasiva, para que lleves un estilo de vida indomable con la seguridad que exiges.', 'img/seguridadcol.webp', 'img/coloradoi.png', 'img/coloradoi.png', 'Una nueva raza ha nacido. Que no se deja de nadie ni de nada, que sabe lo que quiere y va por ello siempre. Para ellos hemos traÃdo la Nueva Chevrolet Colorado, que combina la fortaleza y autonomÃa de una Pick up.'),
(20, 29, 'Tu vida sigue en constante evoluciÃ³n. Te esfuerzas por conseguir tus metas y estas te exigen siempre querer mÃ¡s. Es por esto que debes salir y reclamar el espacio que necesitas para que tu vida y tus ideas sigan evolucionando. Te presentamos el Nuevo Chevrolet Beat, la mejor combinaciÃ³n entre tecnologÃa y estilo que te dan las mejores herramientas para seguir ascendiendo hacia lo que quieres.', 'img/medianos/introbeat.webp', 'VersÃ¡til, retador y dinÃ¡mico. Todo lo que a ti te define, es lo que encuentras en el fabuloso diseÃ±o exterior del Nuevo Chevrolet Beat: Sus increÃbles lÃneas de vanguardia seducen haciendo que todos se volteen a mirar.', 'img/medianos/extbeat.webp', 'Tus nuevas ideas necesitan sobresalir y en el amplio espacio interior del Nuevo Chevrolet Beat encuentras todo para hacerlo, porque logra combinar la elegancia y sofisticaciÃ³n de su diseÃ±o interior con la tecnologÃa que te permite estar conectado siempre a tu mundo.', 'img/medianos/intbeat.webp', 'Y en el Nuevo Chevrolet Beat lo compruebas desde que te subes. Inicia tu recorrido con su increÃble motor 1.2 litros que unido a su transmisiÃ³n manual de 5 velocidades,\r\nte brindan un consumo de combustible de hasta 70Km por galÃ³n*. Ahora no hay excusas para que salgas a reclamar tu espacio', 'img/medianos/desbeat.webp', 'El Nuevo Chevrolet Beat te brinda el Sistema de Infoentretenimiento Chevrolet MyLink, que viene con Smartphone Integration, compatible con Android AutoTM y demÃ¡s sistemas operativos, para que conectes tu celular a tu vehÃculo en segundos y lo disfrutes a travÃ©s de su pantalla touch de 7”.', 'img/medianos/tecnobeat.webp', 'Toda la seguridad que tu vida necesita para perseguir tus sueÃ±os la encuentras en el Nuevo Chevrolet Beat: Doble Airbag, Frenos ABS y 5 cinturones de seguridad de tres puntos.', 'img/medianos/seguridadbeat.webp', 'img/medianos/beati.png', 'img/medianos/beati.png', 'Tus ideas necesitan el espacio suficiente para hacerlas realidad. Es por ello que en Chevrolet queremos que reclames tu espacio con el Nuevo Chevrolet Beat. '),
(21, 30, 'Transformamos un &aacute;cono para crear el Chevrolet Spark GT Activ, el nuevo crossover de la familia, con el que podrÃ¡s ir mÃ¡s allÃ¡.', 'img/medianos/introsa.webp', 'Barras de techo y molduras de protecciÃ³n, que van con tu personalidad y ademÃ¡s protegen la carrocer&iacute;a.', 'img/medianos/extspa.webp', 'DirecciÃ³n electroasistida y controles en el timÃ³n, para que siempre tengas el poder en el camino ', 'img/medianos/intspa.webp', 'DirecciÃ³n electroasistida y controles en el timÃ³n, para que siempre tengas el poder en el camino ', 'img/medianos/desempspa.webp', 'Sistema Mylink con pantalla tÃ¡ctil de 7 pulgadas, compatible con Android Auto y Apple CarPlay, visualiza Waze o Maps y reproductor de mÃºsica por Bluetooth.', 'img/medianos/tecspa.webp', 'Barras de techo y molduras de protecciÃ³n, que van con tu personalidad y ademÃ¡s protegen la carrocer&iacute;a.', 'img/medianos/segspa.webp', 'img/medianos/spai.png', 'img/medianos/spgt2.webp', 'Transformamos un Ãcono para crear el Chevrolet Spark GT Activ, el nuevo crossover de la familia, con el que podrÃ¡s ir mÃ¡s allÃ¡.'),
(22, 19, 'El modelo NHR, uno de los iconos de la marca, se caracteriza por su tamaÃ±o y econom&iacute;a, conocido por ser un modelo de llanta sencilla lo cual le permite transitar sin restricciones dentro y fuera de la ciudad.', '', 'El camiÃ³n NHR Silver Class es una EdiciÃ³n especial del modelo icÃ³nico de la marca en su versiÃ³n de lujo, inspirado en el arte moderno con diseÃ±os sobrios y elegantes.\r\n\r\n', '', 'En este camiÃ³n llevamos el cromado a un nivel superior, un armonioso diseÃ±o de cabina y accesorios exclusivos originales, que solo la marca Chevrolet con Tecnolog&iacute;a Isuzu logra desarrollar para darle una confortable y mejor experiencia a nuestros clientes mÃ¡s exigentes.', '', 'El modelo NHR cuenta con un cilindraje de motor de 2999 CC. con bajo consumo de combustible y bajos costos de mantenimiento, logrando posicionarse como un modelo altamente competitivo.', '', 'AdemÃ¡s nuestros clientes Silver Class podrÃ¡n configurar su camiÃ³n a gusto con el radio el lujo Kenwwod tÃ¡ctil DDX416BT', '', 'El sistema de frenos hidrÃ¡ulicos con ABS hacen del NHR un veh&iacute;culo mÃ¡s seguro para operar en terrenos resbalosos, ademÃ¡s es complementado con su sistema LVSP de distribuciÃ³n de frenado que mantiene la seguridad en pendientes pronunciadas.', '', NULL, '', ''),
(23, 20, 'El nuevo NHR Doble Cabina Reward Euro IV puede cargar hasta 2 toneladas y el primero de Chevrolet que cuenta con un espacio confortable para 6 pasajeros, es el camiÃ³n que necesitan las empresas que requieren movimiento de carga mÃ¡s personal, es el veh&iacute;culo ideal para trabajo multifuncional con opciÃ³n de transporte familiar.', '', 'Cuenta con 2 espejos laterales, 1 en cabina. Las ventanas laterales estÃ¡n especialmente diseÃ±adas para optimizar el desempeÃ±o aerodinÃ¡mico y el espacio interior, y lo mejor, el panel de instrumentos utiliza el motivo \"onda dura\" que expande el sentido del espacio interior, proporcionando mayor comodidad al conductor.', '', 'La cabina del nuevo NHR Doble Cabina Reward Euro IV es cuadrada con un diseÃ±o aerodinÃ¡mico, con columnas frontales y laterales rectas que ademÃ¡s de un buen diseÃ±o, da un excelente aprovechamiento del espacio logrando que 6 pasajeros puedan viajar cÃ³modamente.', '', 'El Chevrolet NHR Doble Cabina Reward Euro IV, estÃ¡ equipado con un motor Isuzu 4JH1-TC de inyecciÃ³n directa Common Rail, es un motor que garantiza un mayor ahorro de combustible y menor contaminaciÃ³n, lo logra gracias a su sistema de control que inyecta con precisiÃ³n el combustible necesario.', '', NULL, '', 'La cabina del NHR Doble Cabina Reward Euro IV es altamente r&iacute;gida y el bastidor viene reforzado para controlar las fuerzas de impacto, minimizando la distorsiÃ³n en caso de una colisiÃ³n, todo esto para proteger al conductor y los ocupantes al crear un espacio de supervivencia, y para mÃ¡s seguridad, viene con seis cinturones (4 de tres puntos y 2 centrales de 2 puntos).', '', NULL, '', ''),
(24, 21, 'Seguridad y diseÃ±o ajustados a su negocio. La cabina del NKR Reward, es cuadrada con un diseÃ±o aerodinÃ¡mico con columnas frontales y laterales rectas que proporcionan una belleza funcional y un excelente aprovechamiento del espacio.', '', 'El modelo NKR Largo, el especialista en reparto urbano. Su caracter&iacute;stica principal es su largo carrozable 4.4 mts que le permite transportar materiales de baja densidad y bajo peso; un camiÃ³n que aprovecha al mÃ¡ximo su capacidad de carga en materiales de gran volumen y baja densidad.', '', 'El camiÃ³n Chevrolet NKR Euro IV ABS cuenta con un diseÃ±o de cabina ergonÃ³mico que incluye columnas frontales y laterales rectas, diseÃ±adas para brindar un excelente aprovechamiento del espacio y seguridad en el manejo', '', 'El modelo NKR largo estÃ¡ calibrado con una gran potencia y torque dentro del segmento Ultralight, lo cual lo posiciona como un camiÃ³n versÃ¡til, fuerte y rÃ¡pido.', '', NULL, '', 'El nuevo sistema de frenos hidrÃ¡ulicos con ABS + EBD + ASR hacen del camiÃ³n NKR Euro IV un veh&iacute;culo con mucha seguridad, no solo dando cumplimiento a las normativas, sino utilizando las Ãºltimas tecnolog&iacute;as para mejorar la conducciÃ³n en condiciones de carga y de terreno dif&iacute;cil', '', NULL, '', ''),
(25, 22, 'El NNR Reward, es un camiÃ³n con un peso bruto vehicular de 6.300 Kg, que se ajusta a la necesidad de una gran variedad de negocios.', '', 'La cabina del NNR Reward, es cuadrada con un diseÃ±o aerodinÃ¡mico con columnas frontales y laterales rectas que proporcionan una belleza funcional y un excelente aprovechamiento del espacio.', '', 'La direcciÃ³n es asistida hidrÃ¡ulicamente y abatible en altura y posiciÃ³n garantizando mayor ergonom&iacute;a y comodidad al conductor. Ahora, el NNR  Reward estÃ¡ equipado con una silla central para dos pajeros, 3 cinturones de seguridad (2 de 3 puntos y central de tres puntos), 2 manijas en la cabina y 2 manijas en puerta para que facilitan el acceso al camiÃ³n.', '', 'El Chevrolet NNR Reward Euro IV estÃ¡ equipado con un motor Isuzu 4JJ1 TC Turbo cargado Intercooler, con inyecciÃ³n directa Common Rail, que reduce el consumo de combustible y por lo tanto los costos de operaciÃ³n al mismo tiempo que emite menor cantidad de contaminantes Potencia: 122@2.600 hp@rpm, torque: 36@1.500 kg-m@rpm y Emisiones Euro IV, que garantizan una operaciÃ³n ideal para el combustible Colombiano y menores costo de mantenimiento.', '', NULL, '', 'La cabina del NNR Reward Euro IV, altamente r&iacute;gida y el bastidor reforzado controlan las fuerzas de impacto para minimizar la distorsiÃ³n en caso de colisiÃ³n, protegiendo a los conductores al crear una espacio de supervivencia.\r\n\r\nAdemÃ¡s, con el nuevo diseÃ±o de cabina, los conductores tienen mayor visibilidad de las Ã¡reas que rodean el veh&iacute;culo.\r\n\r\nPara ofrecer mayor seguridad, el chasis del NNR Reward Euro IV es perforado con el fin de reducir los impactos en casos de choque.\r\n', '', NULL, '', ''),
(26, 23, 'El NPR Reward, es un camiÃ³n liviano con un nuevo motor Isuzu 4HK1-TCN de inyecciÃ³n directa Common Rail, que garantiza mayor econom&iacute;a en el consumo de combustible y menor contaminaciÃ³n, gracias a su control electrÃ³nico que inyecta con precisiÃ³n el combustible a una presiÃ³n extremadamente alta, reduciendo la emisiÃ³n de NO2 y la contaminaciÃ³n ambiental.\r\n', '', 'El camiÃ³n NPR Reward, ahora viene equipado con direcciÃ³n asistida hidrÃ¡ulicamente telescÃ³pica y ajustable en altura y posiciÃ³n, 2 cinturones de seguridad de 3 puntos, cinturÃ³n central de 2 puntos, antena, dos parlantes y una cabina mÃ¡s amplia y ergonÃ³mica que facilita el acceso al camiÃ³n.', '', 'La cabina del NPR Reward, es cuadrada con un diseÃ±o aerodinÃ¡mico con columnas frontales y laterals rectas que proporcionan una belleza funcional y un excelente aprovechamiento del espacio.', '', 'El Chevrolet NPR Reward estÃ¡ equipado con un motor Isuzu 4HK1-TCN Turbo cargado Intercooler, con inyecciÃ³n directa Common Rail, que reduce el consumo de combustible y por lo tanto los costos de operaciÃ³n al mismo tiempo que emite menor cantidad de contaminantes. Potencia: 148@2.600 hp@rpm, torque: 40,9@1.600 kg-m@rpm y Emisiones Euro II, que garantizan una operaciÃ³n ideal para el combustible Colombiano y menores costo de mantenimiento.', '', NULL, '', NULL, '', NULL, '', ''),
(27, 24, 'El modelo NKR microbÃºs estÃ¡ diseÃ±ado para servicio urbano, dando cumplimiento a las nuevas reglamentaciones de accesibilidad y seguridad. ', '', 'Gracias a su tamaÃ±o se puede desplazar con facilidad en los espacios reducidos propios de nuestras ciudades, y cumple a la perfecciÃ³n con las necesidades de transporte de pasajeros.', '', 'Este chasis se adapta a variadas necesidades de transporte y permite un fÃ¡cil montaje de la carrocer&iacute;a gracias a su particular diseÃ±o. Queremos que pueda empezar a trabajar cuanto antes y recupere su inversiÃ³n rÃ¡pidamente.', '', 'El motor Isuzu del NKR microbÃºs cuenta con un torque plano desde bajas revoluciones, que en conjunto con una transmisiÃ³n de 6 velocidades permite un adecuado desempeÃ±o en distintas condiciones topogrÃ¡ficas, a la vez que permite un bajo consumo de combustible debido a su tamaÃ±o de motor.', '', NULL, '', 'El sistema de frenos hidrÃ¡ulico junto con las ayudas de seguridad activa ABS, EBD y ASR, garantizan una mÃ¡xima seguridad durante frenadas de emergencia y en terrenos de poca adherencia, ya que acorta la distancia de frenado y evita el patinado de las ruedas.', '', NULL, '', ''),
(28, 25, 'El modelo NPR minibuseta Euro IV, estÃ¡ diseÃ±ado para servicio urbano, dando cumplimiento a las nuevas reglamentaciones de accesibilidad y seguridad.', '', 'El chasis NPR minibuseta Euro V, cumple con las nuevas legislaciones y normativas ambientales, gracias a sus bajos niveles de emisiones ayudando as&iacute; a cuidar el medio ambiente. Es ademÃ¡s un chasis de un excelente desempeÃ±o en las diferentes modalidades de transporte y brinda una inmejorable relaciÃ³n costo-beneficio.', '', 'La suspensiÃ³n del chasis NPR minibuseta Euro V, brinda un excelente confort de marcha ya que cuenta con barra estabilizadora tanto delantera como trasera y amortiguadores telescÃ³picos de doble efecto. AdemÃ¡s, tiene un bajo costo de mantenimiento gracias a su diseÃ±o sencillo y eficiente.', '', 'El motor Isuzu de la NPR minibuseta Euro V, cuenta con un torque plano desde bajas revoluciones en la versiÃ³n diÃ©sel que en conjunto con la transmisiÃ³n Isuzu de 6 velocidades, permite un adecuado desempeÃ±o en distintas condiciones topogrÃ¡ficas de nuestras ciudades, a la vez que permite un bajo consumo de combustible.', '', 'El sistema de frenos hidrÃ¡ulico de la NPR minibuseta Euro V, junto con las ayudas de seguridad activa ABS, EBD y ASR Ãºnicas en su segmento, garantizan una mÃ¡xima seguridad durante frenadas de emergencia y en terrenos de poca adherencia, ya que acorta la distancia de frenado, evita el patinado de las ruedas y permite el control del veh&iacute;culo durante la frenada.', '', NULL, '', NULL, '', ''),
(29, 26, 'El modelo NQR busetÃ³n 9.2 T Euro V, estÃ¡ diseÃ±ado para servicio urbano, intermunicipal y especial y da cumplimiento a las nuevas reglamentaciones de accesibilidad y seguridad. Contamos con tecnolog&iacute;a Euro V que da respuesta a los Ãºltimos requerimientos ambientales del pa&iacute;s, por lo que se convierte en la mejor opciÃ³n para los clientes que quieren adelantarse a estas nuevas exigencias. AdemÃ¡s, estÃ¡ equipado con frenos 100% de aire. Cuenta con toda la tecnolog&iacute;a Isuzu que sumada a la experiencia Chevrolet en Colombia, le darÃ¡n la mayor tranquilidad a la hora de invertir en el transporte de pasajeros', '', 'Este chasis cuenta con la mÃ¡s reciente exigencia ambiental Euro V para dar cumplimiento a las normativas mÃ¡s estrictas del pa&iacute;s y estÃ¡ diseÃ±ado para operar en las modalidades de transporte urbano, intermunicipal y especial, adaptÃ¡ndose a las diferentes necesidades de los clientes.', '', 'La suspensiÃ³n de este chasis brinda un excelente confort de marcha ya que cuenta con barra estabilizadora tanto delantera como trasera y amortiguadores telescÃ³picos de doble efecto que tiene bajos costos de reparaciÃ³n y una alta resistencia frente al trabajo duro.', '', 'Cuenta con un torque plano desde bajas revoluciones y en conjunto con la transmisiÃ³n Isuzu de 6 velocidades, permite un adecuado desempeÃ±o en distintas condiciones topogrÃ¡ficas a la vez que permite un bajo consumo de combustible.', '', NULL, '', 'El sistema de frenos 100 % aire de la NQR busetÃ³n 9.2 T Euro V, junto con el sistema antibloqueo de frenos ABS, garantiza una mÃ¡xima seguridad durante frenadas de emergencia.', '', NULL, '', ''),
(31, 32, 'Toda tu vida sigue en constante evoluciÃ³n, por lo que necesitas un lugar a la medida. El Nuevo Chevrolet Onix SedÃ¡n te brinda un exterior deportivo con sus l&iacute;neas modernas y elegantes; mientras que en su interior cuenta con todo el espacio y la tecnolog&iacute;a que necesitas: una amplia capacidad de baÃºl de 500 litros y toda la conectividad de su Sistema de Entretenimiento MyLink con pantalla tÃ¡ctil de 7\", compatible con Android Auto y Apple Car Play.', 'img/sparlifee/onixsedain.jpg', 'Cada detalle en diseÃ±o y estilo del Nuevo Chevrolet Onix SedÃ¡n viene pensado en transmitir tu personalidad. Sus l&iacute;neas deportivas y diseÃ±o moderno convierten cada viaje que realices en una experiencia llena de confort y elegancia.  ', 'img/onixsedan/extersedan.png', 'Toda tu vida sigue en constante evoluciÃ³n, por lo que necesitas un lugar a la medida. El Nuevo Chevrolet Onix SedÃ¡n te brinda un exterior deportivo con sus l&iacute;neas modernas y elegantes; mientras que en su interior cuenta con todo el espacio y la tecnolog&iacute;a que necesitas: una amplia capacidad de baÃºl de 500 litros y toda la conectividad de su Sistema de Entretenimiento MyLink con pantalla tÃ¡ctil de 7\", compatible con Android Auto y Apple Car Play.', 'img/onixsedan/interiorsedan.png', 'El Nuevo Chevrolet Onix SedÃ¡n estÃ¡ equipado con un motor de 1.4L de nueva generaciÃ³n Smart Perfomance Economy (SPE) y 4 cilindros, adecuado para la topograf&iacute;a colombiana, transmisiÃ³n mecÃ¡nica de cinco velocidades o automÃ¡tica secuencial de seis velocidades que garantiza la eficiencia, el rendimiento y una excelente experiencia de manejo.', 'img/onixsedan/desem.png', 'La tecnolog&iacute;a estÃ¡ presente para garantizar tu tranquilidad:\r\n\r\nCÃ¡mara de reversa (VersiÃ³n LTZ),\r\nSensores de reversa (VersiÃ³n LTZ),\r\nSistema de apertura de puertas remota,\r\nFaros antiniebla (VersiÃ³n LTZ)', 'img/onixsedan/tecnosedan.png', 'La seguridad estÃ¡ presente para garantizar tu tranquilidad:\r\nDoble airbag frontal,\r\nAlarma antirrobo,\r\nCinco (5) cinturones de seguridad y apoyacabezas regulables en altura\r\nAnclajes ISOFIX/LATCH asientos de niÃ±os', 'img/onixsedan/seguridad.png', 'img/medianos/onixsedan.jpg', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `nit` varchar(60) CHARACTER SET utf8 NOT NULL,
  `nombres` varchar(170) CHARACTER SET utf8 NOT NULL,
  `apellidos` varchar(170) CHARACTER SET utf8 NOT NULL,
  `correo` mediumtext CHARACTER SET utf8 NOT NULL,
  `telefono` varchar(15) CHARACTER SET utf8 NOT NULL,
  `direccion` mediumtext CHARACTER SET utf8 NOT NULL,
  `vehiculo` varchar(120) CHARACTER SET utf8 NOT NULL,
  `placa` varchar(100) CHARACTER SET utf8 NOT NULL,
  `km` int(11) NOT NULL,
  `usuario` varchar(100) CHARACTER SET utf8 NOT NULL,
  `contraseña` varchar(200) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colision`
--

CREATE TABLE `colision` (
  `e1` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'Nulo',
  `e2` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Nulo',
  `e3` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Nulo',
  `e4` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Nulo',
  `e5` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Nulo',
  `e6` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Nulo',
  `e7` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Nulo',
  `e8` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Nulo',
  `e9` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Nulo',
  `idc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `colision`
--

INSERT INTO `colision` (`e1`, `e2`, `e3`, `e4`, `e5`, `e6`, `e7`, `e8`, `e9`, `idc`) VALUES
('Nulo', 'Nulo', 'Nulo', 'Activo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Activo', 7),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Activo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 8),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 9),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 10),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 11),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 12),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 13),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 14),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 15),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 16),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 17),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 18),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 19),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 20),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 21),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 22),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 23),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 24),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 25),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 26),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 27),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 28),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 29),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 30),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 31),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 32),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 33),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 34),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 35),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 36),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 37),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 38),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 39),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 40),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 41),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 42),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 43),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 44),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 45),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 46),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 47),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 48),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 49),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 50),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 51),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 52),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 53),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 54),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 55),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 56),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 57),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 58),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 59),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 60),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 61),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 62),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 63),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 64),
('Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 'Nulo', 65);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colision1`
--

CREATE TABLE `colision1` (
  `idcoli` int(11) NOT NULL,
  `placa` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordent` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fechac` date DEFAULT NULL,
  `horac` time DEFAULT NULL,
  `idmeca` int(11) NOT NULL,
  `e1` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'nulo',
  `e2` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'nulo',
  `e3` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'nulo',
  `e4` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'nulo',
  `e5` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'nulo',
  `e6` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'nulo',
  `e7` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'nulo',
  `e8` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'nulo',
  `e9` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'nulo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `colision1`
--

INSERT INTO `colision1` (`idcoli`, `placa`, `ordent`, `fechac`, `horac`, `idmeca`, `e1`, `e2`, `e3`, `e4`, `e5`, `e6`, `e7`, `e8`, `e9`) VALUES
(1, 'AVG-135', '72600', '2017-03-09', '09:30:00', 3, 'nulo', 'nulo', 'nulo', 'nulo', 'nulo', 'nulo', 'nulo', 'nulo', 'Activo'),
(4, 'JJR-135', '1000', NULL, NULL, 5, 'nulo', 'nulo', 'nulo', 'nulo', 'nulo', 'nulo', 'nulo', 'nulo', 'Activo'),
(5, '1', '21312888', NULL, NULL, 5, 'nulo', 'nulo', 'nulo', 'nulo', 'nulo', 'nulo', 'nulo', 'nulo', 'nulo'),
(6, '2kjkj', '1', NULL, NULL, 5, 'nulo', 'nulo', 'nulo', 'nulo', 'nulo', 'nulo', 'nulo', 'nulo', 'nulo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacion`
--

CREATE TABLE `cotizacion` (
  `id` int(11) NOT NULL,
  `vehiculo` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cedula` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cotizacion`
--

INSERT INTO `cotizacion` (`id`, `vehiculo`, `cedula`, `nombre`, `celular`, `email`) VALUES
(1, 'prueba', 'prueba', 'prueba', '3', 'prueba'),
(3, '1', '1323', 'bvb', '66', 'gg'),
(4, '14', '121', 'assa', '222', 'sss'),
(5, '15', '232', 'wew', '2323', 'wewe'),
(6, '4', '25886', 'Luis', '3456667', 'Kkjjj@hjhg.co'),
(7, '7', '655888', 'Jjhhhh', 'Jujhh', 'Jjjj@'),
(8, '7', '655888', 'Jjhhhh', 'Jujhh', 'Jjjj@'),
(9, '15', '33', 'bvb', '66', 'sss'),
(10, '17', '154', 'bvb', '444', 'wew'),
(11, '17', '22', 'fff', '222', 'wew'),
(12, '16', '020', 'bvb', '02', 'bvbb'),
(13, '15', '25121', 'bvb', '2121', 'sss'),
(14, '17', '112', 'bvb', '30', 'sss'),
(15, '17', '203', 'lÃ±ll', '323', 'wew'),
(16, '16', '10251', 'jj', '02', 'bvbb'),
(17, '16', '21', 'bvb', '02', 'opop');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizar`
--

CREATE TABLE `cotizar` (
  `id` int(11) NOT NULL,
  `nombres` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `vehiculo` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Fecha_ingreso` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cotizar`
--

INSERT INTO `cotizar` (`id`, `nombres`, `correo`, `telefono`, `vehiculo`, `Fecha_ingreso`) VALUES
(1, 'prueba', 'prueba', 'prueba', 'prueba', 'prueba'),
(2, 'prueba2', 'prueba2', '3123212323', 'beat', '2019-4-29'),
(3, 'lizeth lorena cuaicuan ', 'lizethlorena1908@gmail.com', '3014721802', 'spark life', '2019-5-8'),
(7, 'andrea castro', 'andreamilecas@gmail.com', '3147453115', 'equinox', '2019-5-28'),
(8, 'leybar andres acosta vallejo', 'andresacosta124@hotmail.com', '3134296027', 'nhr', '2019-6-2'),
(9, 'alberth alvear lopez', 'alvearlopezalberth', '3176030891', 'luv dmax 4x4 diesel', '2019-6-2'),
(14, 'SEBASTIAN ROMERO SOLIS ', 'SEBAS6410@GMAIL.COM', '3162284792', 'SPARK GT', '2019-7-9'),
(15, 'JULL GUERRERO', 'julisitt@hotmail.com', '3017830289', 'spark life', '2019-7-31'),
(20, 'Andres Sebastian Romero Solis ', 'Sebas6410@gmail.com ', '3162284792', 'Spark life o gt', '2019-8-9'),
(21, 'Miguel Suarez', 'miguealexander14@gmail.com', '3168002130', 'Spark life', '2019-8-16'),
(24, 'Natalia Cabrera', 'Nayitacj@gmail.com', '3178533717', 'spark gt', '2019-8-20'),
(25, 'Lauro Alejandro Rojas Benavides ', 'alerojas2304@gmail.com', '3128705342', 'Spark GT ', '2019-8-21'),
(28, 'CARLOS  GUILLERMO RAMIREZ VILLEGAS ', 'glocarivan@hotmail.com', '3182704933', 'CHEVROLET COBAL  2015 ', '2019-8-29'),
(29, 'LEIDY MARIANA ROSERO', 'odomarianarosero@gmail.com', '3134635643', 'spark gt', '2019-8-30'),
(31, 'Alex Urbano', 'alexurbanogomez@gmail.com', '3187660558', 'Chevrolet Sail', '2019-9-1'),
(32, 'Camila perez andrade ', 'Perezcamila299@yahoo.com', '3004847646', 'Taxi', '2019-9-2'),
(33, 'Carlos Esneider Guancha Rojas', 'carloseguanchar@gmail.com', '3167645507', 'Spark Active', '2019-9-13'),
(36, 'IvÃ¡n DarÃ­o FlÃ³rez PotosÃ­', 'dariofp_94@hotmail.com', '31849211654', 'spark gt', '2019-9-19'),
(37, 'James Erazo', 'Jameserazo532@gmail.com ', '3208151715', 'Empaque de culata de FTR', '2019-9-27'),
(38, 'JOSE ALIRIO NAZATE ', 'paulacastro1524@gmail.com', '3125264932', 'Dmax CD', '2019-10-1'),
(40, 'amanda daniela huerfano tobar', 'danielatobar302@gmail.com', '3176728216', 'chevrolet sail', '2019-10-9'),
(41, 'Gerson Rivera Gomez', 'jersonrivera@gmail.com', '3003566154', 'chevrolet beat', '2019-10-18'),
(43, 'Anjeli MartÃ­nez ', 'Anjeli.martinez@correo.policia.gov.co ', '3182055112', 'Chevrolet spark o chevrolet aveo ', '2019-10-24'),
(46, 'LUIS EDUARDO', 'lesantacruz7@hotmail.com', '3108251461', 'beat', '2019-11-6'),
(47, 'Jesus arvey erazo', 'Arvey_5284@hotmail.com', '3174859256', 'Npr', '2019-11-10'),
(49, 'Jesus Alexander Miramag', 'jesuad12@hotmail.com', '3157571988', 'Chevrolet Onix', '2019-11-12'),
(50, 'FERNANDO ENRIQUE PAREDES JIMÃ‰NEZ', 'fernandoparedesj@gmail.com', '3188749991', 'ONIX', '2019-11-19'),
(51, 'LUIS VIVAS', 'electroredes.nar@gmail.com', '3014578208', 'Chasis NPR servicio publico', '2019-11-29'),
(52, 'Jesus arvey erazo', 'Arvey_5284@hotmail.com', '3174859256', 'Nkr', '2019-12-3'),
(53, 'Jose Esteban Moncayo', 'joseestebange@hotmail,com', '3136366740', 'Camioneta Dmax', '2019-12-8'),
(54, 'NAYIBE BENAVIDES ARTEAGA', 'NAYIBEANGE@GMAIL.COM', '3167353168', 'SAIL 2018-2014', '2019-12-11'),
(55, 'Camilo AndrÃ©s MadroÃ±ero ', 'Camilomadro246@gmail.com ', '3225285905', 'Camioneta luv dmax 4x4 ', '2019-12-12'),
(56, 'HARNOL BOLAÃ‘OS MUÃ‘OZ', 'habo3155@hotmail.com', '3233445730', 'captiva sport 2010', '2019-12-18'),
(57, 'WILLIAM CAMILO HOYOS ROSALES ', 'william.hoyos4168@correo.policia.gov.co', '3225685597', 'spark gt', '2019-12-20'),
(58, 'juan aguirre', 'Juan.aguirre1423Ã correo.policia.gov.co', '3235851977', 'Usado sonic azul vivo', '2020-1-7'),
(59, '', '', '', '', ''),
(60, 'CARLOS ENRIQUE CORTEZ', 'caenco@hotmail.com', '3112876368', 'Nissan Kicks ', '2020-1-13'),
(61, '', '', '', '', ''),
(62, 'kevin andres ortiz ORTIZ', 'carortiz150@hotmail.com', '3155591628', 'chevrolet', '2020-1-20'),
(63, 'Ricardo Pasuy', 'ricardopasuy@hotmail.com', '3167532107', 'Tracker', '2020-1-24'),
(64, 'FabiÃ¡n Vinicio Barros Carmona', 'fabian_barroscarmona@hotmail.com', '593999806505', 'Chevrolet o Suzuki Alto 2003', '2020-1-27'),
(65, 'Jose armando rosero', 'anamolaferre@gmail.com', '3127314621', 'Camion Nks', '2020-1-31'),
(66, '', '', '', '', ''),
(67, 'Loreny GÃ³mez', 'loreny.gomezb@gmail.com', '3143937794', 'Chevrolet sail ltz', '2020-2-7'),
(68, 'Diego Armando Mafla Grijalba', 'diegomafla02@gmail.com', '3172907318', 'captiva', '2020-2-12'),
(69, '', '', '', '', ''),
(70, '', '', '', '', ''),
(71, 'Andres Quitiaquez', 'silvioandres@outlook.com', '3104972435', 'Spark GT', '2020-3-18'),
(72, '', '', '', '', ''),
(73, 'Edgar Tovar ', 'tovaredgar607@gmail.com ', '3137540976', 'Npr con carrocerÃ­a ', '2020-4-8'),
(74, '', '', '', '', ''),
(75, 'alejandro', 'narvaez', '3218732696', 'tracker', '2020-4-18'),
(76, '', '', '', '', ''),
(77, 'David Fernando caicedo Salazar ', 'Deivifer4@gmail.com', '3167072643', 'Chevrolet NP 300 piajo ', '2020-4-26'),
(78, 'Santiago Timana Guerrero', 'satg2107@gmail.com', '3156955746', 'Taxis', '2020-4-28'),
(79, 'Leonardo coral andrade', 'leocoan85@gmail.com', '3218286331', 'Nhr', '2020-5-2'),
(80, 'gustavo adolfo melan', 'valenti220@hotmail.com', '3217756609', 'onix turbo', '2020-5-12'),
(81, 'Ivan DarÃ­o GonzÃ¡lez Martinez ', 'melizza.1991@gmail.com', '3102717258', 'Spark ', '2020-5-15'),
(82, '', '', '', '', ''),
(83, 'Orlando Quintana', 'publicidadquintanacar@hotmail.com', '3188650204', 'NNR', '2020-5-18'),
(84, 'Anderson Raul Gomajoa', 'argb0222@gmail.com', '3155010109', 'AVD307', '2020-5-19'),
(85, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `ide` int(11) NOT NULL,
  `cedula` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `genero` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sede` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cargo` int(11) NOT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  `foto` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`ide`, `cedula`, `nombre`, `apellido`, `email`, `genero`, `sede`, `cargo`, `estado`, `foto`) VALUES
(1, '404045', 'Edwin ', 'Mera', '', 'm', 'prin', 2, 1, 'mecanicos/edwinm.jpg'),
(2, '404046', 'Wilder', 'Nupan', '', 'm', 'prin', 2, 1, 'mecanicos/wildern.jpg'),
(3, '404047', 'Jaime', 'Rojas', '', 'm', 'prin', 2, 1, 'mecanicos/jaimer.jpg'),
(4, '404048', 'Segundo', 'Ruano', '', 'm', 'prin', 2, 1, 'mecanicos/segundor.jpg'),
(5, '404049', 'Anderson', 'Timaran', '', 'm', 'prin', 2, 1, 'mecanicos/andersont.jpg'),
(6, '404050', 'Alvaro', 'Escobar', '', 'm', 'prin', 2, 1, 'img/user.png'),
(7, '404051', 'Juan', 'Rosas', '', 'm', 'prin', 2, 1, 'mecanicos/juanr.jpg'),
(8, '404052', 'Jose', 'Bolaños', '', 'm', 'prin', 2, 1, 'mecanicos/joseb.jpg'),
(9, '', 'MAURICIO', 'CASTRO', 'carlosguerrero@autodenar.com', 'm', 'prin', 1, 1, 'img/user.png'),
(11, '', 'CRISTIAN', 'BETANCOURT', 'carolinaortiz@autodenar.com', 'f', 'prin', 1, 1, 'img/user.png'),
(12, '', 'CLAUDIA', 'PALACIOS', '', 'f', 'prin', 1, 0, 'asesores/a2.jpg'),
(13, '', 'ANDRÉS', 'RODRÍGUEZ', 'andresrodriguez.autodenar@gmail.com', 'm', 'prin', 1, 1, 'asesores/andresr.JPG'),
(14, '', 'ANGELICA', 'ORTIZ', 'andreahidalgo@autodenar.com', 'f', 'prin', 1, 1, 'asesores/a2.jpg'),
(15, '', 'JHON', 'BETANCOUR', 'aidita1002@gmail.com', 'm', 'prin', 1, 1, 'img/user.png'),
(16, '404053', 'Marcos', 'Ceballos', '', 'm', 'ipia', 2, 1, 'mecanicos/marcosceballos.jpg'),
(17, '', 'Aleatorio', ' ', '', 'm', 'prin', 1, 1, 'asesores/alea.jpg'),
(18, '', 'Aleatorio', ' ', '', 'm', 'prin', 2, 1, 'asesores/alea.jpg'),
(19, '', 'GUSTAVO', 'VILLOTA', '', 'm', 'prin', 1, 0, 'asesores/a1.png'),
(20, '', 'Alejandro', 'Narvaez', 'dircontactcenter@autodenar.com.co', 'm', 'prin', 4, 1, 'asesores/a1.png'),
(21, '13014227', 'LUCY', 'CUARAN', 'lucycuaran.autodenar@gmail.com', 'f', 'ipia', 1, 1, 'asesores/a2.jpg'),
(22, '', 'YARITZA', 'GOYES', 'yaritzagoyes.autodenar@gmail.com', 'm', 'ipia', 1, 1, 'asesores/a2.jpg'),
(23, '', 'Carlos', 'Cabrera', 'carlos23alve@yahoo.es', 'm', 'chevy', 1, 1, 'asesores/carlo.jpg'),
(24, '', 'ANGEL', 'VARELA', '', 'm', 'chevy', 1, 1, 'asesores/a1.png'),
(25, '', 'Jose', 'Ibarra', 'natfer_03@hotmail.com', 'm', 'chevy', 1, 1, 'asesores/josei.jpg'),
(26, '', 'Juan', 'Madroñero', 'juankho88@hotmail.com', 'm', 'chevy', 1, 1, 'asesores/juanm.jpg'),
(27, '', 'German', 'Criollo', 'germancriollo12@gmail.com', 'm', 'chevy', 1, 1, 'asesores/germanc.jpg'),
(28, '', 'Alfonso', 'Mayorga', 'alfomayova@hotmail.com', 'm', 'chevy', 1, 1, 'asesores/alfoma.jpg'),
(29, '', 'Jorge', 'Mera', 'andersson0217@hotmail.com', 'm', 'chevy', 1, 1, 'asesores/andersonm.jpg'),
(30, '', 'Luis', 'Pinillos', 'salaspi@hotmail.com', 'm', 'chevy', 1, 1, 'asesores/luisp.jpg'),
(31, '', 'Aleatorio', ' ', '', 'm', 'chevy', 1, 1, 'asesores/alea.jpg'),
(32, '', 'OMAR ', 'SARMIENTO', 'adrianmartinez@autodenar.com', 'm', 'prin', 3, 1, 'asesores/a1.png'),
(33, '120354', 'ANDRES', 'GUEVARA', '', 'm', 'prin', 3, 1, 'asesores/GuevaraA.jpg'),
(34, '404053', 'Cristian', 'Meneses', '', 'm', 'prin', 2, 1, 'img/user.png'),
(45, '1234', 'Maria Fernanda', 'Lopez', 'asisfinanciera@autodenae.com.co', 'f', 'pasto', 5, 1, ''),
(314771942, '314771942', 'juan carlos', 'fajardo', 'mercadeo@autodenar.com.co', 'm', 'mercadeo', 4, 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleos`
--

CREATE TABLE `empleos` (
  `id` int(11) NOT NULL,
  `nombres` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ruta` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cargo` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `experiencia` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Fecha_ingreso` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empleos`
--

INSERT INTO `empleos` (`id`, `nombres`, `correo`, `telefono`, `ruta`, `cargo`, `experiencia`, `Fecha_ingreso`) VALUES
(11, 'FREDDY EDUARDO PANTOJA', 'fredys_pp@hotmail.com', '3107028232', 'CV_FREDDY_PANTOJA_PEREZ.pdf', 'Asistente comercial', 'Menos de un año', '2019-03-26'),
(12, 'Jairo Geovany Fuertes Ortega', 'jairof24.8@hotmail.com', '31726564 45', 'HOJA DE VIDA a.pdf', 'Asistente Comercial', 'Mas de dos años', '2019-04-09'),
(13, 'LUIS GABRIEL PATIÃ‘O ARCOS', 'gabriel9148@hotmail.com', '2147483647', 'HOJA DE VIDA LUIS  PATIÃ‘O..pdf', 'mecanico', 'Entre 1 y 2 años', '2019-04-01'),
(14, 'Natalia Jimena EspaÃ±a Moncayo', 'natalia.moncayo@outlook.com', '3106443534', 'cv Natalia EspaÃ±a - Hoja de vida.pdf', 'PÃ³lizas y financieras - aux. ', 'Mas de dos años', '2019-04-03'),
(15, 'Paola Andrea Martinez Arcos', 'pamartinez909@misena.edu.co', '3105953055', 'Hoja de vida_Paola Martinez (Contabilidad y Finanzas).pdf', 'Recepcionista', 'Menos de un año', '2019-04-08'),
(17, 'CHRISTIAN DAVID GONZALEZ LOPEZ', 'DAVIDGONZA360@YAHOO.COM', '3164859705', 'HV CHRISTIAN GONZALEZ ASESOR.pdf', 'AGENTE CALL CENTER', 'Menos de un año', '2019-04-16'),
(19, 'Angie paola ', 'Angiepaolaye@gmail.com ', '3184340222', 'ANGIE PAOLA YELA 2019.pdf', 'Agente ', 'Entre 1 y 2 años', '2019-04-17'),
(20, 'Oscar Jobany Rivadeneira EspaÃ±a ', 'ojrivadeneira@misena.edu.co ', '3225881934', 'Hoja de vida (1).pdf', 'Agente', 'Más de dos años', '2019-04-17'),
(21, 'Erika revelÃ³ Del Valle ', 'Solek1887@hotmail.com', '3156164948', '', 'Call center', 'Más de dos años', '2019-04-17'),
(23, 'Daissy Jackeline Molina MiÃ±o ', 'jackelinemolinam@gmail.com', '3185028076', 'jackeline molina miÃ±o pfd.pdf', 'AtenciÃ³n al cliente y auxilia', 'Más de dos años', '2019-04-17'),
(24, 'Gabriela OrdoÃ±ez ', 'gabyordonez2019@gmail.com ', '3116316410', 'GABRIELA ALEXANDRA ORDOÃ‘EZ BOLAÃ‘OS.pdf', 'Agente contact center ', 'Menos de un año', '2019-04-17'),
(25, 'Diana Yisela Casanova Cordoba ', '97casanovadiana03@gmail.com ', '3225267814', '', 'Agente Contact center ', 'Menos de un año', '2019-04-17'),
(26, 'Esteban Soto Ortiz ', 'exoto7464@hotmail.com ', '3128180967', 'hoja de vida esteban..docx', 'Asesor ', 'Mas de dos años', '2019-04-17'),
(27, 'Diana Andrea botina Castro ', 'dabotina6@misena.edu.co ', '3224966864', 'hoja dw vida diana botina.docx', 'Asesora en ventas ', 'Entre 1 y 2 años', '2019-04-17'),
(28, 'Pablo AndrÃ©s Dulce AraÃºjo ', 'padulce@hotmail.com', '3116145084', 'PABLO ANDRES DULCE Hoja de Vida 3.docx', 'Asesor Comercial ', 'Mas de dos años', '2019-04-18'),
(29, 'WILSON JAVIER', 'CHÃVES BASTIDAS', '3117724746', 'HV WILSON CHAVES PDF.pdf', 'AGENTE CONTAC CENTER', 'Ninguna', '2019-04-18'),
(30, 'WILSON JAVIER CHÃVES BASTIDAS', 'bastidasjavier@hotmail.com', '3117724746', 'HV WILSON CHAVES PDF.pdf', 'AGENTE CONTACT CENTER', 'Ninguna', '2019-04-18'),
(31, 'TANIA MARCELA CHAVES', 'marce.tatis@hotmail.es ', '3146180928', 'Test_Competencias.pdf', 'Auxiliar. Administrativo ', 'Más de dos años', '2019-04-18'),
(32, 'David Esteban MÃ©ndez Delgado ', 'mendezd75@gmail.com', '3232873927', 'Hoja de vida david esteban mendez 2019.pdf', 'Agente contra center', 'Entre 1 y 2 años', '2019-04-18'),
(33, 'Diana Carolina Mora Morillo', 'dianacarolina-222@hotmail.com', '3207793711', 'Hoja de vida Febrero 2019.pdf', 'TecnÃ³logo en gestiÃ³n de merc', 'Menos de un año', '2019-04-18'),
(34, 'Adriana VerÃ³nica Ruano Bravo', 'veronic840506@hotmail.com', '3188560907', 'HOJA DE VIDA MARZO 2019.pdf', 'Agente contact center', 'Más de dos años', '2019-04-19'),
(35, 'miguel antonio chamorro peÃ±a ', 'miancha-12@hotmail.com', '3122691786', 'HOJA DE VIDA ACTUALIZADA.docx', 'call center ', 'Menos de un año', '2019-04-19'),
(37, 'Wilmer Arturo Pozo Cuatapi ', 'Wil-67@hotmail.com', '3174446903', '303.pdf', 'Cajero/Asesor ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-04-19'),
(39, 'Myriam Patricia Obando Soto', 'mpobando@misena.edu.co', '3137532742', 'hoja de vida pdf Actualizada.pdf', 'Agente de Contact Center', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-04-19'),
(40, 'Danny Arley Benavides Rosero ', 'Dbenavides107@gmail.com ', '3178519347', 'HV - Danny Arley Benavides Rosero(1).pdf', 'Contact center ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-04-19'),
(41, 'Leidy Ximena Villota Meneses ', 'lxvillotam@gmail.com ', '3184788816', 'HOJA DE VIDA LEIDY 2.docx', 'Asesor de Call Center ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-19'),
(42, 'EDUARDO OBIDIO BOTINA TEPUD', 'eduardobotina319@hotmail.com', '3146029882', 'hojadevidaeduardo-1.docx', 'asesor ventas ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-19'),
(43, 'Wilson Armando Unigarro Botina', 'unigarro92@gmail.com', '3114342543', '', 'Ingeniero de sistemas', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-19'),
(44, 'Angie Paola Botina CaÃ±izares', 'apbotina9@misena.edu.co', '3178249343', 'C.V Angie Paola Botina CaÃ±izares.pdf', 'Agente centro de contactos', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-19'),
(45, 'Jennifer Vanessa ', 'Ortega Vallejo ', '3043708841', '', 'Contact Center', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-04-19'),
(46, 'Diego Alejandro Portilla RodrÃ­guez ', 'kingbebeco@hotmail.com ', '3182264441', 'DOCUMENTOS HOJA DE VIDA DIEGO (1).docx', 'Agente contac center', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-19'),
(47, 'MarÃ­a Daniela Chamorro Benavides', 'danielachamorro11@hotmail.com', '3013816431', 'HOJA DE VIDA DANIELA CHAMORRO BENAVIDES (1) (1).doc', 'abogada', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-04-20'),
(48, 'Johanna Milena Marcillo Combita', 'joha.marcillo1113@gmail.com', '3197719843', 'HDV pasto.pdf', 'asesor de call center', 'Menos de un aï¿½0ï¿½9o', '2019-04-20'),
(49, 'Byron Fernando Obando de la Cruz', 'baiferob@hotmail.com', '3183088611', 'HOJA DE VIDA FERNANDO.docx', 'AdministraciÃ³n', 'Más de dos años', '2019-04-21'),
(50, 'Limbania delicia Cifuentes lagos', 'samluna20-10@hotmail.com', '3117948836', '', 'Contac senter', 'Ninguna', '2019-04-21'),
(51, 'Jimena Esther Delgado Barahona ', 'ximenadelgadoba@gmail.com ', '3187776200', 'HOJA De vida XIMENA (1).doc', 'Asesor comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-21'),
(52, 'NATALIA MARCELA ERAZO CHAMORRO', 'nataliamarcelaerazo@hotmail.com', '3182050163', 'HV NATALIA ERAZO.pdf', 'AGENTE', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-22'),
(53, 'EDGAR SEBASTIAN VILLOTA VARONA', 'sebastianv.v@hotmail.com', '3146260835', 'Hoja de vida.  SEBASTIAN VILLOTA..pdf', 'Asesor comercial y asesor tÃ©c', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-22'),
(54, 'Estiven Andres Vasconez Ortiz', 'e.andres.vo@gmail.com', '3117228680', 'HOJA DE VIDA ESTIVEN VASCONEZ - 2019 (1).pdf', 'Teleoperador ', 'Mas de dos años', '2019-04-22'),
(55, 'Cristopher david moncayo delgado', 'cristopher261@hotmail.com', '3233720756', 'HOJA DE VIDA 18.pdf', 'Agente contac center', 'Menos de un aï¿½0ï¿½9o', '2019-04-22'),
(56, 'Yesica Fernanda Tumbaco Chachinoy', 'jesik1407@hotmail.com', '3122508908', 'hoja-de-vida.pdf', 'atenciÃ³n al usuario ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-22'),
(58, 'leonardo Burbano Bravo', 'leonardobbravo@outlook.es', '3202285496', 'Hoja de vida Leonardo Burbano Bravo 2019.docx', 'Agente comercial contact cente', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-22'),
(59, 'Rubio MuÃ±oz Arteaga ', 'Rubio01994@gmail.com', '3148194732', 'hoja vida rubio.pdf', 'Ventas ', 'Ninguna', '2019-04-22'),
(60, 'Luz Andrea Cordoba R.', 'saravalenty@gmail.com', '3168905972', 'HOJA DE VIDA LUZ A. CORDOBA 2019pdf.pdf', 'asistente administrativa o asi', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-04-20'),
(62, 'Johana catherine pantoja ', 'Johanapantoja@outlook.com', '3178568779', 'HOJA DE VIDA JOHANA 2.pdf', 'Ventas ', 'Ninguna', '2019-04-22'),
(63, 'Javier Hernan Ortiz  Rosero ', 'jortiz1234@hotmail.com', '3175704116', 'Hoja de vida Javier Ortiz act 09 marzo2019 env.pdf', 'Asesor', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-22'),
(64, 'Diego Alejandro Portilla RodrÃ­guez ', 'Kingbebeco@hotmail.com ', '3182264441', 'DOCUMENTOS HOJA DE VIDA DIEGO (1).docx', 'Agente contac center', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-23'),
(65, 'Manuel Juvenal Gordillo', 'gordillomanuel12@hotmail.com', '3104632618', 'HOJA DE VIDA MANUEL GODILLO.doc', 'Agente Contact Center', 'Ninguna', '2019-04-23'),
(67, 'Diana Andrea Botina Castro ', 'dabotina6@misena.edu.co ', '3224966864', 'Hoja de Vida.PDF', 'Asesora en ventas y atenciÃ³n ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-04-23'),
(68, 'CARLOS ARTURO HURTADO BENAVIDES', 'c0495@hotmail.com', '3175639026', 'hoja de  vida 2019.pdf', 'INGENIERO MECANICO', 'Menos de un aï¿½0ï¿½9o', '2019-04-23'),
(71, 'Martin junior espin almeida', 'lore-erika29@hotmail.com', '312239219', '', 'Asesor', 'Menos de un aï¿½0ï¿½9o', '2019-04-23'),
(72, 'Jenny MuÃ±oz', 'jenmun37@hotmail.com', '3187010447', 'HOJA JENNY  MUÃ‘OZ CON DOCUMENTOS.docx', 'Ventas', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-24'),
(73, 'Karen Lucely Guevara Cuaran', 'karoguevara4@gmail.com', '3103694836', 'HOJA DE VIDA ACTUA L KAREN GUEVARA.doc', 'AtenciÃ³n a cliente', 'Ninguna', '2019-04-24'),
(74, 'FREDDY EDUARDO PANTOJA PEREZ', 'fredys_pp@hotmail.com', '3107028232', 'CV3_FREDDY_PANTOJA_PEREZ.pdf', 'AGENTE CONTACT CENTER', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-04-24'),
(75, 'Angie Sthefanni  Guerrero Paez ', 'sthefanni.guerrero@gmail.com', '3104027706', 'hoja_de_vida_STHEFA_GUERRERO.pdf', 'Asesor Call Center ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-24'),
(76, 'VANESSA ALEJANDRA ROSERO ORDOÃ‘EZ', 'VANESSA.0108@HOTMAIL.CO ', '3147261106', 'HOJA DE VIDA VANESSA ROSERO.pdf', 'Call center ', 'Ninguna', '2019-04-24'),
(77, 'Jose Dilorver FlÃ³rez Quiroz', 'dilflorez@gmail.com ', '3159260569', 'FLOREZ HOJA DE VIDA.docx', 'Asesor comercial', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-04-24'),
(78, 'Brayan Javier Cabrera Narvaez', 'b.cabrera13@pascualbravo.edu.co', '3148308015', 'HOJA DE VIDA BC.pdf', 'tecnologo mecÃ¡nico automotriz', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-04-24'),
(79, 'ESTEBAN CAMILO LÃ“PEZ DAVID', 'tebaan1994@gmail.com', '3182335912', 'Hoja de Vida Esteban C. LÃ³pez - 1.pdf', 'AGENTE CONTACT CENTER', 'Menos de un aï¿½0ï¿½9o', '2019-04-24'),
(81, 'Maria Camila Matabanchoy Insuasty', 'minkamii17066@gmail.com', '3153408551', 'HV-CAMILA-ORG_1_.pdf', 'Asesora de cobranza ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-04-24'),
(82, 'AndrÃ©s Felipe TerÃ¡n Ortega', 'pipre94@gmail.com', '3118297874', 'HV_AndresFelipeTeran.pdf', 'Ingeniero de sistemas', 'Menos de un aï¿½0ï¿½9o', '2019-04-25'),
(84, 'FREDDY ORLANDO MUÃ‘OZ IZQUIERDO', 'ingfreddymunoz@gmail.com', '3155167873', 'Hoja De Vida Freddy MuÃ±oz.pdf', 'ingeniero de sistemas', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-25'),
(85, 'FREDDY EDUARDO PANTOJA PEREZ', 'fredys_pp@hotmail.com', '3107028232', 'CV2_FREDDY_PANTOJA_PEREZ.pdf', 'INGENIERO DE SISTEMAS', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-25'),
(86, 'JANNINA ROSMERY RODRIGUEZ VALLEJO', 'jannina13@hotmail.com', '3175795046', 'HV JANNINA R.docx', 'AGENTE  CONTAC CENTER', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-04-25'),
(87, 'Jorge Alexander AnguLO Mueses', 'jalexander9609@gmail.com', '3157566618', 'Hoja-de-vida-JorgeAlexanderAnguloMueses1.pdf', 'Ingeniero de sistemas', 'Menos de un aï¿½0ï¿½9o', '2019-04-25'),
(88, 'byron fernando obando de la cruz', 'baiferob@hotmail.com', '3183088611', 'HOJA DE VIDA FERNANDO.docx', 'administrador ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-26'),
(89, 'jhoseth esteban', 'josephrosero47@gmail.com', '3157282173', 'hoja_vida.pdf', 'progrmador', 'Ninguna', '2019-04-27'),
(90, 'Giovanni Araujo Lopez', 'geal10@icloud.com', '3162518559', 'Curriculum Giovanni - completo (1).pdf', 'Ingeniero de Sistemas', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-04-29'),
(91, 'Danny Reinel Burbano', 'dannyrb4@hotmail.com', '3218205470', 'HV Danny Burbano.pdf', 'Ingeniero de Sistemas ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-04-29'),
(92, 'Daniela Argote Delgado ', 'daniela.argote@gmail.com ', '3153528900', 'HOJA DE VIDA DANIELA ARGOTE DELGADO.pdf', 'Asesora Comercial, AtenciÃ³n d', 'Menos de un aï¿½0ï¿½9o', '2019-04-30'),
(93, 'David Fernando Argoty Bastidas', 'davidargotydark@hotmail.com', '3188696117', 'Hojadevidadavid.pdf', 'oficina y ventas ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-05-01'),
(94, 'Daniela Katherine De La Cruz BolaÃ±os ', 'danicruz29@gmail.com', '3113334815', 'Daniela Katherine De La Cruz BolaÃ±os.pdf', 'Ingeniero de sistemas ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-05-01'),
(95, 'LIZETH KATERINE YEPES BETANCOURT', 'yepeslk@outlook.es', '3133916487', 'Hoja de vida LIZETH nueva (1).docx', 'Asesora de cobranzas', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-05-02'),
(96, 'WILSON ANDRES BASTIDAS MORAN', 'andresbastidas82@gmail.com', '3173871847', 'HOJA _DE_VIDA.pdf', 'Ingeniero de sistemas', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-05-02'),
(97, 'VÃ­ctor Hugo Ortega MartÃ­nez ', 'ing.ortegasistel@gmail.com', '3216146347', 'cvvhom-sinP.pdf', 'Ingeniero de Sistemas', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-05-02'),
(98, 'Enith RocÃ­o Cabrera Jossa', 'enith22cabrera@gmail.com', '3106558011', 'HOJA enith 2019.pdf', 'Asesor', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-05-03'),
(100, 'Larry Rodriguez', 'larry.rodriguez@hotmail.com', '3165598984', 'lrodriguez_cv_ep_s_spa.pdf', 'Ingeniero de Sistemas', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-05-04'),
(101, 'NELSON JAIRO CUASTUMAL AUCU', 'jairolot10@hotmail.com', '3155130112', 'HOJA_DE_VIDA_JAIRO.pdf', 'INGENIERO DE SISTEMAS', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-05-09'),
(102, 'Carlos AndrÃ©s Puello BolaÃ±o', 'carlos.puellobolano@gmail.com', '3226475914', 'HOJA_DE_VIDA_CARLOS_ANDRES_PUELLO.pdf', 'Ingeniero de sistemas', 'Menos de un aï¿½0ï¿½9o', '2019-05-14'),
(103, 'Leidy Ximena Villota ', 'lxvillotam@gmail.com ', '3184788816', 'HOJA DE VIDA LEIDY 2.docx', 'Call center', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-05-14'),
(104, 'Jhonny Fernando Loza Chavez', 'jhfernando.95@gmail.com', '3167990247', '0_hoja de vida jhonny 2018.pdf', 'INGENIERO DE SISTEMAS', 'Ninguna', '2019-05-15'),
(105, 'Lady Mercedes Zambrano Benavides', 'judany2703@hotmail.com', '3233481041', 'Hoja de Vida Lady.pdf', 'cajera ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-05-16'),
(107, 'Adriana Marcela Guerrero ArÃ©valo', 'marcela_guerrero402@hotmail.com', '3174486008', 'hoja de vida.pdf', 'SegÃºn disponibilidad', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-05-23'),
(109, 'Andres Obando Bravo', 'andres_obando93@hotmail.com', '3167712871', 'hoja de vida andres obando (1).docx', 'Asesor comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-05-27'),
(112, 'Edwin Albeiro Hurtado Benavides', 'edwinalbeiro123@gmail.com', '3164452613', 'HOJA DE EDWIN ALBEIRO.pdf', 'Asesor de servicio, analista d', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-06-04'),
(113, 'Mary isabel', 'Camberoisabel2@gmail.com', '3206124731', 'HOJA DE VIDA MARY CAMBERO', 'Vendedor. Call center', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-06-08'),
(114, 'Dario Fernando Rodriguez Insuasty', 'Daferoin1708@gmail.com', '3204574549', 'H.V FERNANDO RODRIGUEZ 2019.pdf', 'Comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-06-22'),
(115, 'prueba', 'prueba@gmail.com', '3123214323', '953600393919CC59835991C.pdf', 'prueba', 'Menos de un aï¿½0ï¿½9o', '2019-06-25'),
(116, 'Maria Isabel urbina caicedo', 'iurbinac_@hotmail.com', '3163299888', 'MARIA ISABEL URBINA CAICEDO HOJA DE VIDA actializadapdf.pdf', 'Abogada', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-07-05'),
(119, 'JONATHAN DAVID RUIZ KAHUAZANGO', 'jonax-x@hotmail.com', '3146033349', 'HOJA DE VIDA ACTUALIZADA.pdf', 'Agente Contact Center', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-09'),
(120, 'Octavio rodriguez ', 'Roes88@hotmail.com', '3006890836', '', 'Asesor ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-10'),
(121, 'RAUL ARMANDO AYALA PORTILLA', 'loqueayes2408gmail.com', '3218792220', 'TECNICO EN RECURSOS HUMANOS 98379410.pdf', 'AUXILIAR CONTABLE Y FINANCIERO', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-12'),
(122, 'Angie Lorena ', 'lorenabotinameneses@gmail.com', '3164615623', 'HOJA DE VIDA angie lorena botina meneses.docx', 'ventas ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-07-17'),
(123, 'Luis Albeiro Alfonso Pimiento', 'ing.contador@outlook.es', '3138155351', 'HV SP ICONTEC.pdf', 'Contador PÃºblico', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-17'),
(124, 'Jhon Carlos GuzmÃ¡n Rosales', 'johncarlo19911991@gmail.co', '3178681025', 'HOJA_VIDA_MEDELLIN_JOHNCARLO_JUNIO DE 2019 - copia.pdf', 'Ingeniero mecÃ¡nico', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-26'),
(125, 'Edison Arbey Jurado', 'edison3939@gmail.com', '3162958584', 'KNSUQW (1).pdf', 'Asesor comercial', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-07-29'),
(126, 'Pablo Edgardo', 'jppauldelcrz@hotmail.com', '3148938800', '', 'MecÃ¡nico automotriz', 'Ninguna', '2019-07-29'),
(127, 'Oscar Andres Manrique Rodriguez', 'manrique402@hotmail. com', '3104714398', 'hojadevidacompletaoscar2018_OscarManrique__OscarManrique__1_.docx', 'asesor comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-29'),
(128, 'John fredy Rosales Villota', 'Jfrero@Hotmail.com', '3157944555', 'HOJA DE VIDA JOHN ACTUAL (3).pdf', 'Asesor comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-29'),
(129, 'Magaly del pilar Bravo Santacruz', 'Magallybra198432@gmail.com ', '3154706269', 'HOJA DE VIDA FORMATO.docx', 'Cajera, atencion y servicio al', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-29'),
(130, 'DIANA CATHERINE RUANO SANCHEZ', 'di_anis_0621@hotmail.com', '3166309294', 'DIANA CATHERINE RUANO SANCHEZ ULTIMO.docx', 'ASESOR COMERCIAL', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-29'),
(131, 'Silvia Estefany NarvÃ¡ez jojoa', 'Sinarvaez@umariana.edu.co', '3163163046', 'hdv actu...docx', 'Asesora comercial ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-07-30'),
(132, 'Jessica Estrada', 'Profe.jekita@gmail.com', '3146811175', 'HOJA DE VIDA JESSICA  ESTRADA PDF_compressed.pdf', 'Asistente ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-07-31'),
(133, 'Javier romo ojeda', 'javier.romo@cun.edu.co', '3145069821', '', 'Agente contac center', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(134, 'Ana Mercedes Villacorte Obando', 'ana-timy@hotmail.com', '3177725119', 'CV 19.pdf', 'Asistente Administrativo', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(135, 'Javier romo ojeda', 'javier.romo@cun.edu.co', '3145069821', 'HOJADEVIDAJAVIERROMOOJEDA..docx', 'Agente contac center', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(136, 'Gabriela pazmino trujillo', 'Paznimogaby301agmail. Com', '3233645457', '', 'Asesor de vantas', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(137, 'Doris Gabriela PazmiÃ±o Trujillo ', 'Gabypaz0527@gmail.com', '3233645457', 'Hoja de Vida_Gabriela PazmiÃ±o word(1).docx', 'Asesor de servicios ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(138, 'Danny daisy bermudez blanco', 'Bermudezdani09@gmail.com', '3134594737', 'hoja de vida.docx', 'Asistente administrativo', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(139, 'Doris gabriela pazmino trujillo', 'gabypaz0527agmail. Com', '3233645457', '', 'Acesor de servicios ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(140, 'Claudia RocÃ­o Martinez MuÃ±oz', 'Claudiamarti09@gmail.com ', '3147517739', 'Hojadevidaactualizadajulio2019conanexos.pdf', 'Asistente administrativa', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(141, 'Angie Tatiana Ceron Huertas', 'atch1996@gmail.com', '3187726992', 'HOJA DE VIDA ANGIE CERON.pdf', 'Asistente Administrativo ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(144, 'Danny daisy bermudez blanco', 'Bermudezdani09@gmail.com', '3134594737', 'hoja de vida.docx', 'Asistente administrativo', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(145, 'Yecid Yanine Coral Rodriguez', 'yecidycrgy1@gmail.com', '3137522651', 'HOJA DE VIDA YECID.docx', 'Asesor comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(146, 'Alejandra', 'Pupiales', '3178775644', '', 'Experiencia ventas ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(147, 'Paula Andrea DÃ­az Llorente', 'Paula.diaz75@yahoo.com', '3193036944', 'Hoja de Vida Paula Andrea DÃ­az_Ok.pdf', 'Agente contac center', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(148, 'Violeta Ceballos OrdoÃ±ez', 'bioletiqa_@hotmail.com', '3024586602', 'hoja de vida bioleta.pdf', 'secretaria administrativa ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(149, 'Diana karina ', 'Dianisk1224@gmail.com ', '3225312754', '0_DIANA KARINA RUIZ CASTILLO 2019-convertido.pdf', 'Asistente administrativo ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-07-31'),
(150, 'Diana karina Ruiz Castillo ', 'Dianisk1224@gmail.com ', '3225312754', '0_DIANA KARINA RUIZ CASTILLO 2019-convertido.pdf', 'Asistente administrativo ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(151, 'Diana karina ', 'Dianisk1224@gmail.com ', '3225312754', '0_DIANA KARINA RUIZ CASTILLO 2019-convertido.pdf', 'Asistente administrativa', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(152, 'MarÃ­a Amparo Delgado Gallardo', 'amparodg1969@gmail.com', '3178785024', 'hoja de vida amparo.doc', 'Auxiliar Administrativa', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(153, 'Diana Patricia Dorado Cabrera ', 'dianapdc1993@gmail.com ', '3202081028', 'DPDC HOJA DE VIDA', 'Asistente ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-07-31'),
(154, 'Lizeth Yecely Acosta Obando ', 'lizacob12@gmail.com ', '3217059900', 'HOJA DE VIDA 2019.docx', 'Abogada', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-07-31'),
(155, 'VÃ­ctor Edwin Jojoa Riascos', 'edwin.jojoa@gmail.com', '3163376038', 'Hoja de vida Edwin Jojoa Tecnologo CV.pdf', 'Auxiliar Administrativo y Cont', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(156, 'franco puerres', 'fhernan2081989@gmail.com', '3158019693', 'Hoja de vida franco.pdf', 'auxiliar administrativo', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-07-31'),
(157, 'Edison Yair MuÃ±oz Casabon ', 'yairmunozc@hotmail.com', '3004208141', '', 'Administrador en SST con licen', 'Menos de un aï¿½0ï¿½9o', '2019-07-31'),
(158, 'Ruth Alejandra Rubio HernÃ¡ndez', 'alejandrarubio_91@hotmail.com', '3045898146', 'HOJA DE VIDA 2019AGOSTO.pdf', 'Asistente Administrativo', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-07-31'),
(159, 'JUDY ANDREA LOPEZ PACHAJOA', 'judy170911@gmail.com', '3122403631', 'HOJA DE VIDA JALP.docx', 'ASISTENTE ADMINISTRATIVA ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(161, 'Mariana CalderÃ³n Moreno ', 'marianac.752@hotmail.com ', '3152272117', '', 'Asistente administrativo ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-07-31'),
(162, 'Diana Vanessa Robles coral ', 'divane9637@gmail.com', '3007635075', 'hoja de vida vanessa r.docx', 'Auxiliar administrativo ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-07-31'),
(163, 'Sandra del Carmen Burbano GonzÃ¡lez ', 'Zamdrabg@gmail.com', '3207060708', 'Hoja de Vida Actualizada Mayo 2019 Sandra Burbano GonzÃ¡lez nueva.docx', 'Auxiliar Administrativa ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-07-31'),
(165, 'Melva Mariela Vela Hurtado', 'melvamarivela@hotmail.com', '3103718571', 'Hoja De vida  MARY VELA.docx', 'Asistente administrativo', 'Menos de un aï¿½0ï¿½9o', '2019-07-31'),
(166, 'Alexandra Marcela Toscano Arias ', 'marcelatoscani@hotmail.com ', '3144450332', 'HOJA DE VIDA MARCE1.docx', 'Asistente', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-01'),
(167, 'Martha lucia timana', 'Marthaluciadejaramillo@gmail.com ', '3185946858', 'hoja de vida actualizada 2019 C.pdf', 'Auxiliar administrativa', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-01'),
(168, 'CLAUDIA MARCELA BENAVIDES RIASCOS', 'clau.m31@umariana.edu.co', '3105346138', 'HOJA DE VIDA Claudia Benavides Agosto.pdf', 'Asistente Administrativo', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-01'),
(169, 'MarÃ­a Alejandra Cabrera GonzÃ¡lez ', 'aleja10022017@hotmail.com', '3192215089', 'aleja hoja de vida.docx', 'Archivo, aux contable, atenciÃ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-01'),
(170, 'MarÃ­a Alejandra Cabrera GonzÃ¡lez', 'aleja10022017@hotmail.com', '3192215089', 'aleja hoja de vida.docx', 'Aux contable, archivo, atenciÃ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-01'),
(171, 'Martha juliana lopez guerrero ', 'Julianalopezguerrero739@gmail.com ', '3107999752', '', 'Asistente ', 'Menos de un aï¿½0ï¿½9o', '2019-08-01'),
(172, 'JEIMY CAROLINA VALLEJO', 'carolinene0805@hotmail.con', '3103641638', 'caro hoja de vida 2 .2.docx', 'Auxiliar administrativo ', 'Ninguna', '2019-08-01'),
(173, 'Tatiana yolanda huertas luna ', 'lunitikabella96@hotmail.com ', '3209093394', 'HOJA DE VIDA ACTUALIZADA TATIANA.docx', 'Ventas o asistente ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-01'),
(174, 'Ana RocÃ­o', 'anabucheliibarra@gmail.com', '3125749128', 'HOJA DE VIDA ANA ROCIO BUCHELI IBARRA (1).pdf', 'Asistente administrativo', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-01'),
(175, 'Diana MarÃ­a Lagos Bastidas', 'Dianamlb95@hotmail.com', '3178089107', 'hoja de vida simple - copia.pdf', 'Asesora comercial ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-01'),
(176, 'Paola Andrea LÃ³pez Linares ', 'lopezpaola307@gmail.com', '3108281916', 'HOJA DE VIDA PAOLA ANDREA 29 DE JULIO (2).docx', 'Asistente Administrativo ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-01'),
(177, 'Janneth Cristina Torres CÃ³rdoba', 'janethctorres29@hotmail.com', '3003019210', 'hoja de vida.pdf', 'Asesora comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-01'),
(178, 'Carolina de la Cruz Rosales', 'carodelacruzr22@gmail.com', '3187986592', 'hoja de vida carolina.pdf', 'Asistente administrativa', 'Menos de un aï¿½0ï¿½9o', '2019-08-01'),
(179, 'Diana Karolina Erazo Hernandez', 'karito-2498@hotmail.com', '3136593955', 'hoja de vida karo.pdf', 'Asistente administrativa', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-01'),
(180, 'MARCELA CISNEROS CORTES', 'qelly06@hotmail.com', '3502724349', 'HOJA DE VIDA MARCELA 270719.pdf', 'Auxiliar administrativa', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-01'),
(181, 'DIANA FERNANDA CORAL PAREDES', 'nanis.coral@gmail.com', '3173072618', 'HV - DIANA  F CORAL PAREDES -.pdf', 'Asistente Administrativa', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-01'),
(182, 'Doris Margoth Ortiz Vivas', 'danitavaleriacoral@gmail.com ', '3173452966', '31266', 'Asistente Administrativa', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-01'),
(183, 'Sandra Esperanza Villota Rendon', 'Svillota02@gmail.com', '3217260635', 'HOJA DE VIDA SANDRA 2 (1).docx', 'Asistente administrativo', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-01'),
(184, 'MERY SOFIA NUPAN ROSERO', 'merysofia0788@gmail.com', '3173029782', 'HOJA DE VIDA AD.doc', 'ASISTENTE ADMINISTRATIVA', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-01'),
(185, 'NESLY LORENA MESA BOLAÃ‘OS', 'neslylorena@gmail.com', '3152902279', 'HOJA DE VIDA NESLY LORENA MESA.pdf', 'Auxiliar Administrativo ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-01'),
(186, 'LEIDY JOHANA SANTACRUZ ARCOS', 'leijosantacruz@gmail.com', '3176926530', 'LEIDY JOHANA SANTACRUZ ARCOS.pdf', 'ASISTENTE ADMINISTRATIVA', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-01'),
(187, 'LUCY MAGALY NARVAEZ FIGUEROA', 'mana148@gmail.com', '3168536599', 'HOJA DE VIDA MAGALY NARVAEZ.pdf', 'Auxiliar Administrativa', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-01'),
(190, 'ROSA LIDIA CUARAN VALENZUELA', 'rosacuaran@hotmail.com', '3137136896', 'HOJA DE VIDA.pdf', 'Asistente Administrativa', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-01'),
(191, 'Paula Andrea GÃ³mez Ortiz ', 'Paulaandreagomezortiz@gmail.com ', '3173419459', '1563917227779_Hoja de vida Paula GÃ³mez.docx', 'Asistente administrativa ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-01'),
(192, 'Paola Andrea Jojoa Ruiz', 'pajo779@hotmail.com', '3178882971', 'hdev paola.doc', 'Asistente administrativa', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-01'),
(193, 'Diana Jimena Ortega Sarria', 'dianitaor@hotmail.com', '3133907716', 'Hoja de Vida_Diana Ortega 1 1.pdf', 'Asistente Administrativo', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-01'),
(194, 'Jazmin anali reyes yandun', 'anajaider@hotmail.com', '3155110473', '', 'Secretaria', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-01'),
(195, 'Luisa Fernanda londoÃ±o enriquez', 'Luisafle@gmail.com', '3013973842', '', 'Auxiliar administrativo', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-01'),
(197, 'Caren Adriana Arteaga  Gomez ', 'adrikar2122@hotmail.com', '3208908382', 'HojadevidaCarenArteaga', 'Asistente administrativa ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-01'),
(198, 'Emilse Alexandra Pachon Vidal', 'emydady@hotmail.com', '3153744396', 'HOJA DE VIDA ORIGINAL (1) (1).docx', 'Vendedora cajera', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-01'),
(199, 'Jennifer Jeraldine Ruano Florez', 'jennifergrf@gmail.com', '3122036173', 'HOJA DE VIDA JENNIFER JERALDINE RUANO FLOREZ (3).docx', 'vendedora', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-01'),
(200, 'MÃ³nica Milena GonzÃ¡lez PatiÃ±o', 'jaegmmgp@hotmail.com', '3126129961', '', 'Asistente Administrativo', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-01'),
(201, 'Diego Alrxander Bucheli', 'dagosoy@hotmail.com ', '3184553447', '66338', 'El asignado', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-01'),
(202, 'MÃ³nica Andrea Navia Ãlvarez', 'lnmana@hotmail.com', '3155677715', 'hoja de vida monica 2019.pdf', 'Asistente Administrativa', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-01'),
(203, 'Diana BeltrÃ¡n ', 'dpilarbp@gmail.com', '3105813439', '', 'Asistente', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-02'),
(204, 'Diana Lorena Pantoja salas ', 'diana.lorena989@gmail.com', '3217522735', '', 'Asistente administrativa', 'Menos de un aï¿½0ï¿½9o', '2019-08-02'),
(206, 'Jhulianna Lizeth Araujo MuÃ±oz', 'jhuli.conta22@gmail.com', '3154774354', 'HV JHULIANNA ARAUJO (TERMINADA)-convertido-convertido.pdf', 'Asistente administrativa', 'Menos de un aï¿½0ï¿½9o', '2019-08-02'),
(207, 'Claudia Marcela Delgado Cuayal', 'Claudiamarceladlgdcuayal@gmail.com', '3014126382', 'HOJA DE VIDA CORTA.docx', 'El vacante al que pueda aplica', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-02'),
(208, 'DIANA CRISTINA ORDOÃ‘EZ ORTIZ', 'dianacristi89@gmail.com ', '3127854607', 'hoja de vida.. Diana ', 'Secretaria ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-02'),
(209, 'Liliana Janneth JimÃ©nez Bastidas', 'lilianitajbs@gmail.com', '3104701825', 'hoja de vida Alk..pdf', 'Secretaria', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-02'),
(210, 'James Emiro Cabrera Cabrera', 'jamescabrerac2@gmail.com', '3174334408', 'hoja de vida jc Abril.pdf', 'Auxiliar Contable', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-02'),
(211, 'Andrea Maritza Navarro Pupiales ', 'andreita18-18@hotmail.com ', '3147018060', '117541', 'Auxiliar administrativo ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-02'),
(212, 'Elizabeth Jennifer Rodriguez Paz', 'eliza2009@hotmail.com', '3188737526', 'HV ELI.doc.pdf', 'Asistente Administrativo', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-02'),
(214, 'Fabiola ', 'Fabiolavi27@hotmail.com ', '3187945060', 'hojavidafabiola-ilovepdf-compressed.pdf', 'Asiste Administrativa ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-02'),
(215, 'AMANDA CRISTINA MORA GRANJA', 'morita-cris@hotmail.com', '3156185534', 'HOJA DE VIDA CRISTINA MORA.doc', 'asistente administrativo', 'Menos de un aï¿½0ï¿½9o', '2019-08-01'),
(218, 'Maritza Dalila Rojas Burbano', 'maryd8rojas@gmail.com', '3216543943', 'formato-hoja-de-vida maritza - aux. admin.docx', 'Asistente Administrativo', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-02'),
(219, 'Daissy viviana Gelpud Noguera ', 'Isachris122@gmail.com ', '3194878798', 'HOJA DE VIDA VIVIANA GELPUD 2 pdf.pdf', 'Auxiliar administrativo ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-02'),
(220, 'Daissy viviana Gelpud Noguera ', 'Isachris122@gmail.com ', '3194878798', 'HOJA DE VIDA VIVIANA GELPUD 2 pdf.pdf', 'Auxiliar administrativo ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-02'),
(221, 'Natalia Alejandra MuÃ±oz Martinez', 'natalia_alejandra@live.com', '3108365301', 'HOJA DE VIDA NATALIA ALEJANDRA modificada (1) (1) - copia-2.doc', 'Asesora', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-02'),
(222, 'Christian David Gonzalez Lopez ', 'Davidgonza360@yahoo.com', '3164859705', 'HV CHRISTIAN GONZALEZ WHIT       SOPORTES.pdf', 'Agente contact center', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-02'),
(223, 'Pablo AndrÃ©s Dulce AraÃºjo ', 'padulce@hotmail.com ', '3116145084', 'PABLO ANDRES DULCE  Hoja de vida 1.docx', 'Asesor Comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-03'),
(224, 'Marcela Gomez Castro', 'marcego2001@gmail.com', '3184477081', 'HOJA DE VIDA MARCELA 2018.doc', 'Asistente Administrativo', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-04'),
(225, 'Monica Alejandra Portillo Rodriguez', 'a_sm2007@hotmail.com', '3117850408', 'Hoja de vida Monica Portillo Rodriguez Julio 25 19.pdf', 'Adjunto hoja de vida para su r', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-05'),
(226, 'AURA LUZ DARY CHAMORRO', 'ldchj@hotmail.com', '3178420295', 'hjldch.pdf', 'asistente secretaria ejecutiva', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-06'),
(227, 'Violeta Ceballos OrdoÃ±ez', 'bioletiqa_@hotmail.com', '3024586602', 'hoja de vida bioleta.pdf', 'asistente', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-08'),
(229, 'Paola andrea jaramillo acosta ', 'Paolaj9274@gmail.com', '3144397935', '', 'Vendedor ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-13'),
(230, 'Andrea Johanna MuÃ±oz Quelal ', 'ajmuniozq@gmail.com ', '0987426350', 'ANDREA-JOHANNA-MUÃ‘OZ-QUELAL-HOJA-DE-VIDA.pdf', 'Ingeniera comercial ', 'Menos de un aï¿½0ï¿½9o', '2019-08-13'),
(231, 'Luis Carlos Eraso Ramos', 'luiscar60372 @hotmail.com', '3165408369', '', 'Asesor comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-13'),
(232, 'William AndrÃ©s RisueÃ±o Enriquez ', 'risuewilliam@hotmail.com', '3123194988', '', 'Asesor', 'Ninguna', '2019-08-13'),
(233, 'Carlos rene andrade benavides', 'Carlos--andrade@hotmail.com', '3175349507', 'HOJA DE VIDA COMPLETA.pdf', 'Asesor comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-13'),
(234, 'Carlos rene andrade benavides ', 'Carlos--andrade@hotmail.com', '3175349507', 'HOJA DE VIDA COMPLETA.pdf', 'Asesor comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-13'),
(235, 'Diana Andrea Botina ', 'dabotina6@misena.edu.co ', '3224966864', 'Hoja de Vida.PDF', 'Asesora de ventas en llantas r', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-14'),
(241, 'Royer Anderson Castillo Garcia', 'royer2292@gmail.com', '3014708509', '', 'Mecanico', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-19'),
(243, 'Diego armando salas salas', 'diegosalas16@hotmail.com', '3163898898', 'hoja de vida (2).docx', 'Asesor comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-19'),
(244, 'William Jesus Insandara Maritnez', 'wiwmartinez@gmail.com', '3173585970', 'Hoja de vida William Insandara Martinez.pdf', 'Asesor comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-25'),
(246, 'Santiago patino arellano ', 'Arellanosanti92@gmail.com', '3168058621', 'hoja de vida santiago patiÃ±o Arellano.pdf', 'TÃ©cnico automotriz lubricador', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-29'),
(247, 'Neider Stiven Cuastuza Eraso', 'stivenferaso@gmail.com', '3132652120', 'TCYTVP.pdf', 'Lubricador', 'Menos de un aï¿½0ï¿½9o', '2019-08-29'),
(248, 'Jhonny Brian Rosero Vallejo ', 'jhonnyroserov@gmail.com', '3164155103', 'HOJA DE Vida Jhonny.docx', 'MecÃ¡nico', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-29'),
(249, 'Alex hernando zambrano', 'alexhernandozambrano@gmail.com', '3226752693', '', 'Mecanica rrapida lubricacion a', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-30'),
(251, 'milton lopez revelo', 'milton2h@hotmail.com', '3163850038', '', 'mecanico', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-30'),
(254, 'Abel Giobvani Genoy Cortez', 'Giomotos1989@gmail.com', '3007724729', ' hv Giovani Genoy Cortez.pdf', 'Tecnico mecanico', 'Menos de un aï¿½0ï¿½9o', '2019-08-30'),
(255, 'Oli', 'Oliver rafael pimentel sabchez', '3014397899', '', 'Ayudante mecanica', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-31'),
(256, 'Danilo German Mesias Castrillon', 'baeroradicalfrv@gmail.com', '318675731', 'HOJA DE VIDA DANILO MESIAS.pdf', 'electromecanico', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-31'),
(257, 'Judy Liliana OrdoÃ±ez Gallardo', 'yudiiordones12@gmail.com', '3117048057', 'HOJA DE VIDA YUDI ORDOÃ‘EZ...pdf', 'AtenciÃ³n al cliente ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-31'),
(258, 'Yeimi Paola Moncayo  LÃ³pez ', 'yeimi2315@hotmail.com ', '3222476648', '', 'Vendedor', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-08-31'),
(259, 'Luis Alfredo Merchancano Carlosama', 'luisalf151@hotmail.com', '3177902193', '4927793.pdf', 'TÃ©cnico mecÃ¡nico', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-08-31'),
(260, 'Anderson Steven Acosta OrdoÃ±ez ', 'Estiven.acosta.o@gmail.com ', '3116442549', 'hoja de vida stiven.docx', 'Mecanico-lubricador', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-09-01'),
(261, 'Luis Alfredo Merchancano Carlosama', 'luisalf151@hotmail.com', '3177902193', '4927793.pdf', 'TÃ©cnico mecÃ¡nico', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-09-02'),
(262, 'dallan camilo lozano mogollon', 'camilolozanom@hotmail.com', '3178925247', 'DALLAN CAMILO LOZANO MOGOLLON 22222.doc', 'mecanico,lubricador', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-09-02'),
(263, 'Jorge Luis Mendoza', 'tobyqyq@hotmail.com', '3106160740', '', 'TÃ©cnico mecÃ¡nico ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-09-03'),
(264, 'DarÃ­o Santiago HernÃ¡ndez Zamora', 'santiago910519@gmail.com', '3196391785', 'HOJA DE VIDA SANTIAGO HERNANDEZ actualizada.doc', '.', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-09-04'),
(265, 'Jhonny Rosero ', 'jhonnyroserov@gmail.com', '3164155103', 'HOJA DE Vida Jhonny.docx', 'MecÃ¡nico', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-09-04'),
(266, 'ESTEBAN ALBERTO HOYOS COLLO', 'hoyoscollo@gmail.com', '3132966283', 'Hoja de vida.pdf', 'mecatronico automotriz', 'Ninguna', '2019-09-04'),
(267, 'ESTEBAN ALBERTO HOYOS COLLO', 'hoyoscollo@gmail.com', '3132966283', 'Hoja de vida.pdf', 'mecatronico automotriz', 'Ninguna', '2019-09-04'),
(268, 'Jose alberto angulo', 'vettoqfm@gmail.com', '3184645247', 'hoja-de-vida-josealbertoangulo tumaco.pdf', 'jefe de taller ', 'Menos de un aï¿½0ï¿½9o', '2019-09-04'),
(269, 'cristian camilo', 'fernandezgaviria739@gmail.com', '3136479262', 'Hoja de vida firada.pdf', 'mecanico automotirz', 'Ninguna', '2019-09-05'),
(270, 'cristian camilo', 'fernandezgaviria739@gmail.com', '3136479263', 'Hoja de vida firada.pdf', 'mecanico automotirz', 'Ninguna', '2019-09-05'),
(271, 'MARIA ALEJANDRA CUASQUEN GUERRERO ', 'mariaalejandracg11@gmail.com', '3172290362', 'HV MARIA ALEJANDRA.pdf', 'auxiliar contable', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-09-06'),
(272, 'angie lorena botina meneses ', 'lorenabotinameneses@gmail.com', '3164615623', 'lorena hoja f.docx', 'asesora comercial  - atencion ', 'Menos de un aï¿½0ï¿½9o', '2019-09-06'),
(273, 'Pablo Alfonso Piedrahita MuÃ±oz', 'pablopiedrahita1402@gmail.com', '3207257517', 'HOJA DE VIDA Pablo Piedrahita Tecnologo Mecanica.pdf', 'Mecanico Automotriz (lubricado', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-09-08'),
(274, 'carlos Daniel Delghans Salazar', 'inversilnesdelghans@gmail.com', '-3043354541', 'HOja de vida de delgan (2).pdf', 'Tecnico Automotriz (mecanico)', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-09-09'),
(277, 'FABIOLA CAROLINA ARRIOJA GARCIA', 'ARRIOJAFABIOLA@GMAIL.COM', '3053530070', 'hoja de vida fabiola.docx', 'ANALISTA ADMINISTRATIVO', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-09-13'),
(278, 'Brayan Raul Chamorro Benavides', 'bryan_9065@hotmail.com', '3177814570', 'HOJA DE VIDA BRAYA CHAMORRO.docx', 'Jefe de patio ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-09-16'),
(279, 'Wilmer Alexander Ortega barahona ', 'Wilmerortega91@gmail.com', '3136499041', 'HOJA DE VIDA wilmer ortega.docx', 'MecÃ¡nico ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-09-17'),
(280, 'Yilmar sigifredo Vallejo Rosero', 'Yilmar4658@gmail.com', '3212771281', 'Hv-Yilmar Vallejo.pdf', 'Agente Contact center ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-09-18'),
(281, 'JAVIER FERNANDO SAAVEDRA ', 'fernandosaavedra567@yahoo.com', '3104874839', 'HOJA DE VIDA JAVIER SAAVEDRA B..docx', 'PINTURA AUTOMOTRIZ, MECÃNICA.', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-09-24'),
(282, 'Alvaro Javier De La Cruz Delgado ', 'alvaro17-02@hotmail.com ', '3175211205', '2052', 'Asesor Comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-09-27'),
(283, 'Jhonny Brian Rosero Vallejo', 'jhonnyroserov@gmail.com', '3164155103', 'HOJA DE Vida Jhonny.docx', 'MecÃ¡nico automotriz', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-09-27'),
(284, 'Diana Andrea Botina Castro ', 'dabotina6@misena.edu.co ', '3224966864', 'Hoja de Vida.PDF', 'Asesora comercial ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-09-27'),
(285, 'Jefferson Mauricio EnrÃ­quez Ramos ', 'JMER891209@gmail.com', '3186397367', 'hdvj', 'Asesor comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-09-27'),
(286, 'Carlos Augusto Santacruz Torres', 'CarlosAugustoSantacruz75@gmail.com', '3155807344', 'Hoja de vida y Soportes de Carlos Santacruz.pdf', 'Sistemas', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-09-28'),
(287, 'Alvaro Javier De La Cruz Delgado', 'alvaro17-02@hotmail.com', '3175211205', 'hoja de vida Alvaro Javier De La Cruz.pdf', 'Asesor Comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-09-28'),
(288, 'Wilson AndrÃ©s puerres Velasco', 'Wisonandrespuerres2019@gmail.com', '3207655215', 'Hoja de Vida Wilson Andres Puerres  2.pdf', 'Contact center', 'Menos de un aï¿½0ï¿½9o', '2019-09-28'),
(289, 'Diana Marcela Melo Rosero ', 'dianarosero05@gmail.com', '3104454961', 'original-HOJA DE VIDA DIANA MELO (1) (2)-.pdf', 'Asesora De Ventas', 'Menos de un aï¿½0ï¿½9o', '2019-09-29'),
(291, 'Jhon Jairo delgado estrella', 'jairodelgadoe@gmail.com', '3128486536', 'HOJA DE VIDA JHON JAIRO (1) actualizada.docx', 'Bodega', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-09-30'),
(292, 'Jhon Eyson Cansimanse Villota', 'jhoncv11@hotmail.com', '3165204182', 'HOJA DE VIDA COMPLETA..pdf', 'TÃ©cnico  MecÃ¡nico', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-09-30'),
(293, 'Cristian Javier abahonza GarcÃ©s', 'Cristian-17-09@hotmail.com', '3217630183', 'HOJA DE VIDA cristian 2.docx', 'Asesor comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-09-30'),
(294, 'Alejandra AlarcÃ³n MuÃ±oz', 'alejiita-94@hotmail.com', '3183790402', 'Hoja-de-Vida-Alejandra-AlarcÃ³n.jpg', 'DiseÃ±adora grÃ¡fica- Experta ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-09-30'),
(295, 'Jonny Dario Cadena Villota', 'Johnny5506@hotmail.com', '3152965898', '', 'Asesor comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-01'),
(296, 'Pablo AndrÃ©s Dulce AraÃºjo ', 'padulce@hotmail.com ', '3116145084', 'PABLO ANDRES DULCE Hoja de Vida 3.docx', 'Asesor Comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-01'),
(297, 'Pablo AndrÃ©s Dulce AraÃºjo ', 'padulce@hotmail.com ', '3116145084', 'PABLO ANDRES DULCE Hoja de Vida 3.docx', 'Asesor Comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-01'),
(298, 'Ricardo Mauricio Buceli Cifuentes', 'ricardobucheli2@gmail.com', '3147387375', 'HOJA DE VIDA RICARDO BUCHELI 771.pdf', 'Asesor comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-01'),
(299, 'Luis David Rosas Sapuyes', 'david.sapuyes@gmail.com ', '3126156239', 'HOJA DE VIDA  LUIS DAVID.pdf', 'Administrador PÃºblico ', 'Menos de un aï¿½0ï¿½9o', '2019-10-01'),
(300, 'JHON SEBASTIAN BENAVIDES ANDRADE', 'sebastianbenavides6@gmail.com', '3103664073', 'SEBASTIAN BENAVIDES ANDRADE.pdf', 'Asesor comercial', 'Menos de un aï¿½0ï¿½9o', '2019-10-01'),
(301, 'Diana Gabriela Vargas Vela ', 'gabrielavargasvela@gmail.com', '3204061639', 'HOJA-DE-VIDA Gabriela Vargas Vela.docx', 'Agente contact center', 'Ninguna', '2019-10-01'),
(302, 'Brayan david ChÃ¡ves gallardo', 'sandybrayan96@hotmail.com', '3163871082', 'hoja de vida brayan.doc', 'Jefe de taller', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-01'),
(303, 'RubÃ©n DarÃ­o Guevara rosero', 'rdarioguevara0@gmail.com', '3187900327', 'RUBEN DARIO GUEVARA ROSERO  HV.docx', 'Asesor ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-01'),
(304, 'Cesar Augusto Arbelaez H.', 'carbelaez99@gmail.com', '3003023092', 'HOJA DE VIDA CESAR AUGUSTO ARBELAEZ HERRERA.pdf', 'Experto Marketing Digital', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-01'),
(305, 'JOSE FEDERMAN BOLAÃ‘OS BURBANO', 'federmanrcn@gmail.com', '3152000723', 'CV Jose Federman BolaÃ±os....pdf', 'Asesor Comercial', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-10-01'),
(306, 'Edison Alexander Rojas Guerrero ', 'Edisonrojas97@hotmail.com', '3103818042', '', 'TÃ©cnico mecÃ¡nico ', 'Menos de un aï¿½0ï¿½9o', '2019-10-01'),
(307, 'Allen AndrÃ©s criollo arcos ', 'Allenarcos250990@gmail.com ', '3163224521', '', 'Laminador y pintor automotriz ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-01'),
(308, 'Darwin Benavides ', 'djbenavidesdelgado345@gmail.com ', '3127887585', 'MI Hvida Nueva 3.docx', 'TÃ©cnico mecÃ¡nico, ascesor', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-10-02'),
(309, 'Wilmer Rodolfo Pantoja Portillo', 'wilpantoja1985@hotmail.com', '3157628078', 'HOJA DE VIDA  WILMER PANTOJA.doc', 'AtenciÃ³n al Cliente', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-02'),
(310, 'Jefferson Fabian Rosero Tovar', 'jeffrosero28@gmail.com', '3164178957', 'hv.pdf', 'Asesor tÃ©cnico', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-02'),
(311, 'EDGAR ESTEBAN', 'ing.estebanm.09@gmail.com', '3137312586', 'HV- Ing. Esteban MuÃ±oz.pdf', 'MARKETING DIGITAL', 'Menos de un aï¿½0ï¿½9o', '2019-10-02'),
(312, 'heberth fernando ruiz', 'fercho1490@hotmail.com', '3188620050', 'HOJA DE VIDA FERNANDO 2019.doc', 'vendedor ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-02'),
(313, 'Juan Camilo PazmiÃ±o Charfuelan', 'juan301520@hotmail.com', '3107416454', 'HOJA DE VIDA 2019-comprimido.pdf', 'Asesor comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-02'),
(314, 'Diego AndrÃ©s Narvaez Cruz ', 'Diegonar53@g.mail.com', '3117996635', '', 'Asesor comercial', 'Menos de un aï¿½0ï¿½9o', '2019-10-02'),
(315, 'Paula Andrea NarvÃ¡ez Paredes', 'pannderz@gmail.com', '3024660993', 'HOJA DE VIDA.pdf', 'Experto en Marketing Digital', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-10-02'),
(316, 'Luis Felipe Castro hidalgo', 'Luisfcastroh@hotmail.com', '3214836008', 'Hoja de vida 2019 pdf Pto.pdf', 'Gerente', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-02'),
(317, 'Andres', 'Andresdiablo1@hotmail.com', '3153181520', '', 'Alistadorde veiculos', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-03'),
(318, 'Brayan alexander Riascos', 'brayanalexanderriascos1991@gmail.com', '3153050251', '', 'Tecnico mecanico', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-03'),
(319, 'Jeferson David Nandar Diaz', 'jefer001diaz@gmail.com ', '3156974350', 'ING. DAVID NANDAR.pdf', 'Jefe de taller', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-03'),
(320, 'Wilmer Arturo Pozo Cuatapi ', 'Wil-67@hotmail.com ', '3174446903', 'Hoja de vida.pdf', 'Asesor comercial ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-10-03'),
(321, 'Luis fernando yama tobar', 'Fernandoyama85@gmail.com', '3177834764', '', 'Mecanico', 'Ninguna', '2019-10-04'),
(322, 'Ricardo Efrain Maya Mera', 'remms10@gmail.com', '3104099893', 'RICARDO MAYA - HOJA DE VIDA.pdf', 'Asesor comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-04'),
(323, 'Edison Ricardo Cabrera Bastidas ', 'edisonricardo1994@gmail.com', '3108426000', '1-hoja-de-vida-EDISON completa_comprimida.pdf', 'asesor comercial ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-10-04'),
(324, 'Diana Cristina ocaÃ±a lopez', 'dianadh1808@gmail.Com', '3232953283', 'HOJA DE VIDA DIANA  ACTUAL 2019 PDF.pdf', 'Administrativo', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-10-04'),
(325, 'MarÃ­a Fernanda Chaves Velasquez', 'mafernanda200@hotmail.com', '3128657308', 'HojadeVidaMafeAutodenar1.pdf', 'Asesores comerciales', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-10-05'),
(326, 'Carlos eduardo leguizamo rozo', 'Carlospitss5@gmail.com ', '3222309728', 'HOJA DE VIDA CELR..-1.pdf', 'Operario/Administrador', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-05'),
(327, 'Andres Alberto Pabon Moran', 'apm1001@hotmail.com', '3007790961', 'Hoja de vida - Andres Pabon Moran.pdf', 'Experto en Marketing Digital', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-07'),
(331, 'Wilmer alexander ortega barahona', 'Wilmerortega91@gmail.com ', '3136499041', 'HOJA DE VIDA 1.docx', 'TÃ©cnico mecÃ¡nico ', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-10-17'),
(332, 'liliana meredes coral mejia', 'lilycor83@hotmail.com', '3156311245', 'HOJA DE VIDA liliana2018 (Autoguardado).docx', 'asesora comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-10-29'),
(333, 'Christian Camilo Salazar Zambrano', 'chriscam40@gmail.com', '3174463788', 'HDV Christian Salazar 2019 actualizada con cartas-converted.pdf', 'ACRESOR DE VENTAS', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-11-07'),
(334, 'JENNIFER ALEJANDRA MARTINEZ GUERRERO', 'ALEJANDRAMARTINEZ3094@GMAIL.COM', '3116298685', 'hoja de vida Jennifer Alejandra MartÃ­nez.docx', 'ASESOR COMERCIAL', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-11-07'),
(335, 'Leidy Carolina Valencia Aguirre', 'caritolc0720@yahoo.es', '3165178855', 'LEIDY CAROLINA VALENCIA AGUIRRE.pdf', 'AdministraciÃ³n y ventas', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-09'),
(337, 'Luis alfredo Reyes PopayÃ¡n', 'luisf-15-92@hotmail.com', '3207586595', 'NuevoDocumento 2019-09-30 08.55.47.pdf', 'Abogado', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-11-13'),
(338, 'MarÃ­a Fernanda Chaves Velasquez', 'mafernanda200@hotmail.com', '3128657308', 'HojadeVidaMafeAutodenar2.pdf', 'Asesores comerciales', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-11-16'),
(339, 'Erick David Bastidas Montenegro', 'ericmontenegro748@gmail.com', '3167971564', 'HOJA DE VIDA ERICK FINAL-FINAL.pdf', 'Desarrollador Freelance Full S', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-11-18'),
(340, 'Daniel Alexander Diaz', 'diazdaniel247@gmail.com', '3136107829', 'CV_DANIEL_DIAZ.pdf', 'Desarrollador freelance', 'Entre 1 y 2 aï¿½0ï¿½9os', '2019-11-19'),
(341, 'Alvaro Javier De La Cruz Delgado', 'alvaro17-02@hotmail.com', '3175211205', 'hoja de vida Alvaro Javier De La Cruz.pdf', 'Asesor Comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-27'),
(342, 'Jhon Armando', 'jhonrodriguez59@gmail.com', '3016284585', '', 'Asesor comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-27'),
(343, 'MarÃ­a Fernanda Chaves Velasquez', 'mafernanda200@hotmail.com', '3128657308', 'HojadeVidaMafeAutodenar2.pdf', 'Asesores comerciales', 'Entre 1 y 2 aÃ±os', '2019-11-27'),
(344, 'Franklin Adolfo GonzÃ¡lez Roso ', 'frank26306@gmail.com', '3043300723', '28110', 'Mecanico automotriz ', 'Entre 1 y 2 aÃ±os', '2019-11-27'),
(345, 'Daniel Alejandro Arturo Insuasty', 'danart24@hotmail.com', '3005695055', 'Hoja de vida Daniel Alejandro Arturo 2019.docx', 'Asesor comercial ', 'Entre 1 y 2 aÃ±os', '2019-11-27'),
(346, 'Francisco Javier Cordoba Chavez', 'Cordobafrancisco89@hotmail.com ', '3117407596', 'inbound1241562757619186013.pdf', 'Asesor comercial', 'Menos de un aÃ±o', '2019-11-27'),
(347, 'Ricardo IvÃ¡n Arteaga paguay', 'ivanarte25@gmail.com', '3235294072', 'inbound90476016338049336.pdf', 'Asesor comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-27'),
(348, 'Ricardo IvÃ¡n Arteaga Paguay', 'ivanarte25@gmail.com', '3235294072', 'inbound2147003208740697505.pdf', 'Asesor comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-27'),
(349, 'Ricardo IvÃ¡n arteaga paguay ', 'Ivanarte25@gmail.com', '3235294072', 'inbound2549492587634214000.pdf', 'Asesor comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-27');
INSERT INTO `empleos` (`id`, `nombres`, `correo`, `telefono`, `ruta`, `cargo`, `experiencia`, `Fecha_ingreso`) VALUES
(350, 'DEISY MARISOL ASMAZA ANAMA', 'marisolasmaza@gmail.com', '3023292187', 'hojadevidadeisy.pdf', 'auxiliar de cartera ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-27'),
(351, 'Jhon Fredy mera Arciniegas', 'jhonfredytaxbel@hotmail.com', '3167925180', '', 'Vendedor ejecutivo', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-27'),
(352, 'Camilo alberto RodrÃ­guez BenÃ­tez ', 'Camguez@hotmail.com', '3173439324', '', 'Asesor conercial', 'Entre 1 y 2 aÃ±os', '2019-11-27'),
(353, 'Jhon Armando rodriguez', 'jhonrodriguez59@gmail.com', '3016284585', '', 'Asesor', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-27'),
(354, 'Emerson Ferney Perez Lasso ', 'emerferney@gmail.com', '3135468516', 'Emerson Perez .pdf', 'Asesor comercial ', 'Menos de un aÃ±o', '2019-11-27'),
(355, 'Jhon Fredy', 'jhonfredytaxbel@hotmail.com', '3167925180', 'inbound1832628070.tmp', 'Vendedor ejecutivo', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-27'),
(356, 'William Rene PabÃ³n LÃ³pez ', 'William.pabon.lge@gmail.com ', '3122925298', 'inbound3170403437700458668.doc', 'Asesor comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-27'),
(357, 'Julio Cesar Delgado Hoyos ', 'jcdh2703@hotmail.com ', '3176925316', 'inbound3313575414605544745.pdf', 'Asesor comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-27'),
(358, 'Lileth Andrea ', 'lileka18@hotmail.com', '3158593292', 'HV LILETH ANDREA SALAS CAMPAÃ‘A..pdf', 'Asesor de Ventas ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-27'),
(359, 'Ismar melo ortega ', 'Ismarlive69@hotmail.com ', '3007508760', '', 'Asesor comercial ', 'Entre 1 y 2 aÃ±os', '2019-11-27'),
(360, 'Ismar melo ortega ', 'Ismarlive69@hotmail.com ', '3007508760', '', 'Asesor comercial ', 'Entre 1 y 2 aÃ±os', '2019-11-27'),
(361, 'Franco Giovanni Rodriguez Quelal', 'franco.rodriguez@uniminuto.edu.co', '3022407358', 'hoja de vida giovanni rodriguez.pdf', 'consultor master ux', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-27'),
(362, 'Yeison FabiÃ¡n Mora DÃ­az ', 'yfmora2907@gmail.com ', '3157014419', 'inbound2929720689891151332.tmp', 'Asesor Comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-28'),
(363, 'Nathalia Melissa Ortiz CÃ¡rdenas ', 'natha2795@gmail.com', '3187853953', 'inbound2320712938034675496.pdf', 'Asesor Comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-28'),
(364, 'Edison Fernando LÃ³pez Guerrero', 'eddyguerrero8906@gmail.com', '3234911451', 'inbound3559951000890656954.pdf', 'Asesor comercial', 'Menos de un aÃ±o', '2019-11-28'),
(365, 'Santiago Jose Estrada Guerrero', 'sjosestrada94@yahoo.es', '3005222728', 'hoja de vida Santiago Estrada .rtf', 'asesor comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-28'),
(366, 'Nathalia Melissa Ortiz CÃ¡rdenas ', 'natha2795@gmail.com', '3187853953', 'inbound2575487266638600019.pdf', 'Asesora comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-28'),
(367, 'Daniel Alexander Achicanoy RodrÃ­guez', 'danielachicanoy.serviautos@gmail.com', '3187205805', '', 'Asesor comercial', 'Entre 1 y 2 aÃ±os', '2019-11-28'),
(368, 'Brandon HernÃ¡n EnrÃ­quez Giron', 'brandone03@hotmail.com', '3158870309', 'hoja de vida BRANDON ENRIQUEZ.pdf', 'Asesor comercial', 'Ninguna', '2019-11-28'),
(369, 'Brandon HernÃ¡n EnrÃ­quez GirÃ³n ', 'brandone03@hotmail.com', '3158870309', 'hoja de vida BRANDON ENRIQUEZ.pdf', 'AtenciÃ³n de servicio al clien', 'Ninguna', '2019-11-28'),
(370, 'DIANA KAROLINA ERAZO HERNANDEZ', 'karito-2498@hotmail.com', '3136593955', 'Hoja de Vida Diana Karolina Erazo.pdf', 'Asesor de ventas', 'Entre 1 y 2 aÃ±os', '2019-11-28'),
(371, 'Jorge Andres Delgado LÃ³pez ', 'Jorgedelgado860414@gmail.com ', '3183809750', 'inbound2221478515087086079.pdf', 'Asesor comercial', 'Menos de un aÃ±o', '2019-11-28'),
(372, 'Pablo AndrÃ©s Dulce AraÃºjo ', 'padulce@hotmail.com ', '3116145084', 'inbound659485165717284872.docx', 'Asesor ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-28'),
(373, 'TANIA MARCELA CHAVES CHAUCANES', 'marce.tatis@hotmail.es', '3146180928', 'inbound8033005592445778038.docx', 'Aux administrativa', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-28'),
(374, 'TANIA MARCELA CHAVES CHAUCANES', 'marce.tatis@hotmail.es', '3146180928', 'inbound6407215234133244957.docx', 'Asesora ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-28'),
(375, 'AndrÃ©s Gerardo Legarda Valencia ', 'and_0208@hotmail.com', '3158661532', '', 'Asesor comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-28'),
(376, 'Tito PiandaArnero', 'titopianda@gmail.com', '3146936198', '', 'Auxiliar operativo', 'Entre 1 y 2 aÃ±os', '2019-11-28'),
(377, 'AndrÃ©s Gerardo Legarda Valencia ', 'and_0208@hotmail.com ', '3158661532', '', 'Asesor comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-28'),
(378, 'Javier Orlando Pantoja Rosero ', 'Javierpantojacp@hotmail.com', '3017356183', 'inbound7415928693132841841.doc', 'Asesor comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-28'),
(379, 'Niber Elvis Meneses Diaz ', 'Niver_0391@hotmail.com ', '3176972549', '', 'Asesor comercial ', 'Ninguna', '2019-11-28'),
(380, 'Niber Elvis Meneses Diaz ', 'Niver_0391@hotmail.com ', '3176972549', 'hoja-de-vida-Niber-Meneses 28-11-19.pdf', 'Asesor comercial ', 'Ninguna', '2019-11-28'),
(381, 'Niber Elvis Meneses Diaz ', 'Niver_0391@hotmail.com ', '3176972549', 'hoja-de-vida-Niber-Meneses 28-11-19.pdf', 'Asesor comercial ', 'Ninguna', '2019-11-28'),
(382, 'Niber Elvis Meneses Diaz ', 'Niver_0391@hotmail.com ', '3176972549', 'hoja-de-vida-Niber-Meneses 28-11-19.pdf', 'Asesor comercial ', 'Ninguna', '2019-11-28'),
(383, 'Niber Elvis Meneses Diaz ', 'Niver_0391@hotmail.com ', '3176972549', 'hoja-de-vida-Niber-Meneses 28-11-19.pdf', 'Asesor comercial ', 'Ninguna', '2019-11-28'),
(384, 'Niber Elvis Meneses Diaz ', 'Niver_0391@hotmail.com ', '3176972549', 'hoja-de-vida-Niber-Meneses 28-11-19.pdf', 'Asesor comercial ', 'Ninguna', '2019-11-28'),
(385, 'Diana Carolina Delgado NarvÃ¡ez ', 'dcdn07@gmail.com ', '3182900805', '', 'Asesora', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-28'),
(386, 'Daniel Enrique Sandoval bonilla', 'Danielsandoval0108@gmail.com ', '3128939627', '', 'Asesor en venyas', 'Entre 1 y 2 aÃ±os', '2019-11-28'),
(387, 'Mario Ricardo Chamorro GÃ³mez', 'marioricardochg@hotmail.com', '3206988411', 'HOJA DE VIDA  (1).pdf', 'Asesor Comercial', 'Entre 1 y 2 aÃ±os', '2019-11-28'),
(388, 'GIRALDO MANUEL LÃ“PEZ CORDPBA', 'giraldolopez123@gmail.com', '3127573056', 'hv giraldo formato libre completa popayan mar2019.pdf', 'ASESOR COMERCIAL', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-11-28'),
(389, 'Lady Andrea CÃ³rdoba Riascos', 'lacordoba@umariana.edu.co ', '3183349544', 'HOJA DE VIDA LADY ANDREA CORDOBA.pdf', 'Asesor comercial ', 'Menos de un aÃ±o', '2019-11-28'),
(390, 'Marilyn Geovanna Merchancano ', 'marilynmerchancano@gmail.com ', '3108386457', '', 'Asesor', 'Ninguna', '2019-11-28'),
(391, 'Luis Carlos Solarte Rosero ', 'luissolarte@umariana.edu.co', '3182629715', 'inbound4143179420823889788.docx', 'Asesor comercial ', 'Entre 1 y 2 aÃ±os', '2019-11-29'),
(392, 'Luis Carlos Solarte Rosero ', 'luissolarte@umariana.edu.co', '3182629715', 'inbound4175694910488612458.pdf', 'Asesor comercial ', 'Entre 1 y 2 aÃ±os', '2019-11-29'),
(393, 'Richard Antonio MejÃ­a Ortiz', 'mejiarichardortiz@hotmail.com', '3206585623', 'hoja de vida richard mejia o.pdf', 'Asesor Comercial ', 'Entre 1 y 2 aÃ±os', '2019-11-29'),
(394, 'Oscar Alexander Cuaspud Rosero', 'cuaspudoscar98@gmail.com', '3188057398', 'HOJA DE VIDA OSCAR CUASPUD (3).doc', 'Cajero y servicio al cliente.', 'Entre 1 y 2 aÃ±os', '2019-11-29'),
(396, 'jessica vanessa lopez ', 'jessicavanessa537@gmail.com ', '3233782395', 'hoja de vida lopez ceballos.pdf', 'Asesor comercial ', 'Ninguna', '2019-12-01'),
(398, 'Andres Mauricio Santacruz Portilla ', 'ansantacruzp@hotmail.com', '3168641054', 'HDV_ANDRES.docx1 (1).docx', 'Asesor Comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2019-12-03'),
(399, 'oscar ramiro hernandez benavides ', 'oscar291hernandez@hotmail.com', '3207205292', 'hoja de vida oscar hernandez.pdf', 'mantenimiento mecatronico de a', 'Entre 1 y 2 aÃ±os', '2020-01-14'),
(401, 'Nicolas Garces Castillo', 'nicogarza66@hotmail.com', '3183868535', 'CV.pdf', 'Administrador de Negocios Inte', 'Entre 1 y 2 aÃ±os', '2020-01-20'),
(402, 'Lisbeth Magaly Quiroz ', 'lis1613@hotmail.com', '3165861227', 'hoja de vida Lisbeth Quiroz', 'Administrativo ', 'Menos de un aÃ±o', '2020-01-21'),
(403, 'MIGUEL ARMANDO CARDENAS VALLEJO', 'MAIK170486@HOTMAIL.COM', '3142639174', 'HOJA_DE_VIDA_MIGUE 2020.pdf', 'ADMINISTRADOR', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-01-22'),
(404, 'CARLOS ARMANDO BURBANO GUSTIN ', '	carlosburbano485@gmail.com', '3219967213', '', 'ASISTENTE ADMINISTRATIVO .INVE', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-01-23'),
(405, 'AndrÃ©s Mauricio Santacruz Portilla', 'ansantacruzp@hotmail.com', '3168641054', 'HDV_ANDRES.docx', 'Comercial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-01-24'),
(406, 'Laura Cristina ', 'Lizarazo LobatÃ³n ', '3183400869', '1561651100652_1560954029030_hoja de vida Laura Lizarazo.pdf', 'Auxiliar contable y administra', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-01-29'),
(407, 'David Esteban MÃ©ndez Delgado', 'mendezd75@gmail.com ', '3232873927', 'Hoja de vida david esteban mendez 2020.pdf', 'Analista Marketing digital ', 'Entre 1 y 2 aÃ±os', '2020-01-30'),
(408, 'ARGENYS NATHALY CADENA ', 'nathalycadena6@gmail.com ', '3113677805', 'hoja de vida nathaly 2020 (2).pdf', 'Auxiliar contable y administra', 'Entre 1 y 2 aÃ±os', '2020-01-30'),
(409, 'RUDY XIMENA LEITON NASTUL', 'xleiton95@gmail.com', '3156457695', 'HOJA RUDY (4).pdf', 'ANALISTA DE MARKETING', 'Menos de un aÃ±o', '2020-01-31'),
(410, 'RUDY XIMENA LEITON NASTUL', 'xleiton95@gmail.com', '3156457695', 'HOJA RUDY (4).pdf', 'AUXILIAR ADMINISTRATIVO Y CONT', 'Menos de un aÃ±o', '2020-01-31'),
(411, 'Carlos LeÃ³n Zambrano ', 'Clickmarketingdigital1@gmail.com ', '3186260170', '', 'Experto en marketing digital ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-01-31'),
(412, 'Camilo Portillo Fajardo', 'camiloporti@hotmail.com', '3127890011', 'Curriculum Vitae Camilo Portillo Fajardo.pdf', 'Experto en Marketing Digitial', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-02-01'),
(413, 'Ricardo Paguay', 'ivanarte25@gmail.com', '3235294072', 'Copia de HOJA DE VIDA RICARDO PAGUAY.docx.pdf', 'Especialista automotriz chevro', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-02-03'),
(414, 'Claudia Vanessa Urrea Urresti', 'vanessablues02@gmail.com', '3105407057', 'HV_Claudia_Vanessa_Urrea.pdf', 'Sistemas', 'Menos de un aÃ±o', '2020-02-04'),
(415, 'Jasson David Montero Goyes ', 'yeikomonteroo_@live.com ', '3012682675', 'hoja de vida Jason Montero (1) (1).docx', 'Asesor comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-02-04'),
(416, 'Juan Carlos Zuleta Medina', 'juanzuleta7@hotmail.com', '3015554387', 'CVJuanZuleta210120soportes.pdf', 'Analista de Marketing Digital', 'Entre 1 y 2 aÃ±os', '2020-02-05'),
(417, 'HernÃ¡n DarÃ­o Mallama BeltrÃ¡n', 'Hdm2294@gmail.com', '3182882368', 'Hoja De Vida.pdf', '.', 'Entre 1 y 2 aÃ±os', '2020-02-05'),
(418, 'JULLY PAOLA AGUDELO BAEZ', 'ypab.17@hotmail.com', '3103405428', 'HOJA_JULLY_AGUGUDELO.pdf', 'Tesoreria Cartera', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-02-06'),
(420, 'Karen Jhoana Villanueva Carvajal', 'karenjhoana.09@gmail.com', '3105151880', 'Karen Johana Villanueva Carvajal.doc', 'Auxiliar Contable', 'Entre 1 y 2 aÃ±os', '2020-02-08'),
(421, 'Guillermo Esteban Trejos Rodriguez', 'getrejos9@misena.edu.co', '3146364131', 'HOJA DE VIDA totalizada.pdf', 'Tecnico', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-02-10'),
(422, 'Andres Esteban Nasmuta Granja', 'anasmuta@gmail.com', '3015268784', 'Ing. Andres Nasmuta Granja.pdf', 'Desarrollador Freelance', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-02-11'),
(423, 'Carlos Daniel LÃ³pez NarvÃ¡ez', 'lopezdaniel@udenar.edu.co', '3218356119', '', 'Vendedor/Asesor comercial', 'Menos de un aÃ±o', '2020-02-17'),
(424, 'Franco Giovanni Rodriguez Quelal', 'franco.rodriguez@uniminuto.edu.co', '3022407358', 'hoja de vida giovanni rodriguez.pdf', 'consultor master ux', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-02-18'),
(425, 'kateryne dayana pantoja moreno', 'kate.pantoja2020@hotmail.com', '3116256525', 'HV KATERINE PANTOJA 2020-convertido.pdf', 'cajera', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-02-18'),
(426, '', '', '', '', '', '', '0000-00-00'),
(427, 'Edgar Alexander BolaÃ±os Molina', 'eabolanos29@misena.edu.co', '3178735531', '', 'mecanico', 'Ninguna', '2020-02-24'),
(428, 'Leidy Johana Ramos Zamudio ', 'lady1390@hotmail.es ', '3113677869', 'hoja de vida Leidy.pdf', 'Auxiliar contable', 'Ninguna', '2020-02-26'),
(429, 'Leidy Johana Ramos Zamudio', 'lady1390@hotmail.es', '3113677869', 'hoja de vida Leidy.pdf', 'auxiliar contable', 'Menos de un aÃ±o', '2020-03-03'),
(430, 'Alejandra lopez', 'Monik_ira7877@hotmail.com', '3007728573', 'inbound4608308958246870533.pdf', 'Asesora comercial ', 'Entre 1 y 2 aÃ±os', '2020-03-04'),
(431, 'Alejandra LÃ³pez', 'Monik_ira7877@hotmail.com', '3007728573', 'inbound1380761152245923165.pdf', 'Asesora comercial', 'Entre 1 y 2 aÃ±os', '2020-03-04'),
(432, 'Piedad Vanessa RamÃ­rez Bastidas', 'darkmaiden86@gmail.com ', '3007450398', 'inbound5611613023489252186.pdf', 'Asesor - enfermera ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-03-04'),
(433, 'Silvio Enrique chamorro Chavez ', 'Pivochamorro@hotmail.com ', '3125437467', 'CurrÃ­culum Enrique 2020.pdf', 'Asesor comercial ', 'Entre 1 y 2 aÃ±os', '2020-03-04'),
(434, 'Diana Carolina OrdoÃ±ez Moreno ', 'diana.ordonezmoreno@hotmail.com ', '3103515509', 'inbound2952259334478246114.pdf', 'Asesor', 'Ninguna', '2020-03-05'),
(435, 'Dayana Katherine ', 'dkbenavides3@misena.edu.co', '3162694304', '', 'Auxiliar administrativo ', 'Menos de un aÃ±o', '2020-03-05'),
(436, 'Miguel Angel Mera GarzÃ³n ', 'Miguel12-24@hotmail.com', '3174955937', 'inbound3077667189602488107.pdf', 'Auxiliar administrativo ', 'Entre 1 y 2 aÃ±os', '2020-03-05'),
(437, 'Javier Andres ', 'Mora termal', '7280258', '', 'Asesor comercial ', 'Entre 1 y 2 aÃ±os', '2020-03-05'),
(438, 'Katherine Yulieth Ascuntar Ascuntar', 'kateas130@hotmail.com', '3167249688', 'HOJA DE VIDA YULIETH ASCUNTAR.pdf', 'Asistente comercial', 'Entre 1 y 2 aÃ±os', '2020-03-05'),
(439, 'Hanna Dalyle Santacruz Leon', 'santacruzhanna@gmail.com', '3156939857', 'HOJA DE VIDA HANNA DAYLE  S 94.docx', 'Auxiliar en enfermerÃ­a ', 'Menos de un aÃ±o', '2020-03-05'),
(440, 'Patricia Elizabeth Tobar ', 'Patty734_@hotmail.com ', '3206874263', 'PATRICIA ELIZABETH TOBAR - HOJA DE VIDA.pdf', 'Asistente comercial ', 'Entre 1 y 2 aÃ±os', '2020-03-05'),
(441, 'Marlen Daniela Pantoja Montiel', 'marlen96261@hotmail.com', '3137226246', 'MARLEN PANTOJA Diciembre 2019_.pdf', 'Asesor', 'Entre 1 y 2 aÃ±os', '2020-03-05'),
(442, 'Marlen Daniela Pantoja Montiel', 'marlen96261@hotmail.com', '3137226246', 'MARLEN PANTOJA Diciembre 2019_.pdf', 'Asistente comercial', 'Ninguna', '2020-03-05'),
(443, 'Leidy Camila Luna Torres', 'Leidycamilaluna@hotmail.com ', '3163811833', 'inbound5702746307524450220.pdf', 'Asesor Comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-03-06'),
(444, 'Edwin Quitiaquez Rosero ', 'Edwin_quitia@hotmail.com ', '3003370649', 'inbound473910678486891096.pdf', 'Asistente comercial ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-03-07'),
(445, 'Milton AndrÃ©s Arcos Prado', 'miland086@gmail.com', '3128820812', 'inbound2400694421379736779.pdf', 'PsicÃ³logo', 'Entre 1 y 2 aÃ±os', '2020-03-08'),
(446, '', '', '', '', '', '', '0000-00-00'),
(447, 'Juan Carlos Vivas caez', 'Jucavi04@hotmail.com', '3178908689', 'jucavi04.pdf', 'Asistente comercial ', 'Ninguna', '2020-03-13'),
(448, 'EDWIN ALFREDO BURGOS PANTOJA', 'alfredoburgospantoja@mail.com', '3117294809', 'HOJA DE VIDA EDWIN BURGOS 2020..pdf', 'Jefe de Negocios', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-03-18'),
(449, 'AdriÃ¡n Arturo Chuquizan Prado ', 'adrian041904@hotmail.com ', '3124958197', 'hojadevidaingenieromecÃ¡nico.pdf', 'Ingeniero mecÃ¡nico ', 'Menos de un aÃ±o', '2020-03-18'),
(450, 'Guido francisco Benavides Salazar', 'Anthonyjacobo1987@gmail.com', '3186551360', '', 'Acesor', 'Entre 1 y 2 aÃ±os', '2020-03-20'),
(451, '', '', '', '', '', '', '0000-00-00'),
(452, 'Fabio AndrÃ©s Wilches Camacho', 'fawil8389@gmail.com', '3222129723', '', 'TÃ©cnico en mantenimiento de m', 'Entre 1 y 2 aÃ±os', '2020-03-21'),
(453, '', '', '', '', '', '', '0000-00-00'),
(454, '', '', '', '', '', '', '0000-00-00'),
(455, 'Jairo AdriÃ¡n QuenÃ¡n caipe ', 'jairoquenan82@gmail.com ', '3132474901', '', 'MecÃ¡nico ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-04-13'),
(456, 'Diego Fernando Rosero Acevedo ', 'diegofdoroseroa@gmail.com', '3012777552', 'Hoja de vida Diego Rosero.docx', 'asistente comercial', 'Entre 1 y 2 aÃ±os', '2020-04-24'),
(457, 'TERESITA DE JESUS BENAVIDES REYES ', 'Terejesu11@hotmail.com', '3218554023', 'HOJA DE VIDA TERESA.pdf', 'ENFERMERA', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-05-07'),
(458, '', '', '', '', '', '', '0000-00-00'),
(460, 'Jenny Victoria Castillo Rodriguez ', 'jennycastillor9@hotmail.com', '3233387951', 'H V JENNY VICTORIA CASTILLO RODRIGUEZ +.pdf', 'enfermera auxiliar, tecnologa ', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-05-27'),
(461, 'dilan stiv zambrano mueses', 'dilanmotos217@gmail.com', '3113354003', '', 'electico automotriz', 'Mï¿½ï¿½s de dos aï¿½0ï¿½9os', '2020-05-27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `llamada`
--

CREATE TABLE `llamada` (
  `id` int(11) NOT NULL,
  `nombres` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vehiculo` longtext COLLATE utf8mb4_unicode_ci,
  `motivo` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Fecha_ingreso` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `llamada`
--

INSERT INTO `llamada` (`id`, `nombres`, `telefono`, `vehiculo`, `motivo`, `Fecha_ingreso`) VALUES
(3, 'luis carlos zuñiga', '2147483647', NULL, 'prueba', '0000-00-00'),
(4, 'Mario Alexander Medina', '2147483647', NULL, 'Cotizar Spark GT fe', '0000-00-00'),
(6, 'JUAN FELIPE GOMEZ', '3218732696', NULL, 'COTIZAR UN CARRO', '0000-00-00'),
(7, 'Antonio Hurtado', '3164538785', NULL, 'Cotizacion vehiculo', '0000-00-00'),
(9, 'Leidy Arboleda ', '3168259468', '', 'Estoy interesada en comprar un carro ', '0000-00-00'),
(10, 'andres alexander melo ', '', '', '', '0000-00-00'),
(11, 'Andres Alexander Melo ', '3186910141', '', 'COTIZACION DE CAMION NKR CON TERMO DE CONGELACION ', '0000-00-00'),
(12, 'Cambio ortega', '3153028861', '', 'Asesoria', '0000-00-00'),
(13, 'andres solarte', '3117819085', '', 'informacion para sacar carro financiado', '0000-00-00'),
(14, 'jose', '3123097156', '', 'cotizacion furgon doble cavina', '0000-00-00'),
(15, 'alexandra castillo', '3164388210', '', 'para solicitar yn credito de carro', '0000-00-00'),
(16, 'Jhon ', '3136462181', '', 'Comprar seguro', '0000-00-00'),
(17, 'EDIXON RAUL MENA', '3224487199', '', 'ME INTERESA UN CAMION QUE REQUISITOS SE NECESITO PARA ADQUIRIR UNO', '0000-00-00'),
(18, 'Lizeth Andrea Benavides Martinez', '3188371503', '', 'Infrmacion de la camioneta CHEVROLET Dmax 2.5 4X4 Doble Cabina con opcion de forma de pago con una camioneta Mazda BT50 modelo 2013\r\n', '0000-00-00'),
(22, 'eduardo coral', '3108402063', '', 'cotizacion', '0000-00-00'),
(23, 'Gabriel chicaiza', '3157652242', '', 'CotizaciÃ³n ', '0000-00-00'),
(25, 'NATALIA HUERTAS', '3122447308', '2009', '', '0000-00-00'),
(26, 'Antonio Mendoza', '3173757711', '', 'Deseo saber la posibilidad para adquirir el carro, gracias ', '0000-00-00'),
(27, 'Edwin cordoba ', '3014093982', '', 'Adquirir auto nuevo', '0000-00-00'),
(28, 'Jaime lopez', '593-993332239', '', 'Estoy volviendo del exterior a vivit al pais y estoy interesado en adquirir un vehiculo chevrolet , agradezco si pudieran contactarme,gracias', '2019-06-13'),
(29, 'EDWIN ANDRES GUERRERO MEJIA', '3168238029 3162712985', '', 'CONSULTA POSIBILIDAD DE ADQUIRIR VEHICULO', '2019-06-14'),
(30, 'Samir erney leiton rosero', '3172864502', '', 'CotizaciÃ³n Spark Life. Solicitud de crÃ©dito', '2019-06-27'),
(31, 'DORIS QUILISMAL', '3166287782', '', 'INFORMACION SOBRE AHORRO PROGRAMADO', '2019-07-02'),
(32, 'Robert ramirez', '3187413100', '', 'InterÃ©s en adquirir vehÃ­culo nuevo o usado, gracias', '2019-07-08'),
(33, 'Duvier Aguiar', '3102005236', '', 'Solicitar test drive ', '2019-07-09'),
(34, 'Duvier Aguiar ', '3102005235', 'Onix active', '', '2019-07-09'),
(35, 'Leydy viviana', '3136661401', '', 'Cotizacion', '2019-07-10'),
(36, 'Gerardo GarcÃ­a ', '3202126211 ', '', 'Cotizar gt', '2019-07-12'),
(37, 'GERARDO GARCÃA ', '3202126211 ', '', 'Cotizar spark lt', '2019-07-12'),
(38, 'Marlon Danilo', '3209607236', '', 'Compra de vehiculo financiado de 8 pasajeros usado', '2019-07-18'),
(39, 'Cristian Erira', '3183545129', '', 'Cotizacion', '2019-07-18'),
(40, 'Alex mauricio ', '3125846514', '2019', '', '2019-07-21'),
(41, 'Mauricio Mosquera', '3125846514', '', 'Consultar cheviplan', '2019-07-22'),
(42, 'Jessica Saya QuiÃ±oned', '3168256792', '', 'Quiero comprar un taxi', '2019-07-24'),
(43, 'Lenin Tarapues-Bomberos TÃºquerres', '3183435720', '', 'Cotizacion vehiculo NHR doble Cabina - con Furgon', '2019-07-30'),
(44, 'NAYIBE BENAVIDES ARTEAGA', '3167353168', '', 'QUIERO COTIZAR UN TAXI', '2019-08-02'),
(45, 'Luis francisco', '3106277807', '', 'Estoy interesado a un chebrole espark', '2019-08-05'),
(46, 'Gentil meneses', '3173624088', '', 'Financiamiento ', '2019-08-09'),
(47, 'JesÃºs orlando Ibarra Ruales ', '3174749027', '', 'Compra de vehÃ­culo financiado. ', '2019-08-15'),
(48, 'EVER VILLOTA', '3122084701', '', 'Si es posible sacar un carro a cuotas ???', '2019-08-17'),
(49, 'Juan SebastiÃ¡n ruiz', '3166997072', '', 'InformaciÃ³n venta del vehÃ­culo spark gt', '2019-08-21'),
(50, 'Andres salas', '3187028909', '', '', '2019-08-26'),
(51, 'ERNESTO RODRIGUEZ', '3117245367', '', 'ADQUIRIR VEHICULO', '2019-08-27'),
(52, 'John montilla', '3133881689', '', 'Financiamiento vehiculos', '2019-08-31'),
(53, 'Oscar Francisco MuÃ±oz Jojoa', '3113621325', '', 'Cotizar sparckGT', '2019-09-05'),
(54, 'SebastiÃ¡n Arciniegas', '3173448008', '', 'Compra de un frr', '2019-09-09'),
(55, 'Silvio Medardo EspaÃ±a', '', '', '', '2019-09-12'),
(56, 'Carlos Cuatin', '3204863229', '', 'Informacion sobre financiamiento', '2019-09-17'),
(57, 'Thomas vallejos ', '3214799507', '', 'Cotizar Unn vehÃ­culo nhr', '2019-09-19'),
(58, 'IvÃ¡n DarÃ­o FlÃ³rez PotosÃ­', '3184921654', '', 'Cotizar Auto', '2019-09-19'),
(59, 'Francy', '3113446777', 'Chevrolet tracker', '', '2019-09-21'),
(60, 'Pedro Luis oliva ', '3184799834', 'Chevrolet beat', '', '2019-09-26'),
(61, 'Pedro Luis oliva ', '3184799834', '', 'Planes de financiamiento para el Chevrolet beat, muchas gracias ', '2019-09-27'),
(62, 'JOSE CUARAN', '3125699151 - 3223686355', '', 'COTIZACION DE DMAX 2.5 L 4X4 FULL PARA SERVICIO ESPECIAL. ', '2019-10-03'),
(64, 'Wilmer', '3136499041', '', 'Cotizar vehiculo', '2019-10-17'),
(65, 'Jhon Freddy Palma agudelo', '3207299316', '', 'Comprar carro chevrolet spark', '2019-10-26'),
(66, 'Wilmer ortega', '3136499041', '2020', '', '2019-10-28'),
(67, 'Lilibeth', '3104226038', '', 'Cotizacion', '2019-10-29'),
(68, 'ADRIANA', '3154967129', '', 'COTIZACIÃ“N ', '2019-10-30'),
(69, 'Jaime SÃ¡nchez ', '3226438283', '', 'Precio de la Colorado ', '2019-10-30'),
(70, 'Esteban Canamejoy', '', 'Spark GT 2019', '', '2019-11-05'),
(71, 'Hugo alberto polo castro', '3165270742', '', 'Para averiguar credito', '2019-11-08'),
(72, 'OMAR ENRRIQUE ORTEGA', '3116048549', '', 'COTIZAR UN CAMION ', '2019-11-18'),
(73, 'JosÃ© Manuel caballero', '3126005723', '', 'Cotizacion', '2019-12-09'),
(74, 'Lina Jael FernÃ¡ndez Solarte', '3128045822', '', 'Solicitud, informaciÃ³n, Carro', '2019-12-15'),
(75, 'LiÃ±a fernandez', '3147343275', '', 'Cuotas para acceder a carro nuevo', '2019-12-15'),
(76, 'antidio bravo portilla', '3204790421', '', 'informacion ', '2019-12-17'),
(77, 'AndrÃ©s Ortega', '5584574666', '', 'AdquisiciÃ³n de una camioneta ', '0000-00-00'),
(78, 'Luz MarÃ­a Ortega ', '3146557782', '', 'Precio de carro estilo camioneta ', '2019-12-29'),
(79, 'Luz MarÃ­a Ortega ', '3146557782', '', 'Precio documentos ', '2019-12-29'),
(80, 'Geraldine', '3122869208', '', 'Quiero un vehÃ­culo ', '2019-12-30'),
(81, 'Oswaldo delgado muÃ±oz ', '3112559687', '', 'Quiero averiguar para la compra de un automÃ³vil ', '2020-01-08'),
(82, 'Wilson delgado', '3206439790', 'Frr', '', '2020-01-23'),
(83, 'Cristian calero', '311855869', '2020', '', '2020-02-03'),
(84, '', '', '', '', '0000-00-00'),
(85, 'jimmy benavides', '3127716331', '', 'cotizar y forma de pago de la van n300', '2020-02-24'),
(86, 'Duvan burbano', '3173730164', '', 'CotizaciÃ³n de un spark GTI ', '2020-02-25'),
(87, 'Henry Padilla', '3103361115', '', 'Compra de taxi nuevo', '2020-03-04'),
(88, 'JosÃ© jarol', '3176065317', '', 'Cotizar park GT lt', '2020-03-06'),
(89, 'Jose Elias Herrera', '3208248606', '2019', '', '2020-03-15'),
(90, 'ANA JULIETH RODRIGUEZ', '3156579434', '', 'COMPRA DE CARRO', '2020-03-17'),
(91, 'Jhon leider', '3166713575 ', '', 'Necesito el financiamiento de un carro', '2020-04-11'),
(92, 'Jesus', '3508866925', '', 'CotizaciÃ³n de vehÃ­culo', '2020-04-14'),
(93, 'Tatiana ruiz', '3104057453', '', 'Adquirir un auto ', '2020-04-29'),
(94, 'Jorge vallejo', '3147781384', '', 'Comprar carro  nuevo', '2020-04-29'),
(95, 'Jorge AdriÃ¡n Vallejo ', '3147781384', '', 'MÃ¡s informaciÃ³n de vehÃ­culos del plan de financiacion', '2020-04-30'),
(96, 'Sergio', '3123456122 ', '', 'AdquisiciÃ³n de vehÃ­culo ', '2020-05-06'),
(97, 'Julia RamÃ­rez', '3155887424', '', 'Quiero cotizar un vehÃ­culo y saber quÃ© modelos y precios', '2020-05-11'),
(98, 'Diego erazo', '3128415519 ', '', 'CotizaciÃ³n veiculo nqr\r\n\r\n', '2020-05-14'),
(99, 'Jonathan Alexander borrero tavera ', '3234886425', '', 'Compra de un taxi', '2020-05-15'),
(100, 'Giovanni eraso', '3159263467', '', 'CotizaciÃ³n vehÃ­culo ', '2020-05-16'),
(101, 'Edison Javier pantoja ', '3155249209 ', '', 'Para un financiamiento de un veiculo', '2020-05-21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peritaje`
--

CREATE TABLE `peritaje` (
  `id` int(11) NOT NULL,
  `cedula` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `peritaje`
--

INSERT INTO `peritaje` (`id`, `cedula`, `nombre`, `apellido`, `celular`, `email`, `fecha`, `hora`) VALUES
(1, '87455348', 'Hammer', 'Bastidas', '3167451707', '####', '0217-10-17', '10:00:00'),
(2, '3333', 'ss', 'sss', '333', 'sss', '2019-01-01', '01:00:00'),
(3, '23', 'ww', 'ww', '232', 'ww', '2019-01-01', '07:01:00'),
(4, '36754932', 'Natalia', 'Huertas', '3122447308', 'nataliahuertas19@hotmail.com', '2019-05-25', '15:00:00'),
(5, '711472', 'Francisco ', 'Andrade', '####', 'francisco.andrade.z@gmail.com', '2019-06-18', '10:00:00'),
(6, '52690023', 'DIANA', 'GUARIN', '3176389776', 'dimaguqui@hotmail.com', '0000-00-00', '00:00:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan`
--

CREATE TABLE `plan` (
  `id_plan` int(11) NOT NULL,
  `tipo` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `plan`
--

INSERT INTO `plan` (`id_plan`, `tipo`) VALUES
(11, 'ESTANDAR'),
(22, 'SILVER'),
(33, 'PLUS'),
(44, 'GOLD');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan_adquirido`
--

CREATE TABLE `plan_adquirido` (
  `id_pa` int(11) NOT NULL,
  `id_plan` int(11) NOT NULL,
  `nit_cliente` varchar(200) NOT NULL,
  `fecha_compra` date NOT NULL,
  `usado` varchar(10) NOT NULL,
  `fecha_usado` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posventa`
--

CREATE TABLE `posventa` (
  `id` int(11) NOT NULL,
  `area` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modelo` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombres` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `posventa`
--

INSERT INTO `posventa` (`id`, `area`, `modelo`, `nombres`, `email`, `telefono`, `tipo`, `descripcion`) VALUES
(2, 'Accesorios', 'prueba2', 'prueba2', 'prueba2', 'prueba2', 'prueba2', 'prueba2'),
(5, 'Repuestos', 'Captiva sport 2.4 20', 'NELSON JAVIER CHICAIZA BARAHONA', 'javi_nel@yahoo.es', '', '', 'Buenos dÃ­as, necesito saber si disponen de un catalizador para la chevrolet captiva 2.4 aÃ±o 2011 y cual es el costo, gracias.'),
(10, 'Repuestos', 'suzuki grand vitara ', 'Carlos Loor Narvaez', 'c.javierloor@hotmail.com', '+593982441', 'tipo', 'Buenos dias, quisiera adquirir las bases del motor y de la caja de transmision manual del Suzuki Gran Vitara aÃ±o 2011 motor a gasolina 2.0   4x2 (motor J20A714XXX)'),
(12, 'Repuestos', 'Sonic 2014', 'Miguel labbao', 'labbaop81@gmail.com', '3223050330', 'Natural', 'Buenas noches necesito saber si tienen el kit de empaque de enfriador de aceite para el Chevrolet sonic '),
(13, 'Repuestos', 'tahoe hÃ­brida 5p6.0', 'Rurik Fedrich', 'rurik.dm@gmail.com', '3168567765', 'nuevo', 'averiguar repuesto'),
(15, 'Repuestos', 'Zafira', 'Jaime Silva', 'jaimesilva59@hotmail.com', '0059399939', 'nuevo', 'busco espejos exteriores x zafira 2004'),
(16, 'Repuestos', 'SPARK', 'DIEGO MURIEL', 'diego13069@hotmail.com', '3182178233', 'nuevo', 'necesito el espejo retrovisor con direccional izquierdo del spark gt modelo 2016'),
(17, 'Repuestos', 'Captiva', 'James Robert Erazo Segovia', 'robert_e1@hotmail.com', '0995004958', '', ''),
(19, 'Repuestos', 'Traker', 'Fernando Santiago Teran', 'fteran2000ec@yahoo.com', '23265597', 'nuevo', 'necesito consultar llista de respuestos\r\npara motor del auto\r\n'),
(27, 'Taller', 'CAPTIVA SPORT', 'LUIS ALEJANDRO', 'matiasalejo.silva@gmail.com', '5930981180', 'Nuevo', 'Taller, tengo problemas con el sensor del catalizador, al parecer debo hacer el cambio, y tambien quiero un cambio de aceite. Quiero conocer costos y si puedo reservar para que me atiendan el sÃ¡bado 21 de Septiembre, la idea es viajar que me hagan el mantenimiento, por favor confirme si me entregan el auto el mismo sÃ¡bado.'),
(32, 'Taller', 'Chevrolet Vivant A/T', 'Nohora Realpe ', 'nohreal@gmail.com ', '3105932875', '', 'RevisiÃ³n mecÃ¡nica '),
(36, 'Accesorios', 'Sail', 'Lorena Orbea Andrade', 'lorena.orbea@chubb.com', '(593)98784', '', 'por favor quisiera realizar una cotizaciÃ³n para colocar aire acondicionado a mi auto, es un Sail 1.4 del aÃ±o 2015'),
(39, '', '', '', '', '', '', ''),
(40, '', '', '', '', '', '', ''),
(41, '', '', '', '', '', '', ''),
(42, '', '', '', '', '', '', ''),
(43, '', '', '', '', '', '', ''),
(44, 'Repuestos', 'Chevrolet Dmax 4x2 C', 'Mario Humberto Andrade Narvaez', 'mandrade_364@hotmail.com', '0995475797', 'Privado', 'Estimados SeÃ±ores\r\n\r\nFavor si me cotizan la bomba de la direccion hidraulica del vehiculo en referencia\r\nGracias por su atencion\r\nSaludos'),
(45, 'Repuestos', 'VITARA SZ', 'Edison Galarraga', 'edison.galarraga@gmail.com', '593986105498', '', 'Estimados necesito una cotizacion de repuestos para un Vitara SZ, por favor que me envien un correo o un mjs de whats up para enviar el listado, muchas gracias'),
(46, 'Accesorios', 'Spark GT', 'Mario Alexander Medina', 'alxmedina17@gmail.com', '3173485047', 'persona', 'Necesito saber el formato de video del radio en un chevrolet spark GT 2019'),
(47, 'Repuestos', 'Suzuki o Chevrolet A', 'FabiÃ¡n Barros Carmona', 'fabian_barroscarmona@hotmail.com', '593999806505', '', 'CotizaciÃ³n compuerta trasera, guardafango, puertas de Suzuki o Chevrolet Alto 2003.'),
(48, '', '', '', '', '', '', ''),
(49, '', '', '', '', '', '', ''),
(50, '', '', '', '', '', '', ''),
(51, '', '', '', '', '', '', ''),
(52, '', '', '', '', '', '', ''),
(53, '', '', '', '', '', '', ''),
(54, 'Repuestos', 'FVZ 34T CAMION AC 7.', 'Cristian Faustino EcheverrÃ­a Olmedo', 'cristianeolid@hotmail.com', '0991986610', 'Cliente de Ecuador', 'Consulta de disponibilidad y precio de los inyectores y bomba de agua por favor... motor 6HK1...'),
(55, 'Taller', '2013', 'FREDY CARDENAS BRAVO', 'camcholo@gmail.com', '3103395623', 'externo', 'cambio de aceite'),
(56, '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicidad`
--

CREATE TABLE `publicidad` (
  `id` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `ruta` mediumtext CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `publicidad`
--

INSERT INTO `publicidad` (`id`, `fecha`, `ruta`) VALUES
(11, '0000-00-00 00:00:00', 'equinox.jpg'),
(12, '0000-00-00 00:00:00', 'equinox2.jpg'),
(13, '0000-00-00 00:00:00', 'equinox1.jpg'),
(14, '0000-00-00 00:00:00', 'promocion.jpg'),
(15, '0000-00-00 00:00:00', 'promocion.jpg'),
(16, '0000-00-00 00:00:00', 'promocion.jpg'),
(17, '0000-00-00 00:00:00', 'publlicidad2.png'),
(18, '0000-00-00 00:00:00', 'cuidado redes adaptado-05.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `repuestos`
--

CREATE TABLE `repuestos` (
  `id` int(11) NOT NULL,
  `cedula` varchar(30) NOT NULL,
  `nombres` varchar(70) NOT NULL,
  `celular` int(10) NOT NULL,
  `email` mediumtext NOT NULL,
  `vehiculo` mediumtext NOT NULL,
  `repuesto` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `repuestos`
--

INSERT INTO `repuestos` (`id`, `cedula`, `nombres`, `celular`, `email`, `vehiculo`, `repuesto`) VALUES
(1, '11', 'prueba', 1, 'prueba', 'prueba', 'prueba'),
(3, '1713530762', 'Jonathan Vicente Bastidas Cevallos', 984060493, 'jonsibc@yahoo.com', '', 'Faro delantero derecho SZ Next Sport 2015. Guardafango delantero RH SZ Next Sport. Cubierta neblinero RH GV 2014.  Rejilla inferior guardachoque delantero SZ Next. Rejilla radiador GV 2014. Enderezada CapÃ³. Pintura de las partes.'),
(4, '1717573644', 'NELSON MORENO', 992998120, 'cancer.nelson@hotmail.com', '8', 'EINER KIT'),
(5, '1086359117', 'Jorge Alfredo Padilla burbano', 2147483647, 'Jpadilla. 19@hotmail.com', '2', 'Activador de puerta'),
(6, '11434195', 'Ricardo OcaÃ±a Paz', 2147483647, 'ricardoop31@gmail.com', '6', 'Juego de bujÃ­as'),
(7, '-1', 'OSCAR SILVER GILON BOLAÃ‘OS', 2147483647, 'ogilon@sedputumayo.gov.co', '', 'brazo y plumilla de limpia briza trasero chevoiet grand vitara modelo 2009 y la correa de distribuciÃ³n para el mismo vehiculo'),
(8, '94384804', 'Pablo andres Bastidas Meneses', 2147483647, 'Pablobasti@gmail.com ', '1', 'Apertura tapa de gasolina aveo'),
(9, '94384804', 'Pablo andres Bastidas Meneses', 2147483647, 'Pablobasti@gmail.com ', '1', 'Apertura tapa de gasolina aveo'),
(10, '94384804', 'Pablo andres Bastidas Meneses', 2147483647, 'Pablobasti@gmail.com ', '1', 'Apertura tapa de gasolina aveo'),
(11, '1004189312', 'JUAN CARLOS MORILLO ORTEGA', 2147483647, 'juanc.312@hotmail.com', '8', 'chapa completa de la puerta delantera izquierda (conductor)'),
(12, '1004189312', 'JUAN CARLOS MORILLO ORTEGA', 2147483647, 'juanc.312@hotmail.com', '8', 'chapa completa de la puerta delantera izquierda (conductor)'),
(13, '0501875926', 'Franklin Changoluisa', 2147483647, 'fgcha@icloud.com', '23', 'caja'),
(14, '18111425', 'JAVIER SOLARTE', 2147483647, 'nesjava@hotmai.com', '3', 'balineras y sello del compresor del aire acondicionado'),
(15, '1085263574', 'Mario PÃ©rez', 2147483647, 'marioandres.perez@gmail.com', '3', 'Antena'),
(16, '18111425', 'JAVIER SOLARTE', 2147483647, 'nesjava@hotmail.com', '3', 'Necesito las balineras y sello del compresor del aire acondicionado, favor informarme si los hay y que costo tienen.'),
(17, '18111425', 'JAVIER SOLARTE', 2147483647, 'nesjava@hotmail.com', '3', 'Necesito las balineras y sello del compresor del aire acondicionado, favor informarme si los hay y que costo tienen.'),
(18, '98390295', 'andres de la vega', 2147483647, 'anjodelavesa@gmail.com', '', 'sensor de oxigeno captiva 2.4'),
(19, '1710193227', 'Ketty Jimbo Santana', 984073516, 'kettyjimbo@yahoo.es', '', 'caja de la direccion de un gran vitara sz 2011'),
(20, '1714668603', 'Gabriela Guerra', 983302297, 'ggamdreaavalos3@hotmail.com', '2', 'retrovisor izquierdo con direccional'),
(21, '1087960249', 'daniel esteban insuasty martinez', 2147483647, 'estebanim@hotmail.es', '11', 'buenas tardes, el la linea del vehiculo no se ecuentra el chevrolet sonic, necesito el termostato de este vehiculo, chevrolet sonic modelo 2015'),
(22, '-1711404275', 'James Robert Erazo Segovia', 995004958, 'robert_e1@hotmail.com', '13', 'kit distribuciÃ³n'),
(23, '-1711404275', 'James Robert Erazo Segovia', 995004958, 'robert_e1@hotmail.com', '13', 'kit distribuciÃ³n'),
(24, '-1711404275', 'James Robert Erazo Segovia', 995004958, 'robert_e1@hotmail.com', '13', 'kit distribuciÃ³n'),
(25, '1716632219', 'ROLANDO VARGAS', 2147483647, 'rv-parts@hotmail.com', '12', '8971771180'),
(26, '59660531', 'MARIA CRISTINA RAMIREZ ', 2147483647, 'glocarivan@hotmail.com', '18', 'RETENEDOR  SIGUENAL PARTE DE ATRAS '),
(27, '59660531', 'MARIA CRISTINA RAMIREZ ', 2147483647, 'glocarivan@hotmail.com', '', 'RETENEDOR SIGUEÃ‘AL PARTE DE TRACERA, DELANTELA  DE UN VEHICULO COBAL '),
(28, '12906785', 'CARLOS  GUILLERMO RAMIREZ ', 2147483647, 'glocarivan@hotmail.com', '', '1. RETENEDOR DE ARCO DE LAVA '),
(29, '12906785', 'CARLOS RAMIREZ ', 2147483647, 'glocarivan@hotmail.com', '', '1. RETENEDOR DE ARCO DE LAVA '),
(30, '12906785', 'CARLOS RAMIREZ ', 2147483647, 'glocarivan@hotmail.com', '', 'CAJA PLASTICA CORREA DE TIEMPO '),
(31, '1719709154', 'Geovana Lourdes Moreno Fierro', 996467220, 'gmore112247@gmail.com ', '3', 'Puerta delantera derecha '),
(32, '0914019468', 'ROSA TIGRERO', 2147483647, 'cadizatb@gmail.com', '2', 'son varios, me ayudaria si me escibe a mi correo '),
(33, '1085660533', 'Ãlvaro AndrÃ©s Espinoza BolaÃ±os ', 2147483647, 'andru8711@hotmail.com ', '', 'Stop chevrolet captiva sport 3.0 2011'),
(34, '1085660533', 'Ãlvaro AndrÃ©s Espinoza BolaÃ±os ', 2147483647, 'andru8711@hotmail.com ', '', 'Stop chevrolet captiva sport 3.0 2011'),
(35, '1003403944', 'TATIANA VERONICA PAGLLACHO CANCAN', 988928240, 'tatianapagllachocanacn@gmail.com', '3', 'P12-17-53E'),
(36, '1712098910', 'fabricio dillon', 2147483647, 'fabricio.dillon@gmail.com', '28', 'homocinetica captiva sport 3.6 2010'),
(37, '1712098910', 'fabricio dillon', 2147483647, 'fabricio.dillon@gmail.com', '28', 'homocinetica captiva sport 3.6 2010'),
(38, '1721700647', 'Juan Mollocana', 2147483647, 'jmollocana@ups.edu.ec', '3', 'Computador 24106050 + Cables de conexiÃ³n para Sail Hatchback 1.4 2014'),
(39, '123659', 'carlos', 318567894, 'aa@aa.com', '4', 'pastiillas'),
(40, '1103535256', 'Elias castillo', 982347519, 'pato83loja@gmail.com', '', 'Resonador y depurafor d aire optra'),
(41, '1081593953', 'milton cabrera', 2147483647, 'alextulcan021@gmail.com', '3', 'pila del tanque de gasolina'),
(42, '93405350', 'Antonio Jesus Osorio Noguera', 2147483647, 'ingenierosorio@gmail.com', '6', 'bms'),
(43, '1000874964', 'Mario Andrade Narvaez', 995475797, 'mandrade_354@hotmail.com', '8', 'Kit de banda de distribucion'),
(44, '1000874964', 'Mario Andrade Narvaez', 995475797, 'mandrade_354@hotmail.com', '8', 'Banda de distribucion para Dmax 4x2 v6 3500 cc aÃ±o 2008 a gasolina'),
(45, '1000874964', 'Mario Andrade Narvaez', 995475797, 'mandrade_354@hotmail.com', '8', 'Banda de distribucion para Dmax 4x2 v6 3500 cc aÃ±o 2008 a gasolina');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `id_servicio` int(11) NOT NULL,
  `nombre` varchar(150) CHARACTER SET utf8 NOT NULL,
  `id_spf` int(11) NOT NULL,
  `id_spm` int(11) NOT NULL,
  `id_spe` int(11) NOT NULL,
  `id_sg` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`id_servicio`, `nombre`, `id_spf`, `id_spm`, `id_spe`, `id_sg`) VALUES
(1, 'Cambio aceite motor', 1, 4, 7, 10),
(2, 'Cambio aceite caja cambios', 1, 4, 7, 10),
(3, 'Cambio aceite dirección hidráulica', 1, 4, 7, 10),
(4, 'cambio refrigerante motor', 1, 4, 7, 10),
(5, 'Cambio liquido de frenos', 1, 4, 7, 10),
(6, 'Cambio kit distribución', 1, 4, 7, 10),
(7, 'Cambio de kit de distribución', 1, 4, 7, 10),
(8, 'Cambio de pastillas de freno', 1, 4, 7, 10),
(9, 'Revisión de km', 1, 4, 7, 10),
(10, 'Calibración e inflado de las llantas con nitrógeno', 1, 4, 7, 10),
(11, 'Alineación delantera y trasera', 1, 4, 7, 10),
(12, 'Balanceo de las cuatro llantas', 1, 4, 7, 10),
(13, 'Rotación de llantas', 1, 4, 7, 10),
(14, 'Ajuste del sistema de suspensión delantera y trasera', 1, 4, 7, 10),
(15, 'Análisis de códigos', 1, 4, 7, 10),
(16, 'Restablecimiento de valores electrónicos', 1, 4, 7, 10),
(17, 'Eliminar historial de códigos', 1, 4, 7, 10),
(18, 'Vehículo para viaje', 2, 5, 8, 11),
(19, 'Vehículo sustituto', 2, 5, 8, 11),
(20, 'Vehículo para un amigo', 2, 5, 8, 11),
(21, 'Vehículo para fechas especiales', 2, 5, 8, 11),
(22, 'Mantenimiento vehículo día pico y placa', 2, 5, 8, 11),
(23, 'Asistencia en caso de colisión', 2, 5, 8, 11),
(24, 'Asistencia vehículo bloqueado', 2, 5, 8, 11),
(25, 'Lavado exterior vehículo', 2, 5, 8, 11),
(26, 'Lavado de motor', 2, 5, 8, 11),
(27, 'Lavado de chasis', 2, 5, 8, 11),
(28, 'Lavado de cojineria ', 2, 5, 8, 11),
(29, 'Servicio de aspirado', 2, 5, 8, 11),
(30, 'Embellecimiento de llantas', 2, 5, 8, 11),
(31, 'Porcelanizado', 2, 5, 8, 11),
(32, 'Limpieza de stop y farolas', 2, 5, 8, 11),
(33, 'Retoque de pintura', 2, 5, 8, 11),
(34, 'Reparación de golpes y rayones leves', 2, 5, 8, 11),
(35, 'Rejuvenecimiento de partes plasticas', 2, 5, 8, 11),
(36, 'Mantenimiento de vehículo día pico y placa', 2, 5, 8, 11),
(37, 'Asistencia en caso de colisión', 2, 5, 8, 11),
(38, 'Asistencia vehículo bloqueado', 2, 5, 8, 11),
(39, 'Limpieza de ambiente de interior del vehículo', 2, 5, 8, 11),
(40, 'Tele asistencia técnica', 2, 5, 8, 11),
(41, 'Recarga de extintor', 2, 5, 8, 11),
(42, 'Visita taller con servicio de taxi incluido', 2, 5, 8, 11),
(43, 'Peritaje gratis', 2, 5, 8, 11),
(44, 'Batería descargada', 3, 6, 9, 12),
(45, 'Fuga de líquidos', 3, 6, 9, 12),
(46, 'Testigos encendidos', 3, 6, 9, 12),
(47, 'Perdida de control de alarma', 3, 6, 9, 12),
(48, 'Ruido anormal', 3, 6, 9, 12),
(49, 'Llantas pinchadas', 3, 6, 9, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios_adquiridos`
--

CREATE TABLE `servicios_adquiridos` (
  `id_sa` int(11) NOT NULL,
  `id_pa` int(11) NOT NULL,
  `id_servicio` int(11) NOT NULL,
  `fecha_compra` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subplan`
--

CREATE TABLE `subplan` (
  `id_sp` int(11) NOT NULL,
  `id_plan` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `subplan`
--

INSERT INTO `subplan` (`id_sp`, `id_plan`, `nombre`, `cantidad`) VALUES
(1, 11, 'FUNDAMENTALES', 2),
(2, 11, 'MEJORA', 6),
(3, 11, 'EMERGENCIA', 2),
(4, 22, 'FUNDAMENTALES', 2),
(5, 22, 'MEJORA', 6),
(6, 22, 'EMERGENCIA', 4),
(7, 33, 'FUNDAMENTALES', 3),
(8, 33, 'MEJORA', 8),
(9, 33, 'EMERGENCIA', 4),
(10, 44, 'FUNDAMENTALES', 4),
(11, 44, 'MEJORA', 10),
(12, 44, 'EMERGENCIA', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usados`
--

CREATE TABLE `usados` (
  `idv` int(11) NOT NULL,
  `vehiculo` varchar(100) DEFAULT NULL,
  `placa` varchar(10) DEFAULT NULL,
  `modelo` varchar(5) DEFAULT NULL,
  `km` varchar(100) DEFAULT NULL,
  `motor` varchar(15) DEFAULT NULL,
  `color` varchar(30) DEFAULT NULL,
  `precio` varchar(100) DEFAULT NULL,
  `aireac` varchar(5) DEFAULT NULL,
  `airbag` varchar(5) DEFAULT NULL,
  `asiento` varchar(30) DEFAULT NULL,
  `vendido` tinyint(1) DEFAULT '1',
  `img` text,
  `img1` text NOT NULL,
  `img2` text NOT NULL,
  `img3` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usados`
--

INSERT INTO `usados` (`idv`, `vehiculo`, `placa`, `modelo`, `km`, `motor`, `color`, `precio`, `aireac`, `airbag`, `asiento`, `vendido`, `img`, `img1`, `img2`, `img3`) VALUES
(15, 'Sail Ltz', '0', '2020', '0', '1400', 'rojo', '$39.400.000', NULL, NULL, NULL, 1, '../usados/img/a2/DSC_0450.JPG', '../usados/img/a2/', '../usados/img/a2/', '../usados/img/a2/');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `social_id` varchar(100) NOT NULL,
  `picture` varchar(250) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `password`, `social_id`, `picture`, `created`) VALUES
(1, 'luis', 'luis@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '2019-06-08 12:36:35'),
(2, 'Alejandro Narvaez', 'alejandro.narvaez@autodenar.com.co', '14b0e7731b9b7b4b2b2ab13f99f32647', '', '', '2019-06-10 07:06:09'),
(3, 'Luis carlos zuÃ±iga', 'luiscarloszuniga@autodenar.com.co', '14b0e7731b9b7b4b2b2ab13f99f32647', '', '', '2019-06-10 07:07:32'),
(4, 'mercadeo', 'mercadeo2@automotoradelsur.com', '14b0e7731b9b7b4b2b2ab13f99f32647', '', '', '2019-06-10 07:08:13'),
(5, 'mercadeo', 'mercadeo@automotoradelsur.com', '14b0e7731b9b7b4b2b2ab13f99f32647', '', '', '2019-06-10 07:08:56'),
(6, 'mercadeo', 'mercadeo@autodenar.com.co', '14b0e7731b9b7b4b2b2ab13f99f32647', '', '', '2019-06-10 07:09:26'),
(7, 'Luis carlos zuÃ±iga', 'lczch@outlook.com', '14b0e7731b9b7b4b2b2ab13f99f32647', '', '', '2019-07-12 16:24:13'),
(8, 'Wilmer diaz', 'wilmer.diaz@autodenar.com.co', '14b0e7731b9b7b4b2b2ab13f99f32647', '', '', '2019-07-25 09:24:08'),
(9, 'AndrÃ©s Felipe TerÃ¡n Ortega', 'andresteran@gmail.com', '281d5cbef8ded4e9bee409e3b9c67ab2', '', '', '2019-10-07 11:58:41'),
(10, 'Carolina', 'carolina@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '2019-11-05 09:06:11'),
(11, 'Director', 'alejandros.narvaez@autodenar.com.co', '827ccb0eea8a706c4c34a16891f84e7b', '', '', '2019-11-14 15:29:54'),
(12, 'ANDREA HIDALGO', 'andrea.hidalgo@autodenar.com.co', 'e10adc3949ba59abbe56e057f20f883e', '', '', '2020-01-08 13:30:45'),
(13, 'Luis Carlos zuÃ±iga', 'soporte@autodenar.com.co', '14b0e7731b9b7b4b2b2ab13f99f32647', '', '', '2020-01-11 13:50:14'),
(14, 'Michael Achicanoy', 'michael.achicanoy@autodenar.com.co', '1032434fa738d9f98f7b8bea8f8ffbd4', '', '', '2020-02-10 15:49:19'),
(15, 'pruebas', 'prueba@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '2020-04-19 01:30:57'),
(16, 'pruebas', 'prueba@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '2020-04-19 01:44:01'),
(17, 'pruebas', 'prueba@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '2020-04-19 01:44:03'),
(18, 'pruebas', 'prueba2@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '2020-04-19 01:44:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuario` varchar(50) NOT NULL,
  `clave` varchar(35) NOT NULL,
  `codrol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuario`, `clave`, `codrol`) VALUES
('404045', '345', 2),
('404047', '345', 2),
('404048', '345', 2),
('404049', '345', 2),
('404050', '345', 2),
('404051', '345', 2),
('404052', '345', 2),
('404053', '345', 2),
('404046', '345', 2),
('jfajardo', '314771942', 2),
('anarvaez', '3218732696', 2),
('314771942', 'j314771942', 4),
('dms', '1307000', 4),
('Maria Fernanda lopez', '12345', 5),
('mlopez', '12345', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculos`
--

CREATE TABLE `vehiculos` (
  `id_v` int(11) NOT NULL,
  `modelo` varchar(100) CHARACTER SET utf8 NOT NULL,
  `tipo` varchar(100) CHARACTER SET utf8 NOT NULL,
  `precio` varchar(100) CHARACTER SET utf8 NOT NULL,
  `catego` varchar(1) CHARACTER SET utf8 NOT NULL,
  `ruta` text CHARACTER SET utf8,
  `color1` text CHARACTER SET utf8 NOT NULL,
  `color2` text CHARACTER SET utf8 NOT NULL,
  `color3` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `vehiculos`
--

INSERT INTO `vehiculos` (`id_v`, `modelo`, `tipo`, `precio`, `catego`, `ruta`, `color1`, `color2`, `color3`) VALUES
(2, 'carros', 'Spark GT', '$ 33.990.000*', 'l', 'img/minicars/sparckgt.png', 'sg1.png', 'sg2.jpg', 'sg3.jpg'),
(4, 'Medianos', 'Onix', '$ 45.990.000*', 'l', 'img/minicars/onix.png', 'o1.png', 'o2.png', 'o3.png'),
(6, 'Camionetas', 'Tracker', '$ 67.900.000*', 'l', 'img/minicars/tracker.png', 't1.png', 't2.png', 't3.png'),
(8, 'Camionetas', 'Dmax CD', '$ 85.000.000*', 'l', 'img/minicars/dmax.png', '', '', 'dm1.png'),
(9, 'Van', 'N300 Cargo', '$50.140.000*', 'l', 'img/minicars/n300c.png', '', '', 'n3c.png'),
(10, 'Van', 'N300 Pasajeros', '$ 47.740.000*', 'l', 'img/minicars/n300p.png', '', '', 'n3p.png'),
(12, 'Premium', 'Trailblazer', '$ 143.990.000*', 'l', 'img/minicars/trailblazer.png', 'tra1.png', '', 'tra2.png'),
(13, 'Premium', 'Traverse', '$ 144.990.000*', 'l', 'img/minicars/traverse.png', 'trav1.png', '', ''),
(14, 'Premium', 'Tahoe', '$ 239.990.000*', 'l', 'img/minicars/tahoe.png', 'tah1.png', 'tah2.png', 'tah3.png'),
(18, 'Taxis', 'ChevyTaxi Plus', '$ 49.990.000*', 'l', 'img/minicars/chevytaxi.png', '', '', ''),
(19, 'Camiones', 'NHR Reward', 'TU ALCANCE ', 'p', 'img/minicars/NKR.jpg', '', '', ''),
(20, 'Camiones', 'NHR Doble Cabinan Reward', 'TU ALCANCE ', 'p', 'img/minicars/NKR.jpg', '', '', ''),
(21, 'Camiones', 'NKR Reward', 'TU ALCANCE ', 'p', 'img/minicars/NKR.jpg', '', '', ''),
(22, 'Camiones', 'NNR Reward', 'TU ALCANCE ', 'p', 'img/minicars/NNRR.jpg', '', '', ''),
(23, 'Camiones', 'NPR Reward', 'TU ALCANCE ', 'p', 'img/minicars/NKR.jpg', '', '', ''),
(24, 'Buses', 'Bus NKR Reward', 'TU ALCANCE ', 'p', 'img/minicars/NKRR.jpg', '', '', ''),
(25, 'Buses', 'Bus NPR Reward', 'TU ALCANCE ', 'p', 'img/minicars/NPRR.jpg', '', '', ''),
(26, 'Buses', 'Bus NQR Reward', 'TU ALCANCE ', 'p', 'img/minicars/NQRR.jpg', '', '', ''),
(27, 'Camionetas', 'COLORADO', '$ 124.990.000', 'l', 'img/minicars/colorado.png', 'c1.png', 'c2.png', 'c3.png'),
(28, 'Camionetas', 'Equinox', '$ 85.990.000', 'l', 'img/minicars/equinox.png', 'e1.png', 'e2.png', 'e3.png'),
(30, 'carros', 'Spark GT Activ', '$40.300.000', 'l', 'img/minicars/sparxkgtactiv.png', 'sga1.png', 'sga2.png', ''),
(32, 'carros', 'ONIX SEDAN', '$45.260.000', 'l', 'img/minicars/onixsedan.png', 'negrosedan.png', 'rojosedan.png', 'onixsedan.png'),
(33, 'Premium', 'Blazer', '146990000', 'l', 'img/minicars/2019-blazer-top-nav-jellybean.png', 'blazernegro.webp', 'blazerrojo.webp', 'blazergris.webp');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `agendac`
--
ALTER TABLE `agendac`
  ADD PRIMARY KEY (`ida`);

--
-- Indices de la tabla `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `caracteristicas_v`
--
ALTER TABLE `caracteristicas_v`
  ADD PRIMARY KEY (`id_c`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`nit`);

--
-- Indices de la tabla `colision`
--
ALTER TABLE `colision`
  ADD PRIMARY KEY (`idc`);

--
-- Indices de la tabla `colision1`
--
ALTER TABLE `colision1`
  ADD PRIMARY KEY (`idcoli`);

--
-- Indices de la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cotizar`
--
ALTER TABLE `cotizar`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empleos`
--
ALTER TABLE `empleos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `llamada`
--
ALTER TABLE `llamada`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `peritaje`
--
ALTER TABLE `peritaje`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `plan`
--
ALTER TABLE `plan`
  ADD PRIMARY KEY (`id_plan`);

--
-- Indices de la tabla `plan_adquirido`
--
ALTER TABLE `plan_adquirido`
  ADD PRIMARY KEY (`id_pa`),
  ADD KEY `id_plan2` (`id_plan`),
  ADD KEY `nitc` (`nit_cliente`);

--
-- Indices de la tabla `posventa`
--
ALTER TABLE `posventa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `publicidad`
--
ALTER TABLE `publicidad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `repuestos`
--
ALTER TABLE `repuestos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id_servicio`),
  ADD KEY `id_sp` (`id_spf`),
  ADD KEY `id_spm` (`id_spm`),
  ADD KEY `id_spe` (`id_spe`),
  ADD KEY `id_spg` (`id_sg`);

--
-- Indices de la tabla `servicios_adquiridos`
--
ALTER TABLE `servicios_adquiridos`
  ADD PRIMARY KEY (`id_sa`),
  ADD KEY `id_pa` (`id_pa`),
  ADD KEY `id_servicio` (`id_servicio`);

--
-- Indices de la tabla `subplan`
--
ALTER TABLE `subplan`
  ADD PRIMARY KEY (`id_sp`),
  ADD KEY `id_plan` (`id_plan`);

--
-- Indices de la tabla `usados`
--
ALTER TABLE `usados`
  ADD PRIMARY KEY (`idv`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `email` (`email`),
  ADD KEY `login` (`password`);

--
-- Indices de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD PRIMARY KEY (`id_v`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `agendac`
--
ALTER TABLE `agendac`
  MODIFY `ida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT de la tabla `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `colision1`
--
ALTER TABLE `colision1`
  MODIFY `idcoli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `cotizar`
--
ALTER TABLE `cotizar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT de la tabla `empleos`
--
ALTER TABLE `empleos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=462;

--
-- AUTO_INCREMENT de la tabla `llamada`
--
ALTER TABLE `llamada`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT de la tabla `peritaje`
--
ALTER TABLE `peritaje`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `posventa`
--
ALTER TABLE `posventa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de la tabla `publicidad`
--
ALTER TABLE `publicidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `repuestos`
--
ALTER TABLE `repuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de la tabla `servicios_adquiridos`
--
ALTER TABLE `servicios_adquiridos`
  MODIFY `id_sa` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usados`
--
ALTER TABLE `usados`
  MODIFY `idv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  MODIFY `id_v` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `plan_adquirido`
--
ALTER TABLE `plan_adquirido`
  ADD CONSTRAINT `id_plan2` FOREIGN KEY (`id_plan`) REFERENCES `plan` (`id_plan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `nitc` FOREIGN KEY (`nit_cliente`) REFERENCES `cliente` (`nit`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD CONSTRAINT `id_sp` FOREIGN KEY (`id_spf`) REFERENCES `subplan` (`id_sp`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_spe` FOREIGN KEY (`id_spe`) REFERENCES `subplan` (`id_sp`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_spg` FOREIGN KEY (`id_sg`) REFERENCES `subplan` (`id_sp`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_spm` FOREIGN KEY (`id_spm`) REFERENCES `subplan` (`id_sp`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `servicios_adquiridos`
--
ALTER TABLE `servicios_adquiridos`
  ADD CONSTRAINT `id_pa` FOREIGN KEY (`id_pa`) REFERENCES `plan_adquirido` (`id_pa`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_servicio` FOREIGN KEY (`id_servicio`) REFERENCES `servicios` (`id_servicio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `subplan`
--
ALTER TABLE `subplan`
  ADD CONSTRAINT `id_plan` FOREIGN KEY (`id_plan`) REFERENCES `plan` (`id_plan`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
